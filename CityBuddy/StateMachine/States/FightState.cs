﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Linq;

namespace CityBuddy
{
    public class FightState : IState
    {
        private SimpleChar _target;

        public FightState(SimpleChar target)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Extensions.IsNull(_target)
                || (!_target.Name.Contains("Hacker") && DynelManager.NPCs.Any(c => c.Name.Contains("Hacker")))) { return new ScanState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            DynelManager.LocalPlayer.StopAttack();
        }

        public void Tick()
        {
            if (CityBuddy._initSit)
                return;

            if (DynelManager.LocalPlayer.FightingTarget == null
                && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
            {
                DynelManager.LocalPlayer.Attack(_target, false);
                Chat.WriteLine($"Attacking {_target.Name}.");
            }
        }
    }
}
