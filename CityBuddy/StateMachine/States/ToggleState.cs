﻿using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CityBuddy
{
    public class ToggleState : IState
    {
        private static bool _toggled = false;
        private static bool _init = false;

        public IState GetNextState()
        {
            if (_toggled)
                return new ScanState();

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ToggleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ToggleState::OnStateExit");
            _init = false;
            _toggled = false;
        }

        public void Tick()
        {
            Dynel citycontroller = DynelManager.AllDynels
                .Where(c => c.Name == "City Controller")
                .FirstOrDefault();

            if (citycontroller != null && !_toggled)
            {
                if (citycontroller?.DistanceFrom(DynelManager.LocalPlayer) < 6f
                    && !_init)
                {
                    _init = true;

                    if (MovementController.Instance.IsNavigating)
                        MovementController.Instance.Halt();

                    ToggleCloak(citycontroller);
                }
                else if (citycontroller?.DistanceFrom(DynelManager.LocalPlayer) >= 6f)
                    MovementController.Instance.SetDestination(citycontroller.Position);
            }
        }

        private static void ToggleCloak(Dynel citycontroller)
        {
            Task.Factory.StartNew(
              async () =>
              {
                  citycontroller.Use();
                  await Task.Delay(2000);
                  if (Inventory.Find(257110, out Item cru))
                      cru.UseOn(citycontroller.Identity);
                  await Task.Delay(2000);
                  CityController.ToggleCloak();
                  await Task.Delay(3500);
                  _init = false;
                  _toggled = true;
              });
        }
    }
}
