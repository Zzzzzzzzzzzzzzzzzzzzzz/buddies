﻿using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace CityBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (CityBuddy._settings["Toggle"].AsBool() && CityBuddy.Toggle)
            {
                CityBuddy.ParkPos = DynelManager.LocalPlayer.Position;

                if (DynelManager.LocalPlayer.Identity == CityBuddy.Leader)
                    return new ToggleState();
                else
                    return new ScanState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");
        }

        public void Tick()
        {
        }
    }
}
