﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using System;
using System.Linq;

namespace CityBuddy
{
    public class ScanState : IState
    {
        private SimpleChar _target;
        public IState GetNextState()
        {
            if (_target != null) { return new FightState(_target); }

            if (CityController.CanToggleCloak() 
                && !DynelManager.NPCs.Any(c => c.Health > 0 && c.Position.DistanceFrom(CityBuddy.ParkPos) < 80f))
                return new ToggleState();

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("AttackState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("AttackState::OnStateExit");
        }

        public void Tick()
        {
            SimpleChar _alien = DynelManager.NPCs
                .Where(c => c.Health > 0
                    && c.Position.DistanceFrom(CityBuddy.ParkPos) < 80f)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ThenBy(c => c.HealthPercent)
                .OrderByDescending(c => c.Name.Contains("Hacker"))
                .FirstOrDefault();

            Corpse _genCorpse = DynelManager.Corpses
                .Where(c => c.Name.Contains("General"))
                .FirstOrDefault();

            if (_alien != null)
            {
                MovementController.Instance.Halt();
                _target = _alien;
            }
            else if (_genCorpse != null && DynelManager.LocalPlayer.Position.DistanceFrom(_genCorpse.Position) > 1f)
                MovementController.Instance.SetDestination(_genCorpse.Position);
            else if (_genCorpse == null && DynelManager.LocalPlayer.Position.DistanceFrom(CityBuddy.ParkPos) > 1f)
                MovementController.Instance.SetDestination(CityBuddy.ParkPos);
        }
    }
}
