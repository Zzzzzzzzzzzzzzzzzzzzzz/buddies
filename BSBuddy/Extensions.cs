﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using static BSBuddy.BSBuddy;

namespace BSBuddy
{
    public static class Extensions
    {
        public static bool IsCasting()
        {
            return Spell.List.Any(c => (c.Id == 223372 || c.Id == 287046) && !c.IsReady);
        }

        public static string TryGetButtonName(out string _buttonName)
        {
            if (CappingSelection.A == (CappingSelection)_settings["CappingSelection"].AsInt32())
                return _buttonName = "Teleport G"; // A
            else if (CappingSelection.B == (CappingSelection)_settings["CappingSelection"].AsInt32())
                return _buttonName = "Teleport J"; // B
            else if (CappingSelection.C == (CappingSelection)_settings["CappingSelection"].AsInt32())
                return _buttonName = "Teleport M"; // C
            else if (CappingSelection.Core == (CappingSelection)_settings["CappingSelection"].AsInt32())
                return _buttonName = "Teleport H"; // Core

            return (_buttonName = string.Empty);
        }
    }
}
