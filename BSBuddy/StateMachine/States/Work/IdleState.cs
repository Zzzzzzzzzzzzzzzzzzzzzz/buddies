﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace BSBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (BSBuddy._settings["Toggle"].AsBool()) 
            {
                if (BSBuddy.Loop)
                {
                    if (Time.NormalTime > BSBuddy._delay +55f) 
                    {
                        BSBuddy.Loop = false;
                        return new EnterBattleStationState();
                    }
                }
                else { return new EnterBattleStationState(); }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");
        }

        public void Tick()
        {
        }
    }
}
