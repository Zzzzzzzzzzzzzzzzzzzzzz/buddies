﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace BSBuddy
{
    public class CapBState : IState
    {
        private static List<Vector3> _waypoints = new List<Vector3>()
        {
            new Vector3(967.2f, 53.4f, 730.1f),
            new Vector3(974.4f, 53.3f, 655.3f),
            new Vector3(989.3f, 53.5f, 656.3f)
        };

        private static bool _init = false;
        private static bool _triggered = false;
        private static bool _fin = false;

        private static double _timer;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == 6007)
            {
                //BSBuddy._settings["Toggle"] = false;
                //BSBuddy.Toggle = false;
                //Chat.WriteLine("Buddy disabled.");
                BSBuddy._delay = Time.NormalTime;
                BSBuddy.Loop = true;
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("CapBState::OnStateEnter");

            MovementController.Instance.SetPath(_waypoints);

            _timer = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("CapBState::OnStateExit");

            _fin = false;
            _triggered = false;
        }

        public void Tick()
        {
            if (_fin) { return; }

            if (!_init && DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(989.3f, 53.5f, 656.3f)) < 6f
                && Time.NormalTime > _timer + 16f && !MovementController.Instance.IsNavigating && !Spell.List.Any(c => !c.IsReady) && !Item.HasPendingUse)
            {
                Dynel _terminal = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Secondary Control Point");

                if (_terminal != null)
                {
                    _terminal?.Use();
                    _init = true;
                    _triggered = true;
                    _timer = Time.NormalTime;
                }
            }

            if (_init && _triggered && !DynelManager.LocalPlayer.Buffs.Contains(265816)
                && Time.NormalTime > _timer + 14f && !Spell.List.Any(c => !c.IsReady) && !Item.HasPendingUse)
            {
                Dynel _terminal = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Secondary Control Point");

                if (_terminal != null)
                {
                    _terminal?.Use();
                    _init = false;
                    _fin = true;
                }
            }
        }
    }
}
