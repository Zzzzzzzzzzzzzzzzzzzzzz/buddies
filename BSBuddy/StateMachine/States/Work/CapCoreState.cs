﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSBuddy
{
    public class CapCoreState : IState
    {
        private static List<Vector3> _waypoints = new List<Vector3>()
        {
            new Vector3(998.8f, 54.1f, 850.0f),
            new Vector3(1012.6f, 66.0f, 1170.2f),
            new Vector3(1041.2f, 66.1f, 1216.6f),
            new Vector3(1014.1f, 66.0f, 1256.5f)
        };

        private static bool _init = false;
        private static bool _triggered = false;
        private static bool _fin = false;

        private static double _timer;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == 6007)
            {
                //BSBuddy._settings["Toggle"] = false;
                //BSBuddy.Toggle = false;
                //Chat.WriteLine("Buddy disabled.");
                BSBuddy._delay = Time.NormalTime;
                BSBuddy.Loop = true;
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("CapCoreState::OnStateEnter");

            MovementController.Instance.SetDestination(new Vector3(998.8f, 54.1f, 850.0f));

            _timer = Time.NormalTime;

            _init = true;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("CapCoreState::OnStateExit");

            _fin = false;
            _triggered = false;
        }

        public void Tick()
        {
            if (_fin) { return; }

            if (_init && DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(998.8f, 54.1f, 850.0f)) < 10f)
            {
                _init = false;
                MovementController.Instance.SetPath(_waypoints);
            }

            if (!_init && DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1011.9f, 66.1f, 1255.4f)) <= 7f
                && Time.NormalTime > _timer + 16f && !MovementController.Instance.IsNavigating && !Spell.List.Any(c => !c.IsReady) && !Item.HasPendingUse)
            {
                Dynel _terminal = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Primary Control Point" && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 7f);

                if (_terminal != null)
                {
                    _terminal?.Use();
                    _init = true;
                    _triggered = true;
                    _timer = Time.NormalTime; 
                }
            }

            if (_init && _triggered && !DynelManager.LocalPlayer.Buffs.Contains(265816)
                && Time.NormalTime > _timer + 14f && !Spell.List.Any(c => !c.IsReady) && !Item.HasPendingUse)
            {
                Dynel _terminal = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Primary Control Point" && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 7f);

                if (_terminal != null)
                {
                    _terminal?.Use();
                    _init = false;
                    _fin = true;
                }
            }
        }
    }
}
