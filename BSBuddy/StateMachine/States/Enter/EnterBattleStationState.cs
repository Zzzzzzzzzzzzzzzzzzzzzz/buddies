﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;

namespace BSBuddy
{
    public class EnterBattleStationState : IState
    {
        private static bool _init = false;
        private static bool _proceed = false;
        private static bool _finished = false;

        private CancellationTokenSource _cancellationToken1 = new CancellationTokenSource();

        public IState GetNextState()
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1862.7f, 41.7f, 303.0f)) < 30f
                && _proceed && !Game.IsZoning && BSBuddy._invites > 1 && _finished) { return new ContainmentState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("EnterBattleStationState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("EnterBattleStationState::OnStateExit");

            _cancellationToken1.Cancel();

            _init = false;
            _proceed = false;
            _finished = false;
            BSBuddy._invites = 0;
            BSBuddy.BattleStationIdentity = Identity.None;
        }

        public void Tick()
        {
            Dynel _terminal = DynelManager.AllDynels
                .Where(c => c.Name.Contains("Battle Station Terminal")
                && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 7f)
                .FirstOrDefault();

            if (BSBuddy.BattleStationIdentity != Identity.None && _proceed && BSBuddy._invites > 1)
            {
                Battlestation.AcceptInvite(BSBuddy.BattleStationIdentity);
                Battlestation.AcceptInvite(BSBuddy.BattleStationIdentity);
                _finished = true;
            }

            if (_terminal != null)
            {
                if (!_init)
                {
                    _init = true;
                    Task.Factory.StartNew(
                        async () =>
                        {
                            Network.Send(new CharacterActionMessage()
                            {
                                Action = (CharacterActionType)254,
                                Target = DynelManager.LocalPlayer.Identity
                            });
                            await Task.Delay(1500);
                            Network.Send(new CharacterActionMessage()
                            {
                                Action = CharacterActionType.JoinBattlestationQueue,
                                Target = DynelManager.LocalPlayer.Identity,
                                Parameter2 = 0 //Red
                            });
                            Chat.WriteLine("Signed Red.");
                            await Task.Delay(500);
                            Network.Send(new CharacterActionMessage()
                            {
                                Action = CharacterActionType.JoinBattlestationQueue,
                                Target = DynelManager.LocalPlayer.Identity,
                                Parameter2 = 1 //Blue
                            });
                            Chat.WriteLine("Signed Blue.");
                            await Task.Delay(500);
                            _proceed = true;
                        }, _cancellationToken1.Token);
                }
            }
        }
    }
}