﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static BSBuddy.BSBuddy;

namespace BSBuddy
{
    public class ContainmentState : IState
    {
        private static bool _proceed = false;
        private static bool _init = false;

        private static double _timer;

        public IState GetNextState()
        {
            if (_proceed)
            {
                if (CappingSelection.A == (CappingSelection)_settings["CappingSelection"].AsInt32())
                    return new CapAState();
                else if (CappingSelection.B == (CappingSelection)_settings["CappingSelection"].AsInt32())
                    return new CapBState();
                else if (CappingSelection.C == (CappingSelection)_settings["CappingSelection"].AsInt32())
                    return new CapCState();
                else if (CappingSelection.Core == (CappingSelection)_settings["CappingSelection"].AsInt32())
                    return new CapCoreState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ContainmentState::OnStateEnter");

            MovementController.Instance.SetDestination(new Vector3(1862.7f, 41.7f, 303.0f));

            _timer = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ContainmentState::OnStateExit");

            _init = false;
            _proceed = false;
        }

        public void Tick()
        {
            if (Time.NormalTime > _timer + 21f)
            {
                ContainmentFinished();

                _timer = Time.NormalTime;
            }
        }

        private static void ContainmentFinished()
        {
            Dynel _terminal = DynelManager.AllDynels.FirstOrDefault(c => c.Name == Extensions.TryGetButtonName(out string str) 
                && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 40f);

            if (_terminal != null)
            {
                if (!Extensions.IsCasting() && !Item.HasPendingUse)
                {
                    Task.Factory.StartNew(
                        async () =>
                        {
                            await Task.Delay(3000);
                            _terminal.Use();
                            await Task.Delay(2000);
                        });
                }
            }
            else { _proceed = true; }
        }
    }
}
