﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using static TemplateBuddy.TemplateBuddy;

namespace TemplateBuddy
{
    public static class Extensions
    {
        public static bool FindKit(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants.Kits.Contains(c.Id))) != null;
        }

        public static bool CanUseSitKit()
        {
            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)
                && DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving && !Game.IsZoning
                && !DynelManager.LocalPlayer.Buffs.Contains(280488) && Spell.List.Any(c => (c.Id == 223372 || c.Id == 287046) && c.IsReady) && !InCombat())
            {
                if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if (!sitKits.Any()) { return false; }

                foreach (Item sitKit in sitKits.OrderBy(x => x.QualityLevel))
                {
                    int skillReq = (sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f));

                    if (DynelManager.LocalPlayer.GetStat(Stat.FirstAid) >= skillReq || DynelManager.LocalPlayer.GetStat(Stat.Treatment) >= skillReq)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static CharacterWieldedWeapon GetWieldedWeapons(SimpleChar local) => (CharacterWieldedWeapon)local.GetStat(Stat.EquippedWeapons);

        public static void HandlePathing(SimpleChar target)
        {
            if (NavMeshMovementController.IsNavigating && target?.IsInLineOfSight == true)
            {
                if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 5f
                    && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                        || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                    NavMeshMovementController.Halt();

                if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                    && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged)
                        || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                    NavMeshMovementController.Halt();
            }

            if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 5f
                && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                    || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                NavMeshMovementController.SetNavMeshDestination((Vector3)target?.Position);

            if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged)
                    || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                NavMeshMovementController.SetNavMeshDestination((Vector3)target?.Position);
        }

        public static bool Rooted()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot);
        }

        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending
                || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }

        [Flags]
        public enum CharacterWieldedWeapon
        {
            Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
            MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
            Melee = 0x02,             // 0x00000000000000000010b
            Ranged = 0x04,            // 0x00000000000000000100b
            Bow = 0x08,               // 0x00000000000000001000b
            Smg = 0x10,               // 0x00000000000000010000b
            Edged1H = 0x20,           // 0x00000000000000100000b
            Blunt1H = 0x40,           // 0x00000000000001000000b
            Edged2H = 0x80,           // 0x00000000000010000000b
            Blunt2H = 0x100,          // 0x00000000000100000000b
            Piercing = 0x200,         // 0x00000000001000000000b
            Pistol = 0x400,           // 0x00000000010000000000b
            AssaultRifle = 0x800,     // 0x00000000100000000000b
            Rifle = 0x1000,           // 0x00000001000000000000b
            Shotgun = 0x2000,         // 0x00000010000000000000b
            Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
            MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
            RangedEnergy = 0x10000,   // 0x00010000000000000000b
            Grenade2 = 0x20000,        // 0x00100000000000000000b
            HeavyWeapons = 0x40000,   // 0x01000000000000000000b
        }
    }
}
