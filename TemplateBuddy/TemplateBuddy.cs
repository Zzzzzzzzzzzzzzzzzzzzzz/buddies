﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using TemplateBuddy.IPCMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.IO;
using System.Text;
using AOSharp.Pathfinding;
using AOSharp.Core.Misc;

namespace TemplateBuddy
{
    public class TemplateBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;
        public static Item _kit;

        public static bool Toggle = false;
        private static bool _initSit = false;

        public static Window _infoWindow;

        public static Settings _settings;

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("TemplateBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\TemplateBuddy\\{Game.ClientInst}\\Config.json");
                NavMeshMovementController = new NavMeshMovementController($"{pluginDir}\\NavMeshes", true);
                MovementController.Set(NavMeshMovementController);
                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;

                Chat.RegisterCommand("buddy", TemplateBuddyCommand);

                SettingsController.RegisterSettingsWindow("TemplateBuddy", pluginDir + "\\UI\\TemplateBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                Game.OnUpdate += OnUpdate;

                _settings.AddVariable("Toggle", false);

                Chat.RegisterCommand("testcommand", (string command, string[] param, ChatWindow chatWindow) =>
                {

                });

                _settings["Toggle"] = false;

                Chat.WriteLine("TemplateBuddy Loaded! Version: 0.9.9.8");
                Chat.WriteLine("/templatebuddy for settings.");
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        public static void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;

            Chat.WriteLine("Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());

            MovementController.Instance.Halt();
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None
                && DynelManager.LocalPlayer.Identity.Instance != sender)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            _settings["Toggle"] = true;
            Start();
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            _settings["Toggle"] = false;
            Stop();
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\TemplateBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            //GameTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Time.NormalTime);

            _stateMachine.Tick();

            if (_settings["Toggle"].AsBool())
                ListenerSit();

            #region UI Update

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;

                if (SettingsController.settingsWindow.FindView("TemplateBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    Leader = DynelManager.LocalPlayer.Identity;

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StartMessage());

                    Start();
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Stop();

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StopMessage());
                }
            }

            #endregion
        }

        private void TemplateBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        Leader = DynelManager.LocalPlayer.Identity;

                        IPCChannel.Broadcast(new StartMessage());
                        _settings["Toggle"] = true;
                        Start();

                    }
                    else
                    {
                        Stop();
                        _settings["Toggle"] = false;
                        IPCChannel.Broadcast(new StopMessage());
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void ListenerSit()
        {
            //bool shouldSitKit = DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66;
            //bool canSitKit = Extensions.CanUseSitKit(out _kit);

            //if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //{
            //    if (canSitKit && shouldSitKit)
            //        _kit?.Use(DynelManager.LocalPlayer, true);
            //    else if (_initSit || Time.NormalTime > _sitUpdateTimer +2f)
            //    {
            //        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //        _initSit = false;
            //    }
            //}
            //else if (DynelManager.LocalPlayer.MovementState == MovementState.Run && canSitKit && shouldSitKit)
            //{
            //    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //    _initSit = true;
            //    _sitUpdateTimer = Time.NormalTime;
            //}

            //if (_initSit == false && Extensions.FindKit(out Item _kit) && Spell.List.Any(c => c.IsReady)
            //    && !DynelManager.LocalPlayer.Buffs.Contains(280488) && Extensions.CanUseSitKit(out _kit))
            //{
            //    if (DynelManager.LocalPlayer.MovementState == MovementState.Sit
            //        && DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)
            //        && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66))
            //    {
            //        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //    }
            //    else if (DynelManager.LocalPlayer.MovementState != MovementState.Sit
            //        && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
            //        && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment))
            //        MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //}

            if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66
                && (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment) || _initSit))
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                    MovementController.Instance.SetMovement(MovementAction.LeaveSit);

                if (_initSit)
                    _initSit = false;
            }

            if (Extensions.CanUseSitKit() && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            {
                if (!_initSit && DynelManager.LocalPlayer.MovementState != MovementState.Sit)
                {
                    _initSit = true;
                    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                }
            }

            //if (_initSit == false && Extensions.CanUseSitKit(out _kit)
            //    && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            //{
            //    Task.Factory.StartNew(
            //        async () =>
            //        {
            //            _initSit = true;
            //            await Task.Delay(400);
            //            MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //            await Task.Delay(1800);
            //            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //            await Task.Delay(400);
            //            _initSit = false;
            //        });
            //}
        }
    }
}