﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace TemplateBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (TemplateBuddy._settings["Toggle"].AsBool()) { return new WorkState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");
        }

        public void Tick()
        {
        }
    }
}
