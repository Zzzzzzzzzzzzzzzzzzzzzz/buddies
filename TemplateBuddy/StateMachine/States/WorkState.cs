﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;

namespace TemplateBuddy
{
    public class WorkState : IState
    {
        private static bool _initializingSwitch = false;
        private static bool _exitSwitch = false;

        public IState GetNextState()
        {
            if (_exitSwitch) { return new IdleState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("EnterState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("EnterState::OnStateExit");

            _initializingSwitch = false;
            _exitSwitch = false;
        }

        public void Tick()
        {
            if (!_initializingSwitch)
            {
                _initializingSwitch = true;
                _exitSwitch = true;
                Chat.WriteLine("I am zoop!");
                TemplateBuddy._settings["Toggle"] = false;
                TemplateBuddy.Toggle = false;
                Chat.WriteLine("Buddy disabled.");
            }
        }
    }
}