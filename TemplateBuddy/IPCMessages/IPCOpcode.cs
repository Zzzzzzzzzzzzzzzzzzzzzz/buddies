﻿namespace TemplateBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 1011,
        Stop = 1012
    }
}
