﻿using AOSharp.Common.GameData;
using System.Collections.Generic;

namespace InfBuddy
{
    public static class Constants
    {
        public static List<string> _ignores = new List<string>
        {
                    "Ergo, Inferno Guardian of Shadows",
                    "One Who Obeys Precepts",
                    "Buckethead Technodealer",
                    "The Retainer Of Ergo",
                    "Buckethead Technodealer",
                    "Guardian Spirit of Purification"
        };

        public static readonly int[] Kits =
        {
            297274, 293296, 291084, 291083, 291082
        };

        public const string SpiritNPCName = "Guardian Spirit of Purification";
        public const string QuestStarterName = "One Who Obeys Precepts";
        public const string QuestGiverName = "The Retainer Of Ergo";

        public static Vector3 LookingForTeamPosition = new Vector3(2787.5f, 25.4f, 3373.9f);
        public static Vector3 DefendPosition = new Vector3(183.8f, 1.0f, 189.9f); // 180.7f, 1.0f, 184.5f
        public static Vector3 EntrancePos = new Vector3(2726.2f, 25.9f, 3339.0f); // 2726.2f, 25.9f, 3339.0f // 2722.7f, 25.3f, 3333.2f
        public static Vector3 EntranceFinalPos = new Vector3(2729.4f, 25.5f, 3345.2f);
        public static Vector3 ExitPos = new Vector3(159.5f, 2.5f, 101.2f); // 159.5f, 2.5f, 101.2f // 162.1f, 2.3f, 105.3f
        public static Vector3 ExitFinalPos = new Vector3(155.5f, 2.3f, 95.5f);
        public static Vector3 QuestGiverPos = new Vector3(2804.9f, 25.5f, 3375.8f);
        public static Vector3 QuestStarterBeforePos = new Vector3(168.9f, 1, 134.4f);
        public static Vector3 QuestStarterPos = new Vector3(181.5f, 1, 160.5f);
        public static Vector3 LeechPosition = new Vector3(156.8f, 1.0f, 101.1f);
        public static Vector3 LeechMissionExit = new Vector3(161.2f, 2.7f, 104.2f);
        public static Vector3 EasyLFTPosition = new Vector3(2800.9f, 25.4f, 3387.0f);
        public static Vector3 MediumLFTPosition = new Vector3(2793.0f, 25.4f, 3384.4f);
        public static Vector3 HardLFTPosition = new Vector3(2790.6, 25.4f, 3377.0);

        public const int InfernoId = 4605;
        public const int OmniPandeGId = 4697;
        public const int ClanPandeGId = 4696;
        public const int PandePlatId = 4328;
        public const int XanReliquaryId = 9042;
        public const int XanHubId = 6013;
    }
}
