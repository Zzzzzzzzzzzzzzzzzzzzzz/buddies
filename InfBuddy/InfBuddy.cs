﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using InfBuddy.IPCMessages;
using AOSharp.Common.GameData.UI;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI.Options;
using System.Threading;
using AOSharp.Core.Misc;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace InfBuddy
{

    public class InfBuddy : AOPluginEntry
    {
        private StateMachine _stateMachine;
        public static IPCChannel IPCChannel { get; set; }
        public static Config Config { get; private set; }

        public static bool Toggle = false;

        public static SimpleChar _leader;
        public static SimpleChar _mob;
        public static Identity Leader = Identity.None;
        public static string LeaderName = string.Empty;
        public static Item _kit;
        //public static Spell _isCastingSpellDummy;

        public static SimpleChar _charmMob;
        public static SimpleChar _brokenCharmMob;

        public static DateTime _previousRound = DateTime.MinValue;
        public static DateTime _currentRound = DateTime.MinValue;
        public static TimeSpan _missionTimeGuesstimation = TimeSpan.Zero;

        public static Corpse _corpse;

        public static float Tick = 0;

        public static double _castTimer;
        public static double _castRecharge;
        public static bool _casting = false;

        public static bool DoubleReward = false;
        public static bool EndMission = false;
        public static bool _initSit = false;
        public static bool _merging = false;
        public static bool _initXanHub = false;
        public static bool _died = false;

        public static bool _subbedUIEvents = false;

        private static double _mainUpdate;
        public static double _lftUpdate;
        public static double _stateTimeOut;
        public static double _lastZonedTime;

        public static List<string> _corpsesLooted = new List<string>();
        public static List<Vector3> _corpsesPositions = new List<Vector3>();

        public static List<Identity> _charmMobs = new List<Identity>();

        public static List<Identity> _invitedIdentities = new List<Identity>();
        public static List<string> _invitedNames = new List<string>();
        public static List<Identity> _acceptedIDs = new List<Identity>();

        public static TextInputView _leaderInput;
        public static TextInputView _channelInput;
        public static TextInputView _tickInput;

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        private bool IsActiveWindow => GetForegroundWindow() == Process.GetCurrentProcess().MainWindowHandle;

        private static Window infoWindow;

        private static string PluginDir;

        public static Settings _settings;

        public override void Run(string pluginDir)
        {
            try
            {
                #region Settings

                _settings = new Settings("InfBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\InfBuddy\\{Game.ClientInst}\\Config.json");

                _settings.AddVariable("LookingSelection", (int)LookingSelection.None);
                _settings.AddVariable("ModeSelection", (int)ModeSelection.Roam);
                _settings.AddVariable("FactionSelection", (int)FactionSelection.Omni);
                _settings.AddVariable("DifficultySelection", (int)DifficultySelection.Hard);

                _settings.AddVariable("Toggle", false);

                _settings.AddVariable("OurOrg", false);

                _settings.AddVariable("DoubleReward", false);

                _settings.AddVariable("Waiting", false);
                _settings.AddVariable("Looting", false);

                _settings["Toggle"] = false;

                #endregion

                #region NavMesh Controller

                SMovementControllerSettings mSettings = new SMovementControllerSettings
                {
                    NavMeshSettings = SNavMeshSettings.Default,
                    PathSettings = SPathSettings.Default,
                };

                mSettings.PathSettings.Extents = new Vector3(5, 100, 5);
                SMovementController.Set(mSettings);
                SMovementController.AutoLoadNavmeshes($"{pluginDir}\\NavMeshes");
                SMovementController.ToggleNavMeshDraw();
                SMovementController.TogglePathDraw();
                //SMovementController.Stuck += OnStuck;

                #endregion

                #region IPC

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);

                #endregion

                //IsCasting Bandaid
                //_isCastingSpellDummy = Spell.List.FirstOrDefault(c => c.Id == 223372);
                LookingForTeam.Leave();

                Chat.WriteLine("InfBuddy Loaded! Version: 0.9.9.98");
                Chat.WriteLine("/infbuddy for settings.");

                #region Events

                SettingsController.RegisterSettingsWindow("InfBuddy", pluginDir + "\\UI\\InfBuddySettingWindow.xml", _settings);

                NpcDialog.AnswerListChanged += NpcDialog_AnswerListChanged;
                Game.OnUpdate += OnUpdate;
                Game.TeleportEnded += OnEndZoned;
                DynelManager.DynelSpawned += OnDynelSpawned;
                LookingForTeam.SearchResultReceived += LookingForTeam_SearchResultReceived;
                Network.N3MessageSent += Network_N3MessageSent;

                #endregion

                #region Commands

                Chat.RegisterCommand("buddy", InfBuddyCommand);

                #endregion

                #region Init

                _stateMachine = new StateMachine(new IdleState());

                LeaderName = Config.CharSettings[Game.ClientInst].Leader;
                Tick = Config.CharSettings[Game.ClientInst].Tick;

                #endregion
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        #region Callbacks

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None) { Leader = new Identity(IdentityType.SimpleChar, sender); }

            if (Extensions.TryGetLeader(out _leader) != null)
            {
                Config.CharSettings[Game.ClientInst].Leader = $"{Extensions.TryGetLeader(out _leader)?.Name}";

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow?.FindView("LeaderBox", out _leaderInput);

                    if (_leaderInput != null)
                        _leaderInput.Text = Config.CharSettings[Game.ClientInst].Leader;
                }

                Config.Save();
            }

            Chat.WriteLine("Buddy enabled.");

            _settings["Toggle"] = true;
            Toggle = true;

            _stateMachine.SetState(new IdleState());
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            if (DynelManager.LocalPlayer.Identity == Leader || IsActiveWindow) { return; }

            _settings["Toggle"] = false;
            Toggle = false;

            Chat.WriteLine("Buddy disabled.");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (SMovementController.IsNavigating())
                SMovementController.Halt();

            _stateMachine.SetState(new IdleState());
        }

        #endregion

        #region Events

        private void LookingForTeam_SearchResultReceived(object s, LookingForTeamSearchResultEventArgs e)
        {
            if (Leader != DynelManager.LocalPlayer.Identity || Team.Members.Count() == 6) { return; }

            foreach (LookingForTeamApplicant _app in e.Applicants)
            {
                if (!Team.Members.Select(c => c.Name).Contains(_app.Name)
                    && _app.Message.ToLower() != ""
                    && (_app.Message.ToLower().Contains("new inferno missions")
                        || _app.Message.ToLower().Contains("new inf missions")
                        || _app.Message.ToLower().Contains("new inf")
                        || _app.Message.ToLower().Contains("new inferno")
                        || _app.Message.ToLower().Contains("new inf miss")
                        || _app.Message.ToLower().Contains("new inf mish")
                        || _app.Message.ToLower().Contains("new inferno miss")
                        || _app.Message.ToLower().Contains("new inferno mish")
                        || _app.Message.ToLower().Contains("new inf mission")
                        || _app.Message.ToLower().Contains("new inferno mission")
                        || _app.Message.ToLower().Contains("inferno mission new")
                        || _app.Message.ToLower().Contains("inf mission new")
                        || _app.Message.ToLower().Contains("inferno missions new")
                        || _app.Message.ToLower().Contains("inf missions new")))
                {
                    string[] _split = _app.Message.ToLower().Split();

                    if (ModeSelection.Defend == (ModeSelection)_settings["ModeSelection"].AsInt32()
                        && !_split.Contains("defend")) { continue; }

                    if (_split.Contains("easy"))
                    {
                        if (_split.Contains("neutral"))
                        {
                            if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                        else if (_split.Contains("omni"))
                        {
                            if (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                        else if (_split.Contains("clan"))
                        {
                            if (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                    }
                    else if (_split.Contains("medium"))
                    {
                        if (_split.Contains("neutral"))
                        {
                            if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                        else if (_split.Contains("omni"))
                        {
                            if (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                        else if (_split.Contains("clan"))
                        {
                            if (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                    }
                    else if (_split.Contains("hard"))
                    {
                        if (_split.Contains("neutral"))
                        {
                            if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                        else if (_split.Contains("omni"))
                        {
                            if (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }
                        else if (_split.Contains("clan"))
                        {
                            if (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32()
                                && DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                            {
                                if (_settings["OurOrg"].AsBool())
                                {
                                    if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                                }
                                else { Team.Invite(_app.Identity); }
                            }
                        }

                    }

                    //if (_split.Length > 1)
                    //{
                    //    switch (_split[1])
                    //    {
                    //        case "Easy":
                    //            if (_split[2] == "Neutral")
                    //            {
                    //                if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            else if (_split[2] == "Omni")
                    //            {
                    //                if (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            else if (_split[2] == "Clan")
                    //            {
                    //                if (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }

                    //            //if (DynelManager.LocalPlayer.Side == _app.Side || _app.Side == Side.Neutral 
                    //            //    || (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32() && DynelManager.LocalPlayer.Side != _app.Side)) 
                    //            //{
                    //            //    if (_app.Side == Side.Neutral)
                    //            //    {
                    //            //        if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //            //            && DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //            //        {
                    //            //            if (_settings["OurOrg"].AsBool())
                    //            //            {
                    //            //                if (_split.Length > 2)
                    //            //                {
                    //            //                    if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //            //                }
                    //            //            }
                    //            //            else { Team.Invite(_app.Identity); }
                    //            //        }
                    //            //    }
                    //            //    else if (DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //            //    {
                    //            //        if (_settings["OurOrg"].AsBool())
                    //            //        {
                    //            //            if (_split.Length > 2)
                    //            //            {
                    //            //                if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //            //            }
                    //            //        }
                    //            //        else { Team.Invite(_app.Identity); }
                    //            //    }
                    //            //}
                    //            break;
                    //        case "Medium":
                    //            if (_split[2] == "Neutral")
                    //            {
                    //                if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            else if (_split[2] == "Omni")
                    //            {
                    //                if (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            else if (_split[2] == "Clan")
                    //            {
                    //                if (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            //if (DynelManager.LocalPlayer.Side == _app.Side || _app.Side == Side.Neutral
                    //            //    || (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32() && DynelManager.LocalPlayer.Side != _app.Side))
                    //            //{
                    //            //    if (_app.Side == Side.Neutral)
                    //            //    {
                    //            //        if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //            //            && DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //            //        {
                    //            //            if (_settings["OurOrg"].AsBool())
                    //            //            {
                    //            //                if (_split.Length > 2)
                    //            //                {
                    //            //                    if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //            //                }
                    //            //            }
                    //            //            else { Team.Invite(_app.Identity); }
                    //            //        }
                    //            //    }
                    //            //    else if (DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //            //    {
                    //            //        if (_settings["OurOrg"].AsBool())
                    //            //        {
                    //            //            if (_split.Length > 2)
                    //            //            {
                    //            //                if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //            //            }
                    //            //        }
                    //            //        else { Team.Invite(_app.Identity); }
                    //            //    }
                    //            //}
                    //            break;
                    //        case "Hard":
                    //            if (_split[2] == "Neutral")
                    //            {
                    //                if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            else if (_split[2] == "Omni")
                    //            {
                    //                if (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            else if (_split[2] == "Clan")
                    //            {
                    //                if (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //                    && DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //                {
                    //                    if (_settings["OurOrg"].AsBool())
                    //                    {
                    //                        if (_split.Length > 2)
                    //                        {
                    //                            if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //                        }
                    //                    }
                    //                    else { Team.Invite(_app.Identity); }
                    //                }
                    //            }
                    //            //if (DynelManager.LocalPlayer.Side == _app.Side || _app.Side == Side.Neutral
                    //            //    || (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32() && DynelManager.LocalPlayer.Side != _app.Side))
                    //            //{
                    //            //    if (_app.Side == Side.Neutral)
                    //            //    {
                    //            //        if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32()
                    //            //            && DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //            //        {
                    //            //            if (_settings["OurOrg"].AsBool())
                    //            //            {
                    //            //                if (_split.Length > 2)
                    //            //                {
                    //            //                    if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //            //                }
                    //            //            }
                    //            //            else { Team.Invite(_app.Identity); }
                    //            //        }
                    //            //    }
                    //            //    else if (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
                    //            //    {
                    //            //        if (_settings["OurOrg"].AsBool())
                    //            //        {
                    //            //            if (_split.Length > 2)
                    //            //            {
                    //            //                if ($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}" == _split[3]) { Team.Invite(_app.Identity); }
                    //            //            }
                    //            //        }
                    //            //        else { Team.Invite(_app.Identity); }
                    //            //    }
                    //            //}
                    //            break;
                    //        default:
                    //            break;
                    //    }
                    //}
                }

                //Chat.WriteLine($"Name - {_app.Name} Level - {_app.Level} Side - {_app.Side} Profession - {_app.Profession} Message - {_app.Message}");
            }
        }
        private void NpcDialog_AnswerListChanged(object s, Dictionary<int, string> options)
        {
            SimpleChar dialogNpc = DynelManager.GetDynel((Identity)s).Cast<SimpleChar>();

            if (dialogNpc.Name == Constants.QuestGiverName)
            {
                foreach (KeyValuePair<int, string> option in options)
                {
                    if (option.Value == "Is there anything I can help you with?" ||
                        (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the Unredeemed!") ||
                        (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the Redeemed!") ||
                        (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the creatures of the brink!") ||
                        (DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will deal with only the weakest aversaries") || //Brink missions have a typo
                        (DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will deal with only the weakest adversaries") ||
                        (DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                        (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && !_settings["DoubleReward"].AsBool() && option.Value == "I will purge the temple of any and all assailants") ||
                        (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && _settings["DoubleReward"].AsBool() && !DoubleReward && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                        (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && _settings["DoubleReward"].AsBool() && DoubleReward && option.Value == "I will purge the temple of any and all assailants")
                        )
                        NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                }
            }
            else if (dialogNpc.Name == Constants.QuestStarterName)
            {
                foreach (KeyValuePair<int, string> option in options)
                {
                    if (option.Value == "Yes, I am ready.")
                        NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                }
            }
        }

        private void OnStuck(Vector3 stuckPos, Vector3 destPos)
        {
            Chat.WriteLine($"Stuck - {stuckPos} Destination - {destPos}");
        }

        private void OnEndZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }
        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

            if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action == CharacterActionType.CastNano)
                {
                    if (Spell.Find(charActionMsg.Parameter2, out Spell _currentNano))
                    {
                        _casting = true;
                        _castTimer = Time.NormalTime;
                        _castRecharge = _currentNano.AttackDelay;
                    }
                }
            }
        }

        private void OnDynelSpawned(object s, Dynel dynel)
        {
            if (dynel.Identity.Type == IdentityType.SimpleChar)
            {
                if (Team.Members.Select(c => c?.Identity).Contains(dynel.Identity) && DynelManager.LocalPlayer.IsAttacking)
                {
                    Chat.WriteLine($"Character spawned, stopping attack.");
                    DynelManager.LocalPlayer.StopAttack();
                }
            }
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        public static void Leader_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].Leader = e;
            LeaderName = e;
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }

        #endregion

        #region Handles
        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\InfBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            infoWindow.Show(true);
        }

        private void HandleScan()
        {
            SimpleChar mob = DynelManager.NPCs
                .Where(c => c.Health > 0
                    && !c.IsPet
                    && !Extensions.IsFlagged(c)
                    && c.IsInLineOfSight
                    && !Constants._ignores.Contains(c.Name)
                    && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 15f)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ThenBy(c => c.HealthPercent)
                .FirstOrDefault();

            if (mob != null) { Extensions.HandleAttacking(mob); }
        }

        #endregion

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning) { return; }

            #region Edge cases

            if (Toggle && Team.IsInTeam && Leader == Identity.None && Extensions.TryGetLeader(out _leader) != null)
            {
                Config.CharSettings[Game.ClientInst].Leader = $"{Extensions.TryGetLeader(out _leader)?.Name}";
                Leader = (Identity)Extensions.TryGetLeader(out _leader)?.Identity;

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow?.FindView("LeaderBox", out _leaderInput);

                    if (_leaderInput != null)
                        _leaderInput.Text = Config.CharSettings[Game.ClientInst].Leader;
                }

                Config.Save();
            }

            #endregion

            #region LFT

            if (Toggle && Time.NormalTime > _lftUpdate + 15f)
            {
                _lftUpdate = Time.NormalTime;

                if (Team.IsInTeam && LookingSelection.Member == (LookingSelection)_settings["LookingSelection"].AsInt32()
                    && Leader == DynelManager.LocalPlayer.Identity && !ReformState.Reforming) { LookingForTeam.Search(); }
            }

            //Edge case correction: Griefers
            if (Playfield.ModelIdentity.Instance == Constants.InfernoId)
                HandleScan();

            //Edge case correction: Griefers
            if (Team.IsInTeam && Team.Members.Any(c => c.Character != null && Extensions.IsFlagged(c.Character)))
            {
                if (DynelManager.LocalPlayer.Identity == Leader)
                    Team.Kick((Identity)Team.Members.FirstOrDefault(c => c.Character != null && Extensions.IsFlagged(c.Character)).Character?.Identity);
                else { Team.Leave(); }
            }

            #endregion

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                _mainUpdate = Time.NormalTime;

                #region Stuck

                //Edge case correction: Stuck pathing.
                //if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(260.5f, 1.0f, 191.1f)) <= 1f)
                //    DynelManager.LocalPlayer.Position = new Vector3(255.7f, 1.0f, 190.0f);
                //else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(262.9f, 1.1f, 185.6f)) <= 1f)
                //    DynelManager.LocalPlayer.Position = new Vector3(259.5f, 1.0f, 183.8f);
                //else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(256.1f, 1.1f, 191.1f)) <= 1f)
                //    DynelManager.LocalPlayer.Position = new Vector3(261.2f, 1.0f, 193.3f);

                #endregion

                ListenerSit();

                _stateMachine.Tick();
            }

            #region UI Update

            if (_subbedUIEvents && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out _channelInput);
                SettingsController.settingsWindow.FindView("LeaderBox", out _leaderInput);
                SettingsController.settingsWindow.FindView("TickBox", out _tickInput);

                if (_channelInput != null && !string.IsNullOrEmpty(_channelInput.Text)
                    && int.TryParse(_channelInput.Text, out int channelValue)
                    && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (_tickInput != null && !string.IsNullOrEmpty(_tickInput.Text)
                    && float.TryParse(_tickInput.Text, out float tickValue)
                    && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                    Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (_leaderInput != null && !string.IsNullOrEmpty(_leaderInput.Text)
                    && Config.CharSettings[Game.ClientInst].Leader != _leaderInput.Text)
                    Config.CharSettings[Game.ClientInst].Leader = _leaderInput.Text;

                if (SettingsController.settingsWindow.FindView("InfBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (Leader == Identity.None && IsActiveWindow)
                    {
                        if (LookingSelection.Team != (LookingSelection)_settings["LookingSelection"].AsInt32())
                            Leader = DynelManager.LocalPlayer.Identity;

                        if (DynelManager.LocalPlayer.Identity == Leader) { IPCChannel.Broadcast(new StartMessage()); }

                        Start();
                    }
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Stop();

                    IPCChannel.Broadcast(new StopMessage());
                }
            }

            #endregion
        }

        #region Misc

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].LeaderChangedEvent -= Leader_Changed;
                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].LeaderChangedEvent += Leader_Changed;
                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        private void ListenerSit()
        {
            if (DynelManager.LocalPlayer.Profession == Profession.Metaphysicist
                && Extensions.PetNanoLow()) { return; }

            if (_initSit)
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                {
                    if (Extensions.InCombat() || (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66))
                    {
                        _initSit = false;
                        SMovementController.SetMovement(MovementAction.LeaveSit);
                    }
                }
                else if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66)
                { _initSit = false; }
            }
            else if (!Extensions.InCombat() && Extensions.CanUseSitKit())
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Run)
                {
                    if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment) /*!LocalCooldown.IsOnCooldown(Stat.Treatment)*/
                        && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
                    {
                        _initSit = true;
                        SMovementController.SetMovement(MovementAction.SwitchToSit);
                    }
                }
                else if (DynelManager.LocalPlayer.MovementState == MovementState.Sit) { SMovementController.SetMovement(MovementAction.LeaveSit); }
            }

            //if (Extensions.CanUseSitKit())
            //{
            //    if (_initSit)
            //    {
            //        if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //        {
            //            if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66)
            //            {
            //                _initSit = false;
            //                SMovementController.SetMovement(MovementAction.LeaveSit);
            //            }
            //        }
            //        else { _initSit = false; }
            //    }
            //    else if (!Extensions.InCombat() && !LocalCooldown.IsOnCooldown(Stat.Treatment))
            //    {
            //        if (Extensions.PetNanoLow()) { return; }

            //        if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
            //        {
            //            _initSit = true;
            //            SMovementController.SetMovement(MovementAction.SwitchToSit);
            //        }
            //        else if (DynelManager.LocalPlayer.MovementState == MovementState.Sit) { SMovementController.SetMovement(MovementAction.LeaveSit); }
            //    }
            //}


            //if (_initSit == false && Extensions.CanUseSitKit() && !Extensions.IsCasting() && !Spell.HasPendingCast
            //    && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            //{
            //    Task.Factory.StartNew(
            //        async () =>
            //        {
            //            _initSit = true;
            //            await Task.Delay(400);
            //            SMovementController.SetMovement(MovementAction.SwitchToSit);
            //            await Task.Delay(1800);
            //            SMovementController.SetMovement(MovementAction.LeaveSit);
            //            await Task.Delay(400);
            //            _initSit = false;
            //        });
            //}
        }

        //public static void HandleVicinityMessage(ChatMessageBody _msg)
        //{
        //    if (_msg.PacketType == ChatMessageType.VicinityMessage)
        //    {
        //        VicinityMessage _message = (VicinityMessage)_msg;

        //        SimpleChar _player = Team.Members.FirstOrDefault(c => _acceptedIDs.Contains(c.Identity) && c.Identity.Instance == (int)_message.Sender).Character;

        //        if (_player != null)
        //        {
        //            switch (_message.Text)
        //            {
        //                case "Faction - Omni":
        //                    if (DynelManager.LocalPlayer.Side == Side.Clan) 
        //                    {
        //                        Team.Leave();
        //                        return;
        //                    }

        //                    if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32())
        //                    {
        //                        _settings["FactionSelection"] = 2;
        //                        SettingsController.CleanUp();
        //                        Chat.WriteLine("Switching faction to Omni.");
        //                    }
        //                    break;
        //                case "Faction - Clan":
        //                    if (DynelManager.LocalPlayer.Side == Side.OmniTek)
        //                    {
        //                        Team.Leave();
        //                        return;
        //                    }

        //                    if (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32())
        //                    {
        //                        _settings["FactionSelection"] = 1;
        //                        SettingsController.CleanUp();
        //                        Chat.WriteLine("Switching faction to Clan.");
        //                    }
        //                    break;
        //                case "Faction - Neutral":
        //                    if (FactionSelection.Neutral != (FactionSelection)_settings["FactionSelection"].AsInt32())
        //                    {
        //                        if (_settings["OurFaction"].AsBool() 
        //                            && (DynelManager.LocalPlayer.Side == Side.OmniTek || DynelManager.LocalPlayer.Side == Side.Clan))
        //                        {
        //                            Team.Leave();
        //                            return;
        //                        }

        //                        _settings["FactionSelection"] = 0;
        //                        SettingsController.CleanUp();
        //                        Chat.WriteLine("Switching faction to Neutral.");
        //                    }
        //                    break;
        //                case "Difficulty - Hard":
        //                    _settings["DifficultySelection"] = 2;
        //                    SettingsController.CleanUp();
        //                    Chat.WriteLine("Switching difficulty to Hard.");
        //                    break;
        //                case "Difficulty - Medium":
        //                    _settings["DifficultySelection"] = 1;
        //                    SettingsController.CleanUp();
        //                    Chat.WriteLine("Switching difficulty to Medium.");
        //                    break;
        //                case "Difficulty - Easy":
        //                    _settings["DifficultySelection"] = 0;
        //                    SettingsController.CleanUp();
        //                    Chat.WriteLine("Switching difficulty to Easy.");
        //                    break;
        //                case "Mode - Roam":
        //                    if (ModeSelection.Leech == (ModeSelection)_settings["ModeSelection"].AsInt32()) 
        //                    {
        //                        Network.ChatMessageReceived -= (e, msg) => HandleVicinityMessage(msg);
        //                        _acceptedIDs.Remove((Identity)_player?.Identity);
        //                        return;
        //                    }
        //                    if (_settings["Merging"].AsBool()) { _settings["Merging"] = false; }
        //                    if (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
        //                    { _settings["DoubleReward"] = true; }
        //                    _settings["ModeSelection"] = 1;
        //                    Network.ChatMessageReceived -= (e, msg) => HandleVicinityMessage(msg);
        //                    _acceptedIDs.Remove((Identity)_player?.Identity);
        //                    SettingsController.CleanUp();
        //                    Chat.WriteLine("Switching mode to Roam.");
        //                    break;
        //                case "Mode - Defend":
        //                    if (ModeSelection.Leech == (ModeSelection)_settings["ModeSelection"].AsInt32()) 
        //                    {
        //                        Network.ChatMessageReceived -= (e, msg) => HandleVicinityMessage(msg);
        //                        _acceptedIDs.Remove((Identity)_player?.Identity);
        //                        return;
        //                    }
        //                    if (_settings["Merging"].AsBool()) { _settings["Merging"] = false; }
        //                    if (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32())
        //                    { _settings["DoubleReward"] = true; }
        //                    _settings["ModeSelection"] = 0;
        //                    Network.ChatMessageReceived -= (e, msg) => HandleVicinityMessage(msg);
        //                    _acceptedIDs.Remove((Identity)_player?.Identity);
        //                    SettingsController.CleanUp();
        //                    Chat.WriteLine("Switching mode to Defend.");
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //    }
        //}

        private void InfBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        if (Leader == Identity.None)
                        {
                            if (LookingSelection.Team != (LookingSelection)_settings["LookingSelection"].AsInt32())
                                Leader = DynelManager.LocalPlayer.Identity;
                        }

                        IPCChannel.Broadcast(new StartMessage());

                        _settings["Toggle"] = true;
                        SettingsController.CleanUp();
                        Start();
                    }
                    else
                    {
                        Stop();
                        _settings["Toggle"] = false;
                        IPCChannel.Broadcast(new StopMessage());
                    }
                }
                else
                {
                    switch (param[0].ToLower())
                    {
                        case "draw":
                            SMovementController.ToggleNavMeshDraw();
                            SMovementController.TogglePathDraw();
                            break;
                        case "time":
                            if (_missionTimeGuesstimation != TimeSpan.Zero)
                            {
                                Chat.WriteLine($"{_missionTimeGuesstimation} per reform.");
                            }
                            else
                            {
                                Chat.WriteLine($"Cannot calculate yet, wait for reform.");
                            }
                            break;
                        default:
                            break;
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public enum LookingSelection
        {
            None, Member, Team
        }
        public enum ModeSelection
        {
            Defend, Roam, Leech
        }
        public enum FactionSelection
        {
            Neutral, Clan, Omni
        }
        public enum DifficultySelection
        {
            Easy, Medium, Hard
        }

        private void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;
            LookingForTeam.Leave();

            Chat.WriteLine("Buddy disabled.");

            _stateMachine.SetState(new IdleState());

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (SMovementController.IsNavigating())
                SMovementController.Halt();
        }

        #endregion
    }
}
