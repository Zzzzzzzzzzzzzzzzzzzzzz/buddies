﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using static InfBuddy.InfBuddy;

namespace InfBuddy
{
    public static class Extensions
    {
        public static void AddRandomness(this ref Vector3 pos, int entropy)
        {
            pos.X += Next(-entropy, entropy);
            pos.Z += Next(-entropy, entropy);
        }
        public static int Next(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException("Min value is greater or equals than Max value.");
            }

            byte[] intBytes = new byte[4];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(intBytes);
            }

            return min + Math.Abs(BitConverter.ToInt32(intBytes, 0)) % (max - min + 1);
        }

        public static bool IsAtYutto()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestGiverPos) < 9f;
        }

        public static bool IsAtStarterPos()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestStarterPos) <= 4f;
        }

        public static bool HasDied()
        {
            return Playfield.ModelIdentity.Instance == Constants.OmniPandeGId
                || Playfield.ModelIdentity.Instance == Constants.ClanPandeGId
                || Playfield.ModelIdentity.Instance == Constants.XanHubId;
        }
        public static bool CanLoot(Corpse corpse)
        {
            if (corpse.DistanceFrom(DynelManager.LocalPlayer) <= 30f
                && (!_corpsesLooted.Contains(corpse?.Name.Remove(0, 10))
                    || !_corpsesPositions.Contains((Vector3)corpse?.Position))) { return true; }

            return false;
        }

        public static bool TimedOut(double _time, float _timeOut)
        {
            return Time.NormalTime > _time + _timeOut;
        }

        public static bool CanExit()
        {
            if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"))
                && Team.IsInTeam) { return true; }

            return false;
        }

        public static bool IsNull(SimpleChar _target)
        {
            return _target == null
                || _target?.IsPet == true
                || _target?.IsValid == false
                || _target?.Health == 0
                || (ModeSelection.Defend == (ModeSelection)_settings["ModeSelection"].AsInt32()
                    && _target.Position.DistanceFrom(Constants.DefendPosition) > 30f);
        }

        public static bool FindKit(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants.Kits.Contains(c.Id))) != null;
        }

        public static bool CanUseSitKit()
        {
            if (!IsCasting() && DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving)
            {
                if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if ((bool)sitKits?.Any()) { return true; }
            }

            return false;
        }

        public static bool BrokenCharmMobAttacking()
        {
            return _brokenCharmMob?.FightingTarget != null && (bool)_brokenCharmMob?.IsAttacking
                    && (Team.Members.Select(c => c.Identity).Any(c => (Identity)_brokenCharmMob?.FightingTarget.Identity == c)
                        || (bool)_brokenCharmMob.FightingTarget?.IsPet);
        }

        public static Corpse TryGetCorpse(out Corpse _corpse)
        {
            return _corpse = DynelManager.Corpses
                .Where(c => CanLoot(c) && !Team.Members.Select(m => m.Name).Contains(c.Name))
                .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                .FirstOrDefault();
        }

        public static bool IsFightingLeader(SimpleChar mob)
        {
            if (mob.FightingTarget == null) { return false; }

            if (Team.Members.Where(c => c.Identity == Leader).Select(c => c.Identity).Any(c => c == (Identity)mob.FightingTarget?.Identity)) { return true; }

            return false;
        }
        public static void HandleAttacking(SimpleChar target)
        {
            CharacterWieldedWeapon _wep = (CharacterWieldedWeapon)DynelManager.LocalPlayer.GetStat(Stat.EquippedWeapons);

            if (!SMovementController.IsNavigating() && target.IsInLineOfSight && target.Health > 0)
            {
                if (target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 5f
                    && (_wep.HasFlag(CharacterWieldedWeapon.Melee) || _wep.HasFlag(CharacterWieldedWeapon.MartialArts)))
                {
                    if (DynelManager.LocalPlayer.FightingTarget == null
                        && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                    {
                        if (target.Name == "Guardian Spirit of Purification") { target = null; return; }

                        DynelManager.LocalPlayer.Attack(target, false);
                        Chat.WriteLine($"Attacking {target.Name}.");
                    }
                }

                if (target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 17f && target.Health > 0
                    && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                {
                    if (DynelManager.LocalPlayer.FightingTarget == null
                        && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                    {
                        if (target.Name == "Guardian Spirit of Purification") { target = null; return; }

                        DynelManager.LocalPlayer.Attack(target, false);
                        Chat.WriteLine($"Attacking {target.Name}.");
                    }
                }
            }
        }

        public static void HandlePathing(SimpleChar target)
        {
            CharacterWieldedWeapon _wep = (CharacterWieldedWeapon)DynelManager.LocalPlayer.GetStat(Stat.EquippedWeapons);

            if (target?.IsInLineOfSight == true)
            {
                if (SMovementController.IsNavigating())
                {
                    if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 5f
                        && (_wep.HasFlag(CharacterWieldedWeapon.Melee) || _wep.HasFlag(CharacterWieldedWeapon.MartialArts)))
                        SMovementController.Halt();

                    if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                        && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                        SMovementController.Halt();
                }
            }
            else { SMovementController.SetNavDestination((Vector3)target?.Position); }

            if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 5f
                && (_wep.HasFlag(CharacterWieldedWeapon.Melee) || _wep.HasFlag(CharacterWieldedWeapon.MartialArts)))
                SMovementController.SetNavDestination((Vector3)target?.Position);

            if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                SMovementController.SetNavDestination((Vector3)target?.Position);
        }

        public static bool IsFlagged(SimpleChar target)
        {
            if (target.Buffs.Contains(216382) || target.Buffs.Contains(214879) || target.Buffs.Contains(284620) || target.Buffs.Contains(202732))
                return true;

            return false;
        }

        public static void LineOfSightCorrection(SimpleChar target)
        {
            if (target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f)
            {
                DynelManager.LocalPlayer.Position = (Vector3)target?.Position;
                SMovementController.SetMovement(MovementAction.Update);
                DynelManager.LocalPlayer.StopAttack();
            }
        }

        public static bool Rooted()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot);
        }

        public static bool TeamValid()
        {
            return !Team.Members.Any(c => c.Character != null && c.Character?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                    && c.Character?.Position.DistanceFrom(Constants.LeechPosition) > 2f);
        }
        public static bool SelfHealthy()
        {
            return DynelManager.LocalPlayer.HealthPercent >= 66 && DynelManager.LocalPlayer.NanoPercent >= 66;
        }
        public static bool TeamHealthy()
        {
            return Team.Members.Where(c => c.Character != null && c.Character.HealthPercent >= 66 && c.Character.NanoPercent >= 66).Count()
                == Team.Members.Where(c => c.Character != null).Count();
        }
        public static bool IsCasting()
        {
            if (_casting)
            {
                if (Time.NormalTime > _castTimer + _castRecharge)
                {
                    _casting = false;
                }
                else { return true; }
            }

            return false;
        }

        public static bool TryGetHealPet(out Pet healPet)
        {
            return (healPet = DynelManager.LocalPlayer.Pets.FirstOrDefault(c => c.Type == PetType.Heal)) != null;
        }

        public static bool PetNanoLow()
        {
            Pet _healPet;

            return (TryGetHealPet(out _healPet) && (_healPet.Character?.Nano / PetMaxNanoPool(_healPet) * 100 <= 55
                    && _healPet.Character?.Nano != 10
                    && DynelManager.LocalPlayer.DistanceFrom(_healPet.Character) < 10f && _healPet.Character?.IsInLineOfSight == true));
        }

        public static float PetMaxNanoPool(Pet _healPet)
        {
            if (_healPet.Character.Level == 215)
                return 5803;
            else if (_healPet.Character.Level == 192)
                return 13310;
            else if (_healPet.Character.Level == 169)
                return 11231;
            else if (_healPet.Character.Level == 146)
                return 9153;
            else if (_healPet.Character.Level == 123)
                return 7169;
            else if (_healPet.Character.Level == 99)
                return 5327;
            else if (_healPet.Character.Level == 77)
                return 3807;
            else if (_healPet.Character.Level == 55)
                return 2404;
            else if (_healPet.Character.Level == 33)
                return 1234;
            else if (_healPet.Character.Level == 14)
                return 414;

            return 0;
        }

        public static bool ShouldIdle()
        {
            if (Playfield.ModelIdentity.Instance == Constants.InfernoId
                && LookingSelection.Team == (LookingSelection)_settings["LookingSelection"].AsInt32()
                && (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(2730.1f, 25.4f, 3339.0f)) <= 2.7f || !Team.IsInTeam))
            { return true; }

            return false;
        }

        public static SimpleChar TryGetLeader(out SimpleChar _leader)
        {
            return _leader = Team.Members
                    .Where(c => c.Character?.Health > 0
                        && c.Identity != DynelManager.LocalPlayer.Identity
                        && c.Character?.IsValid == true
                        && (c.Identity == Leader || c.IsLeader || c.Character?.Name == LeaderName))
                    .FirstOrDefault()?.Character;
        }

        public static SimpleChar TryGetMob(out SimpleChar _mob)
        {
            return _mob = DynelManager.NPCs
                .Where(c => c.Health > 0
                    && c.IsValid
                    //&& c.IsInLineOfSight
                    && !Constants._ignores.Contains(c.Name) && !_charmMobs.Contains(c.Identity)
                    && c.Position.DistanceFrom(Constants.DefendPosition) <= ModeRange())
                .OrderBy(c => c.HealthPercent)
                .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                //.OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                //.ThenBy(c => c.HealthPercent)
                //.ThenBy(c => c.MaxHealth)
                //.OrderByDescending(c => c.MaxHealth)
                //.ThenBy(c => c.HealthPercent)
                //.ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                //.OrderBy(c => c.HealthPercent)
                //.ThenByDescending(c => c.MaxHealth)
                //.ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();
        }

        public static bool InGarden()
        {
            return Playfield.ModelIdentity.Instance == Constants.ClanPandeGId
                || Playfield.ModelIdentity.Instance == Constants.OmniPandeGId;
        }

        public static bool CanProceed()
        {
            return DynelManager.LocalPlayer.MovementState == MovementState.Run
                && !_initSit;
        }

        public static double ModeRange()
        {
            if (ModeSelection.Roam == (ModeSelection)_settings["ModeSelection"].AsInt32())
                return 85f;
            else if (ModeSelection.Defend == (ModeSelection)_settings["ModeSelection"].AsInt32())
                return 30f;

            return 0f;
        }

        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending) { return true; }

            if (DynelManager.Characters
                .Any(c => c.Health > 0 && !Constants._ignores.Contains(c.Name) && c.FightingTarget != null && !Constants._ignores.Contains(c.FightingTarget?.Name))) { return true; }

            return false;
        }

        [Flags]
        public enum CharacterWieldedWeapon
        {
            Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
            MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
            Melee = 0x02,             // 0x00000000000000000010b
            Ranged = 0x04,            // 0x00000000000000000100b
            Bow = 0x08,               // 0x00000000000000001000b
            Smg = 0x10,               // 0x00000000000000010000b
            Edged1H = 0x20,           // 0x00000000000000100000b
            Blunt1H = 0x40,           // 0x00000000000001000000b
            Edged2H = 0x80,           // 0x00000000000010000000b
            Blunt2H = 0x100,          // 0x00000000000100000000b
            Piercing = 0x200,         // 0x00000000001000000000b
            Pistol = 0x400,           // 0x00000000010000000000b
            AssaultRifle = 0x800,     // 0x00000000100000000000b
            Rifle = 0x1000,           // 0x00000001000000000000b
            Shotgun = 0x2000,         // 0x00000010000000000000b
            Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
            MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
            RangedEnergy = 0x10000,   // 0x00010000000000000000b
            Grenade2 = 0x20000,        // 0x00100000000000000000b
            HeavyWeapons = 0x40000,   // 0x01000000000000000000b
        }
    }
}
