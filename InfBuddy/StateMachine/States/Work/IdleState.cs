﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AOSharp.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using System.Collections.Generic;

namespace InfBuddy
{
    public class IdleState : IState
    {
        public static bool _init = false;

        private static bool _subbedEvents = false;

        public IState GetNextState()
        {
            if (!InfBuddy._settings["Toggle"].AsBool() || !Team.IsInTeam || Game.IsZoning || Time.NormalTime < InfBuddy._lastZonedTime + 2f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Playfield.ModelIdentity.Instance == Constants.InfernoId)
            {
                foreach (Mission mission in Mission.List)
                    if (mission.DisplayName.Contains("The Purification"))
                        mission.Delete();

                return new MoveToQuestGiverState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.XanReliquaryId)
            {
                if (InfBuddy._settings["DoubleReward"].AsBool() && !InfBuddy.EndMission && !InfBuddy.DoubleReward
                    && Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Ha...")))
                {
                    InfBuddy.DoubleReward = true;
                    InfBuddy.EndMission = true;
                }

                if (InfBuddy.ModeSelection.Leech == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                { return new LeechState(); }
                else if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                { return new RoamState(); }
                else if (InfBuddy.ModeSelection.Defend == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                {
                    Constants.DefendPosition = new Vector3(165.6f, 2.2f, 186.4f);
                    return new DefendSpiritState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");

            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(2730.1f, 25.4f, 3339.0f)) <= 2.7f)
            {
                SMovementController.Halt();
                DynelManager.LocalPlayer.Position = new Vector3(2726.0f, 24.6f, 3326.2f);
                SMovementController.SetMovement(MovementAction.Update);
            }

            InfBuddy._lastZonedTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");

            if (InfBuddy.LookingSelection.Team == (InfBuddy.LookingSelection)InfBuddy._settings["LookingSelection"].AsInt32())
            {
                if (_subbedEvents)
                {
                    _subbedEvents = false;
                    Team.TeamRequest -= OnTeamRequest;
                    LookingForTeam.Leave();
                }
            }
        }

        public void Tick()
        {
            //Dynel _statue = DynelManager.AllDynels
            //    .FirstOrDefault(c => c.Name.Contains("Exit"));

            //if (_statue != null) { Chat.WriteLine("yes"); }

            if (!InfBuddy._settings["Toggle"].AsBool()) { return; }

            if (InfBuddy.LookingSelection.Team == (InfBuddy.LookingSelection)InfBuddy._settings["LookingSelection"].AsInt32())
            {
                if (!_subbedEvents)
                {
                    _subbedEvents = true;
                    Team.TeamRequest += OnTeamRequest;

                    if (InfBuddy._settings["OurOrg"].AsBool())
                    {
                        if (InfBuddy.ModeSelection.Defend == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                        {
                            string _str = $"new inferno missions {(InfBuddy.DifficultySelection)InfBuddy._settings["DifficultySelection"].AsInt32()} {(InfBuddy.FactionSelection)InfBuddy._settings["FactionSelection"].AsInt32()} {DynelManager.LocalPlayer.GetStat(Stat.Clan)} Defend";

                            LookingForTeam.Join(_str);
                        }
                        else
                        {
                            string _str = $"new inferno missions {(InfBuddy.DifficultySelection)InfBuddy._settings["DifficultySelection"].AsInt32()} {(InfBuddy.FactionSelection)InfBuddy._settings["FactionSelection"].AsInt32()} {DynelManager.LocalPlayer.GetStat(Stat.Clan)}";

                            LookingForTeam.Join(_str);
                        }
                    }
                    else
                    {
                        if (InfBuddy.ModeSelection.Defend == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                        {
                            string _str = $"new inferno missions {(InfBuddy.DifficultySelection)InfBuddy._settings["DifficultySelection"].AsInt32()} {(InfBuddy.FactionSelection)InfBuddy._settings["FactionSelection"].AsInt32()} Defend";

                            LookingForTeam.Join(_str);
                        }
                        else
                        {
                            string _str = $"new inferno missions {(InfBuddy.DifficultySelection)InfBuddy._settings["DifficultySelection"].AsInt32()} {(InfBuddy.FactionSelection)InfBuddy._settings["FactionSelection"].AsInt32()}";

                            LookingForTeam.Join(_str);
                        }
                    }
                }
            }
        }

        private static void OnTeamRequest(object s, TeamRequestEventArgs e)
        {
            SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester);

            if (_player != null)
            {
                InfBuddy.Config.CharSettings[Game.ClientInst].Leader = _player?.Name;
                InfBuddy.Leader = (Identity)_player?.Identity;

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow.FindView("LeaderBox", out InfBuddy._leaderInput);

                    if (InfBuddy._leaderInput != null)
                        InfBuddy._leaderInput.Text = InfBuddy.Config.CharSettings[Game.ClientInst].Leader;
                }

                InfBuddy.Config.Save();
            }

            //if (InfBuddy._settings["OurOrg"].AsBool() || InfBuddy._settings["OurFaction"].AsBool())
            //{
            //    SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester);

            //    if (_player != null)
            //    {
            //        if ((InfBuddy._settings["OurOrg"].AsBool() && DynelManager.LocalPlayer.GetStat(Stat.Clan) != _player.GetStat(Stat.Clan))
            //            || (InfBuddy._settings["OurFaction"].AsBool() && DynelManager.LocalPlayer.Side != _player?.Side)) { return; }
            //        else { e.Accept(); }
            //    }
            //    else { e.Accept(); }
            //}

            LookingForTeam.Leave();
            e.Accept();

            //SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester);

            //if (_player != null)
            //{
            //    InfBuddy.Leader = _player.Identity;

            //    if (InfBuddy._settings["OurOrg"].AsBool() && DynelManager.LocalPlayer.GetStat(Stat.Clan) != _player.GetStat(Stat.Clan)) { return; }

            //    if (InfBuddy._settings["OurFaction"].AsBool() && DynelManager.LocalPlayer.Side != _player?.Side) { return; }

            //    InfBuddy._acceptedIDs.Add(e.Requester);
            //    e.Accept();
            //}
        }
    }
}
