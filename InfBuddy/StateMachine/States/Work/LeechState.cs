﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Linq;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class LeechState : IState
    {
        private static bool _initLeech = false;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Extensions.CanExit() && Time.NormalTime > InfBuddy._stateTimeOut + 3f) { return new ExitMissionState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("LeechState::OnStateEnter");

            InfBuddy._stateTimeOut = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("LeechState::OnStateExit");

            _initLeech = false;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (!_initLeech)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(160.4f, 2.6f, 103.0f)) > 12f)
                    SMovementController.SetNavDestination(new Vector3(160.4f, 2.6f, 103.0f));
                else
                {
                    _initLeech = true;
                    DynelManager.LocalPlayer.Position = Constants.LeechPosition;
                    SMovementController.SetMovement(MovementAction.Update);
                    SMovementController.SetMovement(MovementAction.JumpStart);
                    SMovementController.SetMovement(MovementAction.Update);
                }
            }
        }
    }
}
