﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Linq;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class MoveToQuestStarterState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < InfBuddy._lastZonedTime + 2f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (!SMovementController.IsNavigating() && Extensions.IsAtStarterPos())
            {
                if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader) { return new StartMissionState(); }

                Constants.DefendPosition = new Vector3(165.6f, 2.2f, 186.4f);
                SMovementController.SetNavDestination(Constants.DefendPosition);
                return new DefendSpiritState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("MoveToQuestStarter::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("MoveToQuestStarter::OnStateExit");
        }

        public void Tick()
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestStarterPos) > 4f)
            {
                SMovementController.SetNavDestination(Constants.QuestStarterPos, true);
            }
        }
    }
}
