﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace InfBuddy
{
    public class LootState : IState
    {
        private LootingPhase _phase;

        public IState GetNextState()
        {
            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (!DynelManager.Corpses.Any(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 30f)
                || Extensions.TryGetCorpse(out InfBuddy._corpse) == null
                || Time.NormalTime > InfBuddy._stateTimeOut + 7f)
            {
                if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                    return new RoamState();

                return new DefendSpiritState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Inventory.ContainerOpened += OnContainerOpened;

            InfBuddy._stateTimeOut = Time.NormalTime;

            _phase = LootingPhase.Scanning;

            //Chat.WriteLine("LootState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("LootState::OnStateExit");

            Inventory.ContainerOpened -= OnContainerOpened;
        }

        private void OnContainerOpened(object sender, Container container)
        {
            if (container.Identity.Type != IdentityType.Corpse)
                return;

            Corpse _corpse = DynelManager.Corpses.FirstOrDefault(c => c.Container.Identity == container.Identity);

            if (_corpse != null)
            {
                if (!InfBuddy._corpsesPositions.Contains(_corpse.Position))
                    InfBuddy._corpsesPositions.Add(_corpse.Position);

                if (!InfBuddy._corpsesLooted.Contains(_corpse.Name.Remove(0, 10)))
                    InfBuddy._corpsesLooted.Add(_corpse.Name.Remove(0, 10));
            }

            InfBuddy._stateTimeOut = Time.NormalTime;

            _phase = LootingPhase.Scanning;
        }

        public void Tick()
        {
            if (_phase == LootingPhase.Scanning)
            {
                if (Extensions.TryGetCorpse(out InfBuddy._corpse) != null) { _phase = LootingPhase.Looting; }
            }
            else if (_phase == LootingPhase.Looting)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)Extensions.TryGetCorpse(out InfBuddy._corpse)?.Position) >= 3f)
                    SMovementController.SetNavDestination((Vector3)Extensions.TryGetCorpse(out InfBuddy._corpse)?.Position);
                else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
            }
        }

        private enum LootingPhase
        {
            Scanning,
            Looting
        }
    }
}
