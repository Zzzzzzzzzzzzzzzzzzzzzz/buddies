﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace InfBuddy
{
    public class FightState : IState
    {
        public const double FightTimeout = 70f;

        private SimpleChar _target;

        private static bool _initLoot = false;

        public static double _timer;
        public static double _fightStartTime;

        public FightState(SimpleChar target)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < InfBuddy._lastZonedTime + 2f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Extensions.IsNull(_target) || Time.NormalTime > _fightStartTime + FightTimeout)
            {
                if (InfBuddy._settings["Looting"].AsBool())
                {
                    if (!DynelManager.NPCs.Any(c => c.Health > 0 && !Constants._ignores.Contains(c.Name) && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 30f))
                    {
                        if (!_initLoot)
                        {
                            _timer = Time.NormalTime;
                            _initLoot = true;
                        }

                        if (_initLoot && Time.NormalTime > _timer + 1.5f) { return new LootState(); }
                    }
                    else if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32()) { return new RoamState(); }
                }
                else if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32()) { return new RoamState(); }
                else { return new DefendSpiritState(); }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");

            _fightStartTime = Time.NormalTime;

            //Edge case correction: Stuck sitting.
            InfBuddy._initSit = false;
        }
        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            //Edge case correction: Stuck sitting.
            InfBuddy._initSit = false;

            _initLoot = false;
        }

        public void Tick()
        {
            if (Game.IsZoning || _target == null || InfBuddy._initSit || Time.NormalTime < InfBuddy._lastZonedTime + 2f) { return; }

            //if (!SMovementController.IsNavigating() && !(bool)_target?.IsInLineOfSight)
            //    Extensions.LineOfSightCorrection(_target);

            //if (InfBuddy._settings["Waiting"].AsBool())
            //{
            //    if (Team.Members.Any(c => c.Character != null && (c.Character.HealthPercent < 66 || c.Character.NanoPercent < 66)) && !Extensions.InCombat()) { return; }

            //    if (Team.Members.Any(c => c.Character != null && c.Character.Position.DistanceFrom(Constants.LeechPosition) > 3f && c.Character.Position.DistanceFrom(DynelManager.LocalPlayer.Position) >= 12f)
            //        && SMovementController.IsNavigating() && !Extensions.InCombat()) { SMovementController.Halt(); }

            //    if (!Team.Members.Any(c => c.Character != null && c.Character.Position.DistanceFrom(Constants.LeechPosition) > 3f && c.Character.Position.DistanceFrom(DynelManager.LocalPlayer.Position) >= 12f)
            //        || Extensions.InCombat())
            //        Extensions.HandlePathing(_target);
            //}
            //else { Extensions.HandlePathing(_target); }


            if (InfBuddy._settings["Waiting"].AsBool())
            {
                if (Extensions.InCombat() || (Extensions.TeamHealthy() && Extensions.TeamValid()))
                {
                    Extensions.HandlePathing(_target);
                }
                else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
            }
            else if (Extensions.InCombat() || Extensions.SelfHealthy())
            {
                Extensions.HandlePathing(_target);
            }

            //if (InfBuddy._settings["Waiting"].AsBool())
            //{
            //    if (Extensions.CanProceed() && !Extensions.IsCasting())
            //    {
            //        if (!Extensions.InCombat())
            //        {
            //            if (Extensions.TeamHealthy() && Extensions.TeamValid()) 
            //            { 
            //                Extensions.HandlePathing(_target);
            //            }
            //            else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
            //        }
            //        else { Extensions.HandlePathing(_target); }
            //    }
            //    else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
            //}
            //else
            //{
            //    Extensions.HandlePathing(_target);
            //}

            Extensions.HandleAttacking(_target);
        }
    }
}
