﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class StartMissionState : IState
    {
        private static double _timer;

        private static bool _init = false;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < InfBuddy._lastZonedTime + 2f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Extensions.IsAtStarterPos() && !SMovementController.IsNavigating()
                && !DynelManager.NPCs.Any(c => c.Name == Constants.QuestStarterName) 
                && DynelManager.NPCs.Any(c => c.Name == Constants.SpiritNPCName))
            {
                if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                    return new RoamState();

                Constants.DefendPosition = new Vector3(165.6f, 2.2f, 186.4f);
                return new DefendSpiritState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("StartMissionState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("StartMissionState::OnStateExit");

            _init = false;
        }

        public void Tick()
        {
            if (Game.IsZoning || Time.NormalTime < InfBuddy._lastZonedTime + 2f) { return; }

            Dynel _yutto = DynelManager.NPCs
                .FirstOrDefault(c => c.Name == Constants.QuestStarterName);

            //Edge case correction: Lag
            if (_init && Time.NormalTime > _timer + 10f && DynelManager.NPCs.Any(c => c.Name == Constants.QuestStarterName)) { _init = false; }

            if (_yutto != null && Extensions.IsAtStarterPos() && !_init) 
            {
                _init = true;
                NpcDialog.Open(_yutto);
                _timer = Time.NormalTime;
            }           
        }
    }
}
