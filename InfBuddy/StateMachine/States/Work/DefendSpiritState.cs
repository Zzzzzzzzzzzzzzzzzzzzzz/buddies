﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class DefendSpiritState : IState
    {
        private SimpleChar _target;

        private int _stuck = 0;

        private static bool _charmMobAttacked = false;
        private static double _charmMobAttacking;

        public DefendSpiritState() { }

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.CanExit() && Time.NormalTime > InfBuddy._stateTimeOut + 3f) { return new ExitMissionState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            #region Edge cases

            //Edge case correction: Dying before starting mission.
            if (Playfield.ModelIdentity.Instance == Constants.XanReliquaryId
                && DynelManager.NPCs.Any(c => c.Name == Constants.QuestStarterName)
                && DynelManager.LocalPlayer.Identity == InfBuddy.Leader) { return new MoveToQuestStarterState(); }

            //Edge case correction: Dying and getting back as mission is over.
            if (Playfield.ModelIdentity.Instance == Constants.InfernoId
                && Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"))
                && !Team.Members.Any(c => c.Character == null))
            {
                ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

                foreach (Mission mission in Mission.List)
                    if (mission.DisplayName.Contains("The Purification Ri"))
                        mission.Delete();

                return new ReformState();
            }

            #endregion

            if (_target != null) { return new FightState(_target); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("DefendSpiritState::OnStateEnter");

            InfBuddy._stateTimeOut = Time.NormalTime;
        }

        public void OnStateExit()
        {
            _stuck = 0;

            //Chat.WriteLine("DefendSpiritState::OnStateExit");
        }

        private void HandleCharmScan()
        {
            InfBuddy._charmMob = DynelManager.Characters
                .FirstOrDefault(c => !InfBuddy._charmMobs.Contains(c.Identity) && (c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)));

            InfBuddy._brokenCharmMob = DynelManager.Characters
                .FirstOrDefault(c => InfBuddy._charmMobs.Contains(c.Identity) && !c.Buffs.Contains(NanoLine.CharmOther) && !c.Buffs.Contains(NanoLine.Charm_Short));

            if (InfBuddy._charmMob != null && !InfBuddy._charmMobs.Contains(InfBuddy._charmMob.Identity))
                InfBuddy._charmMobs.Add((Identity)InfBuddy._charmMob?.Identity);

            if (_charmMobAttacked == true)
            {
                if (Time.NormalTime > _charmMobAttacking + 8f)
                {
                    if (Extensions.BrokenCharmMobAttacking())
                    {
                        _charmMobAttacked = false;

                        if (InfBuddy._charmMobs.Contains((Identity)InfBuddy._brokenCharmMob?.Identity))
                            InfBuddy._charmMobs.Remove((Identity)InfBuddy._brokenCharmMob?.Identity);
                    }
                    else { _charmMobAttacked = false; }
                }
            }
            else if (InfBuddy._brokenCharmMob != null)
            {
                _charmMobAttacking = Time.NormalTime;
                _charmMobAttacked = true;
            }
        }

        private void HandleScan()
        {
            if (Extensions.InCombat() || Extensions.CanProceed())
            {
                if (Extensions.TryGetMob(out InfBuddy._mob) != null)
                {
                    _target = InfBuddy._mob;
                    Chat.WriteLine($"Found target: {_target.Name}");
                    return;
                }
            }
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            #region Edge cases

            //Edge case correction - Heckler kick.
            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(178.6f, 5.1f, 195.2f)) < 3f)
            {
                SMovementController.Halt();
                DynelManager.LocalPlayer.Position = Constants.DefendPosition;
                SMovementController.SetMovement(MovementAction.Update);
            }

            if (InfBuddy._died && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) < 10f)
                InfBuddy._died = false;

            #endregion

            HandleCharmScan();

            //Edge case correction - Time out Somphos/Kolanna.
            if (Time.NormalTime > InfBuddy._stateTimeOut + 420f)
            {
                InfBuddy._stateTimeOut = Time.NormalTime;
                _stuck++;

                if (_stuck > 1)
                {
                    Mission _mission = Mission.List.FirstOrDefault(c => c.DisplayName.Contains("The Purification Ri"));
                    if (_mission != null) { Mission.List.Remove(_mission); }
                }
                else { SMovementController.SetNavDestination(new Vector3(181.3f, 1.0f, 245.6f)); }
            }
            else 
            {
                #region Scan

                HandleScan();

                #endregion

                if (Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                {
                    if (Extensions.CanProceed() && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) > 1f)
                    { SMovementController.SetNavDestination(Constants.DefendPosition); }
                }
            }
        }
    }
}
