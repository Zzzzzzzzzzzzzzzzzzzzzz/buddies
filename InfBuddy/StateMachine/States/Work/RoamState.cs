﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using InfBuddy.IPCMessages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class RoamState : IState
    {
        private SimpleChar _target;

        private int _stuck = 0;

        private static bool _timeOut = false;

        private static bool _charmMobAttacked = false;

        private static double _charmMobAttacking;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < InfBuddy._lastZonedTime + 2f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            #region Edge cases

            //Edge case correction: Dying before starting mission.
            if (Playfield.ModelIdentity.Instance == Constants.XanReliquaryId
                && DynelManager.NPCs.Any(c => c.Name == Constants.QuestStarterName)
                && DynelManager.LocalPlayer.Identity == InfBuddy.Leader) { return new MoveToQuestStarterState(); }

            //Edge case correction: Dying and getting back as mission is over.
            if (Playfield.ModelIdentity.Instance == Constants.InfernoId
                && Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"))
                && Team.IsInTeam
                && !Team.Members.Any(c => c.Character == null))
            {
                ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

                foreach (Mission mission in Mission.List)
                    if (mission.DisplayName.Contains("The Purification Ri"))
                        mission.Delete();

                return new ReformState();
            }

            #endregion 

            if (Extensions.CanExit()) return new ExitMissionState();

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (_target != null) { return new FightState(_target); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("RoamState::OnStateEnter");

            InfBuddy._stateTimeOut = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("RoamState::OnStateExit");

            //Edge case correction - Returning to defend position the moment before boss dies and mission updates.
            if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"))
                && SMovementController.IsNavigating()) { SMovementController.Halt(); }

            _stuck = 0;

            //Edge case correction: Stuck sitting.
            InfBuddy._initSit = false;
        }

        private void HandleScan()
        {
            if (_timeOut) { return; }

            if (Extensions.InCombat() || Extensions.CanProceed())
            {
                if (Extensions.TryGetMob(out InfBuddy._mob) != null)
                {
                    _target = InfBuddy._mob;
                    Chat.WriteLine($"Found target: {_target.Name}");
                    return;
                }
            }
        }

        private void HandleCharmScan()
        {
            InfBuddy._charmMob = DynelManager.Characters
                .FirstOrDefault(c => !InfBuddy._charmMobs.Contains(c.Identity) && (c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)));

            InfBuddy._brokenCharmMob = DynelManager.Characters
                .FirstOrDefault(c => InfBuddy._charmMobs.Contains(c.Identity) && !c.Buffs.Contains(NanoLine.CharmOther) && !c.Buffs.Contains(NanoLine.Charm_Short));

            if (InfBuddy._charmMob != null && !InfBuddy._charmMobs.Contains(InfBuddy._charmMob.Identity))
                InfBuddy._charmMobs.Add((Identity)InfBuddy._charmMob?.Identity);

            if (_charmMobAttacked == true)
            {
                if (Time.NormalTime > _charmMobAttacking + 8f)
                {
                    if (Extensions.BrokenCharmMobAttacking())
                    {
                        _charmMobAttacked = false;

                        if (InfBuddy._charmMobs.Contains((Identity)InfBuddy._brokenCharmMob?.Identity))
                            InfBuddy._charmMobs.Remove((Identity)InfBuddy._brokenCharmMob?.Identity);
                    }
                    else { _charmMobAttacked = false; }
                }
            }
            else if (InfBuddy._brokenCharmMob != null)
            {
                _charmMobAttacking = Time.NormalTime;
                _charmMobAttacked = true;
            }
        }

        public void Tick()
        {
            if (Game.IsZoning
                || Time.NormalTime < InfBuddy._lastZonedTime + 2f
                || !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"))
                || (Playfield.ModelIdentity.Instance == Constants.XanReliquaryId
                    && InfBuddy.LookingSelection.Team == (InfBuddy.LookingSelection)InfBuddy._settings["LookingSelection"].AsInt32()
                    && !Team.IsInTeam)) { return; }

            #region Edge cases

            //Edge case correction - Heckler kick.
            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(178.6f, 5.1f, 195.2f)) < 3f)
            {
                SMovementController.Halt();
                DynelManager.LocalPlayer.Position = Constants.DefendPosition;
                SMovementController.SetMovement(MovementAction.Update);
            }

            //Edge case correction - Time out Somphos/Kolanna.
            if (Time.NormalTime > InfBuddy._stateTimeOut + 60f)
            {
                if (!_timeOut) { _timeOut = true; }

                if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(181.3f, 1.0f, 245.6f)) < 2f)
                {
                    _timeOut = false;
                    InfBuddy._stateTimeOut = Time.NormalTime;
                    _stuck++;

                    if (_stuck > 1)
                    {
                        Mission _mission = Mission.List.FirstOrDefault(c => c.DisplayName.Contains("The Purification Ri"));
                        if (_mission != null) { Mission.List.Remove(_mission); }
                    }
                }
                else { SMovementController.SetNavDestination(new Vector3(181.3f, 1.0f, 245.6f), true); }
            }

            if (InfBuddy._died && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) < 10f)
                InfBuddy._died = false;

            #endregion

            HandleCharmScan();

            if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
            {
                if (InfBuddy._settings["Waiting"].AsBool())
                {
                    if (Extensions.TryGetLeader(out InfBuddy._leader) != null)
                    {
                        #region Scan

                        if (Extensions.TryGetLeader(out InfBuddy._leader)?.IsAttacking == true)
                        {
                            SimpleChar targetMob = DynelManager.NPCs
                                .FirstOrDefault(c => c.Health > 0
                                    && !Constants._ignores.Contains(c.Name)
                                    && c.Identity == (Identity)Extensions.TryGetLeader(out InfBuddy._leader)?.FightingTarget?.Identity);

                            if (targetMob != null)
                            {
                                _target = targetMob;
                                Chat.WriteLine($"Found target: {_target.Name}");
                                return;
                            }
                        }

                        #endregion

                        #region Pathing

                        if (Extensions.CanProceed() && !Extensions.IsCasting())
                        {
                            if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)Extensions.TryGetLeader(out InfBuddy._leader)?.Position) > 2f)
                                SMovementController.SetNavDestination((Vector3)Extensions.TryGetLeader(out InfBuddy._leader)?.Position);
                            else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                        }
                        else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }

                        #endregion
                    }
                    else
                    {
                        #region Scan

                        if (Extensions.InCombat())
                        {
                            if (Extensions.TryGetMob(out InfBuddy._mob) != null)
                            {
                                _target = InfBuddy._mob;
                                Chat.WriteLine($"Found target: {_target.Name}");
                                return;
                            }
                        }

                        #endregion

                        #region Pathing

                        if (Extensions.CanProceed() && !Extensions.IsCasting() && Extensions.TeamHealthy()
                            && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) > 1f)
                        {
                            SMovementController.SetNavDestination(Constants.DefendPosition);
                        }
                        else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }

                        #endregion
                    }
                }
                else
                {
                    #region Scan

                    HandleScan();

                    #endregion

                    #region Pathing

                    if (Extensions.CanProceed() 
                        && (Extensions.SelfHealthy() || Extensions.InCombat())
                        && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) > 1f)
                    {
                        SMovementController.SetNavDestination(Constants.DefendPosition);
                    }
                    else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }

                    #endregion
                }
            }
            else
            {
                #region Scan

                HandleScan();

                #endregion

                #region Pathing

                if (InfBuddy._settings["Waiting"].AsBool())
                {
                    if (_timeOut) { return; }

                    if (Extensions.CanProceed() && !Extensions.IsCasting() && Extensions.TeamHealthy() && Extensions.TeamValid()
                        && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) > 1f)
                    {
                        SMovementController.SetNavDestination(Constants.DefendPosition);
                    }
                    else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                }
                else
                {
                    if (_timeOut) { return; }

                    if (Extensions.CanProceed() && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) > 1f && (Extensions.SelfHealthy() || Extensions.InCombat()))
                    {
                        SMovementController.SetNavDestination(Constants.DefendPosition);
                    }
                    else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                }

                #endregion
            }
        }
    }
}
