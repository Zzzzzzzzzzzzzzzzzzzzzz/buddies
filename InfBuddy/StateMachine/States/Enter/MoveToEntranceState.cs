﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class MoveToEntranceState : IState
    {
        private const int _minWait = 5;
        private const int _maxWait = 7;

        private static double _timer;

        private static float _randomWait;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < InfBuddy._lastZonedTime + 1f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")) && Team.IsInTeam) { return new GrabMissionState(); }

            if (Playfield.ModelIdentity.Instance == Constants.XanReliquaryId)
            {
                if (InfBuddy.ModeSelection.Leech == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                { return new LeechState(); }
                else if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                {
                    if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
                    {
                        if (!Team.Members.Any(c => c.Character == null)
                            || InfBuddy.DoubleReward
                            || (!Team.Members.Any(c => c.Character != null && c.Character?.Position.DistanceFrom(new Vector3(163.1f, 2.0f, 111.2f)) < 5f)
                                && InfBuddy._merging)) { return new RoamState(); }

                        if (Time.NormalTime > InfBuddy._stateTimeOut + 25f)
                        {
                            if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPosition) > 2f) { SMovementController.SetNavDestination(Constants.DefendPosition, true); }
                            else { return new RoamState(); }
                        }
                    }
                    else if (!Team.Members.Any(c => c.Character == null) || Time.NormalTime > InfBuddy._stateTimeOut + 25f) { return new MoveToQuestStarterState(); }
                }
                else if (InfBuddy.ModeSelection.Defend == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                { return new MoveToQuestStarterState(); }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("MoveToEntranceState::OnStateEnter");

            InfBuddy._stateTimeOut = Time.NormalTime;

            int randomWait = Extensions.Next(_minWait, _maxWait);

            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader || InfBuddy._merging)
                randomWait = 1;

            float.TryParse($"{randomWait}", out _randomWait);

            Chat.WriteLine($"Idling for {_randomWait} seconds..");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("MoveToEntranceState::OnStateExit");
        }

        public void Tick()
        {
            if (Game.IsZoning || Playfield.ModelIdentity.Instance != Constants.InfernoId) { return; }

            if (Time.NormalTime > InfBuddy._stateTimeOut + _randomWait)
            {
                if (Time.NormalTime > _timer + 11f)
                {
                    SMovementController.SetNavDestination(Constants.EntrancePos, PathingType.Interpolated);

                    _timer = Time.NormalTime;
                }
            }
        }
    }
}
