﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class GrabMissionState : IState
    {
        private static double _timer;

        private static Vector3 _randoPos = Vector3.Zero;

        private static float _entropy = 1.34f;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"))) { return new MoveToEntranceState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("GrabMissionState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("GrabMissionState::OnStateExit");
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (!InfBuddy.EndMission && InfBuddy.DoubleReward) { InfBuddy.EndMission = true; }

            Dynel _yutto = DynelManager.NPCs
                .Where(c => c.Name == Constants.QuestGiverName)
                .FirstOrDefault();

            if (!Extensions.IsAtYutto() && !SMovementController.IsNavigating() && Time.NormalTime > _timer + 4f)
            {
                _randoPos = Constants.QuestGiverPos;
                _randoPos.AddRandomness((int)_entropy);

                SMovementController.SetNavDestination(_randoPos, PathingType.Interpolated);

                _timer = Time.NormalTime;
            }

            if (_yutto != null && Extensions.IsAtYutto() && Time.NormalTime > InfBuddy._stateTimeOut + 1f 
                && !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
            {
                NpcDialog.Open(_yutto);
            }
        }
    }
}
