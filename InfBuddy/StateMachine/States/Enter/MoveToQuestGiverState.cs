﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace InfBuddy
{
    public class MoveToQuestGiverState : IState
    {
        private const int _minWait = 2;
        private const int _maxWait = 4;

        private static float _randomWait;
        private static double _timer;

        private static Vector3 _randoPos = Vector3.Zero;

        private static float _entropy = 1.34f;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (!SMovementController.IsNavigating() && Extensions.IsAtYutto()) { return new GrabMissionState(); }
                
            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("MoveToQuestGiverState::OnStateEnter");

            InfBuddy._stateTimeOut = Time.NormalTime;

            int randomWait = Extensions.Next(_minWait, _maxWait);

            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader || InfBuddy._merging)
                randomWait = 1;

            float.TryParse($"{randomWait}", out _randomWait);

            Chat.WriteLine($"Idling for {randomWait} seconds..");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("MoveToQuestGiverState::OnStateExit");

            //InfBuddy._invitedIdentities.Clear();
            //InfBuddy._invitedNames.Clear();
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (!Extensions.IsAtYutto() && Time.NormalTime > InfBuddy._stateTimeOut + _randomWait)
            {
                if (Time.NormalTime > _timer + 11f)
                {
                    _randoPos = Constants.QuestGiverPos;
                    _randoPos.AddRandomness((int)_entropy);

                    SMovementController.SetNavDestination(_randoPos, PathingType.Interpolated);

                    _timer = Time.NormalTime;
                }
            }
        }
    }
}
