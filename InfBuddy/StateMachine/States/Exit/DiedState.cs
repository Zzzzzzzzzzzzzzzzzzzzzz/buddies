﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class DiedState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Playfield.ModelIdentity.Instance == Constants.PandePlatId) { return new ExitPandeState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("DiedState::OnStateEnter");

            InfBuddy._died = true;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("DiedState::OnStateExit");
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            Dynel _portal = DynelManager.AllDynels
                .FirstOrDefault(c => c.Name.Contains("Pandemonium Portal") && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 10f);

            if (_portal != null)
            {
                if (DynelManager.LocalPlayer.DistanceFrom(_portal) < 5f)
                {
                    Item _noviRing = Inventory.Items.FirstOrDefault(c => c.Name.Contains("Pure Novictum Ring"));

                    InfBuddy._initXanHub = true;

                    if (_noviRing != null) { _noviRing?.UseOn((Identity)_portal?.Identity); }                     
                }
            }
            else
            {
                if (Extensions.InGarden() && Extensions.CanProceed() && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
                    && DynelManager.LocalPlayer.HealthPercent >= 66)
                {
                    Dynel _statue = DynelManager.AllDynels
                        .FirstOrDefault(c => c.Name.Contains("Exit"));

                    if (_statue != null)
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(_statue.Position) <= 5f) { _statue?.Use(); }
                        else if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
                            SMovementController.SetNavDestination(_statue.Position);
                        else if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId
                            && DynelManager.LocalPlayer.Position.DistanceFrom(_statue.Position) > 5f)
                            SMovementController.SetNavDestination(_statue.Position);
                    }
                }
                else if (Playfield.ModelIdentity.Instance == Constants.XanHubId
                    && Extensions.CanProceed() && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
                    && DynelManager.LocalPlayer.HealthPercent >= 66 && DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(558.2f, 353.7f, 594.0f)) > 1.5f)
                {
                    SMovementController.SetNavDestination(new Vector3(558.2f, 353.7f, 594.0f));
                }
            }

            //if (Playfield.ModelIdentity.Instance == Constants.XanHubId
            //    && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
            //    && !InfBuddy._initSit
            //    && DynelManager.LocalPlayer.HealthPercent >= 66 && DynelManager.LocalPlayer.MovementState != MovementState.Sit
            //    && DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(558.2f, 353.7f, 594.0f)) > 1.5f)
            //    SMovementController.SetNavDestination(new Vector3(558.2f, 353.7f, 594.0f));

            //if (_statue != null && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)
            //    && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
            //    && !InfBuddy._initSit
            //    && DynelManager.LocalPlayer.MovementState != MovementState.Sit && DynelManager.LocalPlayer.HealthPercent >= 66)
            //{
            //    if (DynelManager.LocalPlayer.Position.DistanceFrom(_statue.Position) <= 5f)
            //        _statue.Use();
            //    else if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            //        SMovementController.SetNavDestination(_statue.Position);
            //    else if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId
            //        && DynelManager.LocalPlayer.Position.DistanceFrom(_statue.Position) > 5f)
            //        SMovementController.SetNavDestination(_statue.Position);
            //}
        }
    }
}
