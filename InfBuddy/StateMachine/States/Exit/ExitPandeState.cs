﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class ExitPandeState : IState
    {
        private static bool _initXan = false;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Playfield.ModelIdentity.Instance == Constants.InfernoId) { return new MoveToErgoState(); }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ExitPandeState::OnStateEnter");

            InfBuddy._stateTimeOut = Time.NormalTime;

            //Edge case correction: Stuck sitting.
            InfBuddy._initSit = false;

            if (_initXan) { SMovementController.SetNavDestination(new Vector3(171.7f, 25.6f, 61.0f), PathingType.Interpolated); }
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ExitPandeState::OnStateExit");

            _initXan = false;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            //Edge case correction: Stuck sitting.
            if (InfBuddy._stateTimeOut > 3f)
            {
                InfBuddy._initSit = false;

                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit && DynelManager.LocalPlayer.HealthPercent >= 66)
                { SMovementController.SetMovement(MovementAction.LeaveSit); }
            }

            if (Extensions.CanProceed() && Extensions.TeamHealthy()
                && DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(117.4f, 49.0f, 25.9f)) > 1.5f
                && !SMovementController.IsNavigating()) { SMovementController.SetNavDestination(new Vector3(117.4f, 49.0f, 25.9f)); }

            //Edge case correction: Zoning into the portal.
            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(117.4f, 49.0f, 25.9f)) <= 1.5f
                && !SMovementController.IsNavigating() && !DynelManager.LocalPlayer.IsMoving) { SMovementController.SetMovement(MovementAction.ForwardStart); }
        }
    }
}
