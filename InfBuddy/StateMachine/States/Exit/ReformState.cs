﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace InfBuddy
{
    public class ReformState : IState
    {
        private const float ReformTimeout = 60;
        private const float DisbandDelay = 23;

        private static double _reformStartedTime;

        public static bool Reforming = false;
        public static bool _leftTeam = false;

        private ReformPhase _phase;

        private static double _timer;

        public static List<Identity> _teamCache = new List<Identity>();
        private static List<Identity> _invitedList = new List<Identity>();

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.TimedOut(_reformStartedTime, ReformTimeout)) 
            {
                if (!Team.IsInTeam)
                {
                    if (Extensions.ShouldIdle()) { return new IdleState(); }
                }
                else if (Extensions.TimedOut(_reformStartedTime, ReformTimeout + 6f)) { return new MoveToQuestGiverState(); }
            }

            if (_phase == ReformPhase.Completed && Team.Members.Count() == _teamCache.Count())
            { return new MoveToQuestGiverState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ReformState::OnStateEnter");

            if (InfBuddy._previousRound != DateTime.MinValue)
            {
                InfBuddy._currentRound = DateTime.UtcNow;

                InfBuddy._missionTimeGuesstimation = InfBuddy._currentRound - InfBuddy._previousRound;

                InfBuddy._previousRound = DateTime.UtcNow;
            }
            else
            {
                InfBuddy._previousRound = DateTime.UtcNow;
            }

            Reforming = true;
            _reformStartedTime = Time.NormalTime;

            if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
            {
                Team.TeamRequest += OnTeamRequest;
                _phase = ReformPhase.Waiting;
                Chat.WriteLine("ReformPhase.Waiting");
            }
            else
            {
                _phase = ReformPhase.Disbanding;
                Chat.WriteLine("ReformPhase.Disbanding");
            }
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ReformState::OnStateExit");

            Reforming = false;
            _leftTeam = false;

            _invitedList.Clear();
            _teamCache.Clear();

            InfBuddy._corpsesPositions.Clear();
            InfBuddy._corpsesLooted.Clear();

            if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
                Team.TeamRequest -= OnTeamRequest;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            #region Waiting

            if (_phase == ReformPhase.Waiting && Time.NormalTime > _reformStartedTime + 13f && Playfield.ModelIdentity.Instance == Constants.InfernoId
                && Team.IsInTeam && !_leftTeam && InfBuddy.LookingSelection.Team == (InfBuddy.LookingSelection)InfBuddy._settings["LookingSelection"].AsInt32())
            {
                _leftTeam = true;
                Team.Leave();
            }

            #endregion

            #region Disbanding

            if (_phase == ReformPhase.Disbanding && Time.NormalTime > _reformStartedTime + DisbandDelay)
            {
                _phase = ReformPhase.Inviting;

                Team.Disband();
                _timer = Time.NormalTime;
                Chat.WriteLine("ReformPhase.Inviting");
            }

            #endregion

            #region Inviting

            if (_phase == ReformPhase.Inviting && Time.NormalTime > _reformStartedTime + DisbandDelay + 3f)
            {
                if (Team.IsInTeam && Team.Members.Count() == _teamCache.Count()
                    && _invitedList.Count() == _teamCache.Count())
                {
                    _phase = ReformPhase.Completed;
                    Chat.WriteLine("ReformPhase.Completed");
                }

                if (_invitedList.Count() < _teamCache.Count())
                {
                    if (Time.NormalTime > _timer + 2f)
                    {
                        _timer = Time.NormalTime;

                        foreach (SimpleChar player in DynelManager.Players.Where(c => !_invitedList.Contains(c.Identity) && _teamCache.Contains(c.Identity)).Take(3))
                        {
                            if (_invitedList.Contains(player.Identity)) { continue; }

                            _invitedList.Add(player.Identity);

                            if (player.Identity == InfBuddy.Leader) { continue; }

                            Team.Invite(player.Identity);
                            Chat.WriteLine($"Inviting {player.Name}");
                        }
                    }
                }
            }

            #endregion
        }

        #region Misc

        private void OnTeamRequest(object s, TeamRequestEventArgs e)
        {
            SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester && c.Name == InfBuddy.LeaderName);

            if (_teamCache.Contains(e.Requester))
            {
                if (e.Requester == InfBuddy.Leader || _player != null)
                {
                    _phase = ReformPhase.Completed;
                    e.Accept();
                }
            }
        }

        private enum ReformPhase
        {
            Disbanding,
            Inviting,
            Waiting,
            Completed
        }

        #endregion
    }
}
