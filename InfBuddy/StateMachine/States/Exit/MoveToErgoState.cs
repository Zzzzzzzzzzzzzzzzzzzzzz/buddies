﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class MoveToErgoState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            //Edge case correction: Dying and getting back as mission is over.
            if (Playfield.ModelIdentity.Instance == Constants.InfernoId
                && Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"))
                && Team.IsInTeam
                && !Team.Members.Any(c => c.Character == null))
            {
                ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

                foreach (Mission mission in Mission.List)
                    if (mission.DisplayName.Contains("The Purification Ri"))
                        mission.Delete();

                return new ReformState();
            }

            //Edge case correction: Dying and getting back as mission is over.
            if (Playfield.ModelIdentity.Instance == Constants.XanReliquaryId)
            {
                if (InfBuddy.ModeSelection.Leech == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32()) { return new LeechState(); }
                else if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())  { return new RoamState(); }

                return new DefendSpiritState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("MoveToErgoState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("MoveToErgoState::OnStateExit");
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            SMovementController.SetNavDestination(Constants.EntrancePos, PathingType.Interpolated, true);
        }
    }
}
