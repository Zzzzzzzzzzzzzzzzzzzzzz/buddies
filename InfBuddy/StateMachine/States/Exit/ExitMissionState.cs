﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class ExitMissionState : IState
    {
        private static float _timer = 30f;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Playfield.ModelIdentity.Instance == Constants.InfernoId)
            {
                if (InfBuddy._settings["DoubleReward"].AsBool() && !InfBuddy.DoubleReward && !InfBuddy.EndMission)
                {
                    InfBuddy.DoubleReward = true;
                    return new MoveToQuestGiverState();
                }
                else if (InfBuddy.DoubleReward && InfBuddy.EndMission)
                {
                    InfBuddy.DoubleReward = false;
                    InfBuddy.EndMission = false;
                }

                return new ReformState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ExitMissionState::OnStateEnter");

            ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

            if (InfBuddy._settings["DoubleReward"].AsBool() && !InfBuddy.DoubleReward)
                _timer = 1f;

            InfBuddy._stateTimeOut = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ExitMissionState::OnStateExit");
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (Time.NormalTime > InfBuddy._stateTimeOut + _timer)
            {
                InfBuddy._stateTimeOut = Time.NormalTime;

                if (InfBuddy.ModeSelection.Leech == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                {
                    DynelManager.LocalPlayer.Position = Constants.LeechMissionExit;
                    SMovementController.SetMovement(MovementAction.Update);
                }

                SMovementController.SetNavDestination(Constants.ExitPos);
            }
        }
    }
}
