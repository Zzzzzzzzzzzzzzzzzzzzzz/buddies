﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using static KHBuddy.KHBuddy;

namespace KHBuddy
{
    public static class Extensions
    {
        public static void AddRandomness(this ref Vector3 pos, int entropy)
        {
            pos.X += Next(-entropy, entropy);
            pos.Z += Next(-entropy, entropy);
        }

        public static int Next(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException("Min value is greater or equals than Max value.");
            }

            byte[] intBytes = new byte[4];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(intBytes);
            }

            return min + Math.Abs(BitConverter.ToInt32(intBytes, 0)) % (max - min + 1);
        }
        public static bool FindKit(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants.Kits.Contains(c.Id))) != null;
        }

        public static bool CanUseSitKit()
        {
            if (DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving)
            {
                if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if ((bool)sitKits?.Any()) { return true; }
            }

            return false;

            //if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)
            //    && DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving && !Game.IsZoning
            //    && !DynelManager.LocalPlayer.Buffs.Contains(280488))
            //{
            //    if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

            //    List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

            //    if (sitKits.Any())
            //    {
            //        foreach (Item sitKit in sitKits.OrderBy(x => x.QualityLevel))
            //        {
            //            int skillReq = (sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f));

            //            if (DynelManager.LocalPlayer.GetStat(Stat.FirstAid) >= skillReq || DynelManager.LocalPlayer.GetStat(Stat.Treatment) >= skillReq)
            //                return true;
            //        }
            //    }
            //}

            //return false;
        }

        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending
                || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }
    }
}
