﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using KHBuddy.IPCMessages;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;
using AOSharp.Common.GameData.UI;
using AOSharp.Core.Misc;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;

namespace KHBuddy
{
    public class KHBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;
        public static Item _kit;

        public static string BureaucratsName;

        public static Settings _settings = new Settings("KHBuddy");

        public static double _timer = 0f;
        public static float Tick = 0;

        public static DateTime RespawnTime;
        public static DateTime RespawnTimeEast;
        public static DateTime RespawnTimeWest;
        public static DateTime GameTime;

        public static double _stateTimeOut = Time.NormalTime;

        private static double _sitUpdateTimer;
        private static double _mainUpdate;

        public static bool _doingEast = false;
        public static bool _doingWest = false;
        public static bool Toggle = false;
        public static bool _init = false;
        public static bool _initSit = false;

        public static Spell _mongoDummySpell;

        public static bool _subbedUIEvents = false;

        public static string PluginDirectory;

        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("KHBuddy Loaded! Version: 0.9.9.91");
                Chat.WriteLine("/khbuddy for settings.");

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\KHBuddy\\{Game.ClientInst}\\Config.json");
                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.StartMode, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopMode, OnStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.MoveEast, OnMoveEastMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.MoveWest, OnMoveWestMessage);

                //Chat.RegisterCommand("buddy", KHBuddyCommand);

                Game.OnUpdate += OnUpdate;

                _settings.AddVariable("Toggle", false);

                _settings["Toggle"] = false;

                _settings.AddVariable("SideSelection", (int)SideSelection.East);

                SettingsController.RegisterSettingsWindow("KHBuddy", pluginDir + "\\UI\\KHBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                BureaucratsName = Config.CharSettings[Game.ClientInst].BureaucratsName;
                Tick = Config.CharSettings[Game.ClientInst].Tick;

                PluginDirectory = pluginDir;
            }
            catch(Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        public static void BureaucratsName_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].BureaucratsName = e;
            BureaucratsName = e;
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }
        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            StartModeMessage startMsg = (StartModeMessage)msg;

            _settings["SideSelection"] = startMsg.Side;

            _settings["Toggle"] = true;

            Start();
        }

        private void OnMoveEastMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1091.7f, 26.5f, 1051.4f)) > 1f && !MovementController.Instance.IsNavigating)
            {
                MovementController.Instance.SetDestination(new Vector3(1091.7f, 26.5f, 1051.4f));
            }
        }

        private void OnMoveWestMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1064.4f, 25.6f, 1032.6f)) > 1f && !MovementController.Instance.IsNavigating)
            {
                MovementController.Instance.SetDestination(new Vector3(1064.4f, 25.6f, 1032.6f));
            }
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            StopModeMessage stopMsg = (StopModeMessage)msg;

            Toggle = false;

            _settings["SideSelection"] = stopMsg.Side;

            _settings["Toggle"] = false;

            if (NavMeshMovementController != null)
                NavMeshMovementController.Halt();
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        private void Start()
        {
            Toggle = true;

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer && !(_stateMachine.CurrentState is PullState))
                _stateMachine.SetState(new PullState());

            if (DynelManager.LocalPlayer.Profession == Profession.NanoTechnician && !(_stateMachine.CurrentState is NukeState))
                _stateMachine.SetState(new NukeState());
        }

        private void Stop()
        {
            Toggle = false;

            _stateMachine.SetState(new IdleState());

            if (NavMeshMovementController != null)
                NavMeshMovementController.Halt();
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            Window helpWindow = Window.CreateFromXml("Info", PluginDirectory + "\\UI\\KHBuddyInfoView.xml",
            windowSize: new Rect(0, 0, 250, 345),
            windowStyle: WindowStyle.Default,
            windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);
            helpWindow.Show(true);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning) { return; }

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                _mainUpdate = Time.NormalTime;
                _stateMachine.Tick();

                ListenerSit();
            }

            #region UI Update

            if (_subbedUIEvents && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                if (SideSelection.EastAndWest == (SideSelection)_settings["SideSelection"].AsInt32())
                {
                    Debug.DrawSphere(new Vector3(1115.9f, 1.6f, 1064.3f), 0.2f, DebuggingColor.White);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, new Vector3(1115.9f, 1.6f, 1064.3f), DebuggingColor.White); // East
                }

                if (SideSelection.East == (SideSelection)_settings["SideSelection"].AsInt32())
                {
                    Debug.DrawSphere(new Vector3(1115.9f, 1.6f, 1064.3f), 0.2f, DebuggingColor.White);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, new Vector3(1115.9f, 1.6f, 1064.3f), DebuggingColor.White); // East
                }

                if (SideSelection.West == (SideSelection)_settings["SideSelection"].AsInt32())
                {
                    Debug.DrawSphere(new Vector3(1043.2f, 1.6f, 1021.1f), 0.2f, DebuggingColor.White);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, new Vector3(1043.2f, 1.6f, 1021.1f), DebuggingColor.White); // West
                }

                if (SideSelection.Beach == (SideSelection)_settings["SideSelection"].AsInt32())
                {
                    Debug.DrawSphere(new Vector3(898.1f, 4.4f, 289.9f), 0.2f, DebuggingColor.White);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, new Vector3(898.1f, 4.4f, 289.9f), DebuggingColor.White); // Beach
                }
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                SettingsController.settingsWindow.FindView("BureaucratsNameBox", out TextInputView bureaucratInput);
                SettingsController.settingsWindow.FindView("TickBox", out TextInputView tickInput);

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text)
                    && int.TryParse(channelInput.Text, out int channelValue)
                    && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text)
                    && float.TryParse(tickInput.Text, out float tickValue)
                    && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                    Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (bureaucratInput != null && !string.IsNullOrEmpty(bureaucratInput.Text)
                    && Config.CharSettings[Game.ClientInst].BureaucratsName != bureaucratInput.Text)
                    Config.CharSettings[Game.ClientInst].BureaucratsName = bureaucratInput.Text;

                if (SettingsController.settingsWindow.FindView("KHBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && Toggle == false
                    && DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
                {
                    if (SideSelection.East == (SideSelection)_settings["SideSelection"].AsInt32())
                        MovementController.Instance.SetDestination(new Vector3(1090.2f, 28.1f, 1050.1f));
                    else if (SideSelection.West == (SideSelection)_settings["SideSelection"].AsInt32())
                        MovementController.Instance.SetDestination(new Vector3(1065.4f, 26.2f, 1033.5f));
                    else if (SideSelection.EastAndWest == (SideSelection)_settings["SideSelection"].AsInt32())
                        MovementController.Instance.SetDestination(new Vector3(1090.2f, 28.1f, 1050.1f));

                    Start();
                }

                if (_settings["Toggle"].AsBool() && Toggle == false
                    && DynelManager.LocalPlayer.Profession == Profession.Enforcer)
                {
                    if (Leader == Identity.None)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;
                    }

                    IPCChannel.Broadcast(new StartModeMessage()
                    {
                        Side = (int)(SideSelection)_settings["SideSelection"].AsInt32()
                    });

                    if (SideSelection.EastAndWest == (SideSelection)_settings["SideSelection"].AsInt32())
                        _doingEast = true;

                    Start();
                }

                if (!_settings["Toggle"].AsBool() && Toggle == true)
                {
                    IPCChannel.Broadcast(new StopModeMessage()
                    {
                        Side = (int)(SideSelection)_settings["SideSelection"].AsInt32()
                    });

                    Stop();
                }
            }

            #endregion
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].BureaucratsNameChangedEvent -= BureaucratsName_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;


                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].BureaucratsNameChangedEvent += BureaucratsName_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        private void ListenerSit()
        {
            if (Extensions.CanUseSitKit())
            {
                if (DynelManager.LocalPlayer.Profession == Profession.Enforcer 
                    && Extensions.InCombat()) { return; }

                if (_initSit)
                {
                    if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                    {
                        if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66)
                        {
                            _initSit = false;
                            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                        }
                    }
                    else { _initSit = false; }
                }
                else if (!LocalCooldown.IsOnCooldown(Stat.Treatment))
                {
                    if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
                    {
                        _initSit = true;
                        MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                    }
                    else if (DynelManager.LocalPlayer.MovementState == MovementState.Sit) { MovementController.Instance.SetMovement(MovementAction.LeaveSit); }
                }
            }

            //if (_initSit == false && Extensions.CanUseSitKit()
            //    && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            //{
            //    Task.Factory.StartNew(
            //        async () =>
            //        {
            //            _initSit = true;
            //            await Task.Delay(400);
            //            MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //            await Task.Delay(1800);
            //            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //            await Task.Delay(400);
            //            _initSit = false;
            //        });
            //}
        }

        //private void KHBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        //{
        //    try
        //    {
        //        if (param.Length < 1)
        //        {
        //            if (!KHBuddySettings["West"].AsBool() && !KHBuddySettings["East"].AsBool()
        //                && !KHBuddySettings["Beach"].AsBool())
        //            {
        //                KHBuddySettings["West"] = false;
        //                KHBuddySettings["East"] = false;
        //                KHBuddySettings["Beach"] = false;
        //                KHBuddySettings["BothSides"] = false;

        //                Chat.WriteLine($"Can only toggle one mode.");
        //                return;
        //            }

        //            if (KHBuddySettings["West"].AsBool() && KHBuddySettings["Beach"].AsBool()
        //                || (KHBuddySettings["East"].AsBool() && KHBuddySettings["Beach"].AsBool()))
        //            {
        //                KHBuddySettings["West"] = false;
        //                KHBuddySettings["East"] = false;
        //                KHBuddySettings["Beach"] = false;
        //                KHBuddySettings["BothSides"] = false;

        //                Chat.WriteLine($"Can only toggle one mode.");
        //                return;
        //            }

        //            if (!KHBuddySettings["Toggle"].AsBool() && !_started)
        //            {
        //                if (KHBuddySettings["West"].AsBool() && KHBuddySettings["East"].AsBool()
        //                    && !KHBuddySettings["Beach"].AsBool())
        //                {
        //                    IsLeader = true;
        //                    Leader = DynelManager.LocalPlayer.Identity;

        //                    KHBuddySettings["BothSides"] = true;

        //                    if (DynelManager.LocalPlayer.Identity == Leader)
        //                    {
        //                        if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 5f) // East
        //                        {
        //                            KHBuddySettings["West"] = false;

        //                            IPCChannel.Broadcast(new StartModeMessage()
        //                            {
        //                                West = KHBuddySettings["West"].AsBool(),
        //                                East = KHBuddySettings["East"].AsBool(),
        //                                Beach = KHBuddySettings["Beach"].AsBool(),
        //                                BothSides = KHBuddySettings["BothSides"].AsBool()
        //                            });
        //                        }

        //                        if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1021.1f)) < 5f) // West
        //                        {
        //                            KHBuddySettings["East"] = false;

        //                            IPCChannel.Broadcast(new StartModeMessage()
        //                            {
        //                                West = KHBuddySettings["West"].AsBool(),
        //                                East = KHBuddySettings["East"].AsBool(),
        //                                Beach = KHBuddySettings["Beach"].AsBool(),
        //                                BothSides = KHBuddySettings["BothSides"].AsBool()
        //                            });
        //                        }
        //                    }
        //                    Start();
        //                    Chat.WriteLine("Starting");
        //                    return;
        //                }
        //                else
        //                {
        //                    IsLeader = true;
        //                    Leader = DynelManager.LocalPlayer.Identity;

        //                    KHBuddySettings["BothSides"] = false;

        //                    if (DynelManager.LocalPlayer.Identity == Leader)
        //                    {
        //                        IPCChannel.Broadcast(new StartModeMessage()
        //                        {
        //                            West = KHBuddySettings["West"].AsBool(),
        //                            East = KHBuddySettings["East"].AsBool(),
        //                            Beach = KHBuddySettings["Beach"].AsBool(),
        //                            BothSides = KHBuddySettings["BothSides"].AsBool()
        //                        });
        //                    }
        //                    Start();
        //                    Chat.WriteLine("Starting");
        //                    return;
        //                }
        //            }
        //            else if (KHBuddySettings["Toggle"].AsBool() && _started)
        //            {
        //                if (DynelManager.LocalPlayer.Identity == Leader)
        //                {
        //                    IPCChannel.Broadcast(new StopModeMessage()
        //                    {
        //                        West = KHBuddySettings["West"].AsBool(),
        //                        East = KHBuddySettings["East"].AsBool(),
        //                        Beach = KHBuddySettings["Beach"].AsBool(),
        //                        BothSides = KHBuddySettings["BothSides"].AsBool()
        //                    });
        //                }

        //                Stop();
        //                Chat.WriteLine("Stopping");
        //                return;
        //            }
        //        }
        //        Config.Save();
        //    }
        //    catch (Exception e)
        //    {
        //        Chat.WriteLine(e.Message);
        //    }
        //}

        public enum SideSelection
        {
            Beach, East, West, EastAndWest
        }
    }
}
