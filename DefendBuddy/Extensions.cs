﻿using AOSharp.Common.GameData;
using System.Security.Cryptography;
using System;
using AOSharp.Core;
using System.Linq;
using AOSharp.Core.Inventory;
using System.Collections.Generic;
using static DefendBuddy.DefendBuddy;
using AOSharp.Core.Movement;

namespace DefendBuddy
{
    public static class Extensions
    {
        public static void AddRandomness(this ref Vector3 pos, int entropy)
        {
            pos.X += Next(-entropy, entropy);
            pos.Z += Next(-entropy, entropy);
        }
        public static int Next(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException("Min value is greater or equals than Max value.");
            }

            byte[] intBytes = new byte[4];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(intBytes);
            }

            return min + Math.Abs(BitConverter.ToInt32(intBytes, 0)) % (max - min + 1);
        }
        public static bool AttackingTeam(SimpleChar mob)
        {
            if (mob?.FightingTarget == null || _helpers.Contains(mob.FightingTarget?.Name)) { return true; }

            //if (mob?.FightingTarget?.Name == "Guardian Spirit of Purification"
            //    || mob?.FightingTarget?.Name == "Rookie Alien Hunter"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Alpha"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Delta"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Gamma"
            //    || RoamBuddy._helpers.Contains(mob.FightingTarget?.Name)) { return true; }

            if (Team.IsInTeam) { return Team.Members.Select(m => m.Name).Contains(mob.FightingTarget?.Name); }

            return mob.FightingTarget?.Name == DynelManager.LocalPlayer.Name;
        }

        public static float PetMaxNanoPool(SimpleChar _healPet)
        {
            if (_healPet.Level == 215)
                return 5803;
            else if (_healPet.Level == 192)
                return 13310;
            else if (_healPet.Level == 169)
                return 11231;
            else if (_healPet.Level == 146)
                return 9153;
            else if (_healPet.Level == 123)
                return 7169;
            else if (_healPet.Level == 99)
                return 5327;
            else if (_healPet.Level == 77)
                return 3807;
            else if (_healPet.Level == 55)
                return 2404;
            else if (_healPet.Level == 33)
                return 1234;
            else if (_healPet.Level == 14)
                return 414;

            return 0;
        }

        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending
                || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && (Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name)
                            || DefendBuddy._helpers.Contains(c.FightingTarget?.Name)));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }
        public static bool Checks(SimpleChar mob)
        {
            if (mob.Name == "Alien Coccoon" && (mob.Level == 250 || mob.Level == 234)) { return false; }

            if (Playfield.ModelIdentity.Instance == 6055 && mob.Name == "Masked Commando") { return false; }

            return true;
        }

        public static bool IsBoss(SimpleChar _boss)
        {
            return _boss?.MaxHealth >= 1200000 || _boss?.Name == "Kyr'Ozch Maid" || _boss?.Name == "Kyr'Ozch Technician"
                        || _boss?.Name == "Masked Commando Superior"
                        || _boss?.Name == "Defense Drone Tower"
                        || _boss?.Name == "Sraosha" || _boss?.Name == "Desert Commander" || _boss?.Name == "Desert Warden"
                        || _boss?.Name == "Prototype Infiltration Unit";
        }

        public static bool CanUseSitKit(out Item _kit)
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(280488) || !Spell.List.Any(c => c.IsReady))
            {
                _kit = null;
                return false;
            }

            if (DynelManager.LocalPlayer.Health > 0 && !InCombat()
                    && !DynelManager.LocalPlayer.IsMoving && !Game.IsZoning)
            {
                if (Inventory.Find(297274, out _kit)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if (!sitKits.Any()) { return false; }

                foreach (Item sitKit in sitKits.OrderBy(x => x.QualityLevel))
                {
                    int skillReq = (sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f));

                    if (DynelManager.LocalPlayer.GetStat(Stat.FirstAid) >= skillReq || DynelManager.LocalPlayer.GetStat(Stat.Treatment) >= skillReq)
                    {
                        _kit = sitKit;
                        return true;
                    }
                }
            }

            _kit = null;
            return false;
        }
        public static void HandlePathing(SimpleChar target)
        {
            if (MovementController.Instance.IsNavigating && target?.IsInLineOfSight == true)
            {
                if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= DefendBuddy.Config.CharSettings[Game.ClientInst].AttackRange)
                    MovementController.Instance.Halt();
            }

            if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > DefendBuddy.Config.CharSettings[Game.ClientInst].AttackRange)
                MovementController.Instance.SetDestination((Vector3)target?.Position);
        }

        public static SimpleChar TryGetLeader()
        {
            return DynelManager.Players
                .FirstOrDefault(c => c.Identity == Leader
                    && c.Identity != DynelManager.LocalPlayer.Identity
                    && c.IsValid
                    && c.Health > 0
                    && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301));
        }

        public static SimpleChar TryGetLeader(Identity leader)
        {
            if (!DynelManager.Players
                .Any(c => c.Identity == leader
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 45f
                    && c.IsValid
                    && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301)))
                return DynelManager.LocalPlayer;

            return DynelManager.Players
                .FirstOrDefault(c => c.Identity == leader
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 45f
                    && c.IsValid
                    && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301));
        }

        public static bool IsNull(SimpleChar _target)
        {
            return _target == null
                || _target?.Health == 0
                || _target?.IsValid == false
                || _target?.IsInLineOfSight == false
                || (DynelManager.LocalPlayer.FightingTarget != null && DynelManager.LocalPlayer.FightingTarget.Name != _target.Name);
        }

        public static bool ShouldStopAttack()
        {
            if (DynelManager.LocalPlayer.FightingTarget == null
                || (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(RelevantNanos.ShovelBuffs) == true
                    && DynelManager.LocalPlayer.FightingTarget?.Name.Contains("Aune") == true)) { return false; }

            if (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(253953) == true
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(273220) == true
                || DynelManager.LocalPlayer.Buffs.Contains(274101)
                || (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(RelevantNanos.ShovelBuffs) == true
                    && DynelManager.LocalPlayer.FightingTarget?.Name != "Sraosha")
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(302745) == true
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(205607) == true
                || DynelManager.LocalPlayer.FightingTarget?.IsPlayer == true) { return true; }

            return false;
        }

        public static bool BrokenCharmMobAttacking()
        {
            return _brokenCharmMob?.FightingTarget != null && (bool)_brokenCharmMob?.IsAttacking
                    && (Team.Members.Select(c => c.Identity).Any(c => (Identity)_brokenCharmMob?.FightingTarget.Identity == c)
                        || (bool)_brokenCharmMob.FightingTarget?.IsPet);
        }

        public static bool FindKit(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants.Kits.Contains(c.Id))) != null;
        }

        public static bool FindTool(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants._toolNames.Contains(c.Name))) != null;
        }

        public static bool ShouldTaunt(SimpleChar _target)
        {
            return _target?.IsInLineOfSight == true
                && !DynelManager.LocalPlayer.IsMoving
                && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending;
        }

        public static bool Rooted()
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot)) { return true; }

            return false;
        }
        public static bool CanAttack()
        {
            return DynelManager.LocalPlayer.FightingTarget == null
                    && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending;
        }

        public static bool CanAttack(SimpleChar target)
        {
            if (target.Name.Contains("Phobettor") || target.Name.Contains("Azi Dahaka")) { return true; }

            if (!target.Buffs.Contains(RelevantNanos.ShovelBuffs) && !target.Buffs.Contains(302745)
                && !target.Buffs.Contains(253953) && !target.Buffs.Contains(205607) && !target.Buffs.Contains(273220)) { return true; }

            return false;
        }

        [Flags]
        public enum CharacterWieldedWeapon
        {
            Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
            MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
            Melee = 0x02,             // 0x00000000000000000010b
            Ranged = 0x04,            // 0x00000000000000000100b
            Bow = 0x08,               // 0x00000000000000001000b
            Smg = 0x10,               // 0x00000000000000010000b
            Edged1H = 0x20,           // 0x00000000000000100000b
            Blunt1H = 0x40,           // 0x00000000000001000000b
            Edged2H = 0x80,           // 0x00000000000010000000b
            Blunt2H = 0x100,          // 0x00000000000100000000b
            Piercing = 0x200,         // 0x00000000001000000000b
            Pistol = 0x400,           // 0x00000000010000000000b
            AssaultRifle = 0x800,     // 0x00000000100000000000b
            Rifle = 0x1000,           // 0x00000001000000000000b
            Shotgun = 0x2000,         // 0x00000010000000000000b
            Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
            MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
            RangedEnergy = 0x10000,   // 0x00010000000000000000b
            Grenade2 = 0x20000,        // 0x00100000000000000000b
            HeavyWeapons = 0x40000,   // 0x01000000000000000000b
        }
    }
}
