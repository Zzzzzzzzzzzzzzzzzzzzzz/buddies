﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace DefendBuddy
{
    public class ScanState : PositionHolder, IState
    {
        private SimpleChar _target;

        private static bool _charmMobAttacked = false;

        private static double _charmMobAttacking;

        public ScanState() : base(Constants._posToDefend, 3f, 1)
        {

        }

        public IState GetNextState()
        {
            if (_target != null)
                return new FightState(_target);

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("DefendState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("DefendState::OnStateExit");
        }

        private void HandleCharmScan()
        {
            DefendBuddy._charmMob = DynelManager.Characters
                .FirstOrDefault(c => !DefendBuddy._charmMobs.Contains(c.Identity) && (c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)));

            DefendBuddy._brokenCharmMob = DynelManager.Characters
                .FirstOrDefault(c => DefendBuddy._charmMobs.Contains(c.Identity) && !c.Buffs.Contains(NanoLine.CharmOther) && !c.Buffs.Contains(NanoLine.Charm_Short));

            if (DefendBuddy._charmMob != null && !DefendBuddy._charmMobs.Contains(DefendBuddy._charmMob.Identity))
                DefendBuddy._charmMobs.Add((Identity)DefendBuddy._charmMob?.Identity);

            if (_charmMobAttacked == true)
            {
                if (Time.NormalTime > _charmMobAttacking + 8f)
                {
                    if (Extensions.BrokenCharmMobAttacking())
                    {
                        _charmMobAttacked = false;

                        if (DefendBuddy._charmMobs.Contains((Identity)DefendBuddy._brokenCharmMob?.Identity))
                            DefendBuddy._charmMobs.Remove((Identity)DefendBuddy._brokenCharmMob?.Identity);
                    }
                    else { _charmMobAttacked = false; }
                }
            }
            else if (DefendBuddy._brokenCharmMob != null)
            {
                _charmMobAttacking = Time.NormalTime;
                _charmMobAttacked = true;
            }
        }

        public void Tick()
        {
            if (DefendBuddy._settings["Waiting"].AsBool() && !Extensions.InCombat())
            {
                if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66
                    || Team.Members.Any(c => c.Character != null
                        && (c.Character.NanoPercent < 66 || c.Character.HealthPercent < 66
                            || c.Character.Buffs.Contains(NanoLine.Root) == true || c.Character.Buffs.Contains(NanoLine.AOERoot)))
                    || DynelManager.Characters.Any(c => c.IsPet
                        && ((c.Nano / Extensions.PetMaxNanoPool(c) * 100 <= 55 && c.Nano != 10)
                            || c.Buffs.Contains(NanoLine.Root) == true
                            || c.Buffs.Contains(NanoLine.AOERoot) == true
                            || c.Buffs.Contains(NanoLine.Snare) == true
                            || c.Buffs.Contains(NanoLine.AOESnare) == true
                            || c.Buffs.Contains(NanoLine.Mezz) == true
                            || c.Buffs.Contains(NanoLine.AOEMezz) == true)
                        && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) < 10f && c.IsInLineOfSight == true))
                { return; }
            }

            if (DynelManager.LocalPlayer.MovementState == MovementState.Sit) { return; }

            HandleCharmScan();

            if (Extensions.TryGetLeader() != null)
            {
                if (Extensions.TryGetLeader()?.FightingTarget != null)
                {
                    _target = Extensions.TryGetLeader()?.FightingTarget;
                    Chat.WriteLine($"Found target: {_target.Name}.");
                }
            }
            else
            {
                if (DefendBuddy._mob.Count >= 1)
                {
                    if (DefendBuddy._mob.FirstOrDefault().Health > 0)
                    {
                        _target = DefendBuddy._mob.FirstOrDefault();
                        Chat.WriteLine($"Found target: {_target.Name}.");
                    }
                }
                else if (DefendBuddy._bossMob.Count >= 1)
                {
                    if (DefendBuddy._bossMob.FirstOrDefault().Health > 0)
                    {
                        _target = DefendBuddy._bossMob.FirstOrDefault();
                        Chat.WriteLine($"Found target: {_target.Name}.");
                    }
                }
            }

            if (DefendBuddy._mob.Count == 0 && DefendBuddy._bossMob.Count == 0 && DynelManager.LocalPlayer.HealthPercent >= 66
                && DynelManager.LocalPlayer.NanoPercent >= 66 && !Extensions.Rooted())
                HoldPosition();
        }
    }
}
