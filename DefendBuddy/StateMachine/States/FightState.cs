﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace DefendBuddy
{
    public class FightState : PositionHolder, IState
    {
        public const double _fightTimeout = 45f;
        
        private double _fightStartTime;
        public static float _tetherDistance;

        public static int _aggToolCounter = 0;

        private SimpleChar _target;

        public FightState(SimpleChar target) : base(Constants._posToDefend, 3f, 1)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Extensions.IsNull(_target)
                || (Time.NormalTime > _fightStartTime + _fightTimeout && _target?.MaxHealth <= 999999))
            {
                _target = null;

                if (DefendBuddy._settings["Looting"].AsBool() && !DynelManager.NPCs
                    .Any(c => c.Position.DistanceFrom(Constants._posToDefend) <= DefendBuddy.AttackRange
                        && !Constants._ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight)) { return new LootState(); }

                return new ScanState();
            }

            if (!DefendBuddy._settings["Toggle"].AsBool())
            {
                _target = null;

                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");

            _fightStartTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            _aggToolCounter = 0;

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }

        public void Tick()
        {
            if (_target == null || DefendBuddy._initSit)
                return;

            if (Extensions.CanAttack())
            {
                if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= DefendBuddy.Config.CharSettings[Game.ClientInst].AttackRange)
                {
                    DynelManager.LocalPlayer.Attack(_target, false);
                    Chat.WriteLine($"Attacking {_target.Name}.");
                    _fightStartTime = Time.NormalTime;
                }
            }
            else if (DynelManager.LocalPlayer.FightingTarget != null)
            {
                if (Extensions.IsBoss(_target))
                {
                    if (DefendBuddy._switchMob.Count >= 1)
                    {
                        if (DefendBuddy._switchMob.FirstOrDefault().Health > 0)
                        {
                            _target = DefendBuddy._switchMob.FirstOrDefault();
                            Chat.WriteLine($"Switching to target {_target.Name}.");
                            _fightStartTime = Time.NormalTime;
                        }
                    }
                    else if (DefendBuddy._mob.Count >= 1)
                    {
                        if (DefendBuddy._mob.FirstOrDefault().Health > 0)
                        {
                            _target = DefendBuddy._mob.FirstOrDefault();
                            Chat.WriteLine($"Switching to target {_target.Name}.");
                            _fightStartTime = Time.NormalTime;
                        }
                    }
                }
                else if (DefendBuddy._switchMob.Count >= 1 && _target.Name != DefendBuddy._switchMob.FirstOrDefault().Name)
                {
                    if (DefendBuddy._switchMob.FirstOrDefault().Health > 0)
                    {
                        _target = DefendBuddy._switchMob.FirstOrDefault();
                        Chat.WriteLine($"Switching to target {_target.Name}.");
                        _fightStartTime = Time.NormalTime;
                    }
                }
            }

            if (Extensions.ShouldTaunt(_target)
                && DefendBuddy.ModeSelection.Taunt == (DefendBuddy.ModeSelection)DefendBuddy._settings["ModeSelection"].AsInt32())
            {
                if (Extensions.TryGetLeader(DefendBuddy.Leader) != null)
                {
                    if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > DefendBuddy.Config.CharSettings[Game.ClientInst].AttackRange)
                    {
                        if (_aggToolCounter >= 5)
                        {
                            MovementController.Instance.SetDestination(_target.Position);
                        }
                        else if (Extensions.FindTool(out Item _tool))
                        {
                            if (!Item.HasPendingUse && !PerkAction.List.Any(c => c.IsExecuting || c.IsPending) && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                _tool.Use(_target, true);
                                _aggToolCounter++;
                            }
                        }
                    }
                }
            }

            if (DefendBuddy.ModeSelection.Path == (DefendBuddy.ModeSelection)DefendBuddy._settings["ModeSelection"].AsInt32()) { Extensions.HandlePathing(_target); }
        }
    }
}
