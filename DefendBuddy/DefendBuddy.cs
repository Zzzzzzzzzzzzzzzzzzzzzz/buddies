﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using DefendBuddy.IPCMessages;
using AOSharp.Common.GameData.UI;
using AOSharp.Core.Inventory;
using System.Threading.Tasks;
using AOSharp.Core.Misc;

namespace DefendBuddy
{
    public class DefendBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static int AttackRange;
        public static int ScanRange;

        public static bool Toggle = false;
        public static bool _initSit = false;
        public static bool _init = false;

        public static bool _subbedUIEvents = false;

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;
        public static Item _kit;

        public static SimpleChar _charmMob;
        public static SimpleChar _brokenCharmMob;

        public static float Tick = 0;

        public static double _stateTimeOut = Time.NormalTime;

        public static List<SimpleChar> _mob = new List<SimpleChar>();
        public static List<SimpleChar> _bossMob = new List<SimpleChar>();
        public static List<SimpleChar> _switchMob = new List<SimpleChar>();

        public static List<Identity> _charmMobs = new List<Identity>();

        public static Pet _pet;
        public static Pet _hinderedPet;

        private static double _mainUpdate;
        public static double _sitUpdateTimer;

        public static Window _infoWindow;
        private static Window _helperWindow;

        private static View _helperView;

        public static string PluginDir;

        public static Settings _settings;

        public static List<string> _helpers = new List<string>();

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("DefendBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\DefendBuddy\\{Game.ClientInst}\\Config.json");
                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.SetPos, OnSetPosMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.SetResetPos, OnSetResetPosMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.AttackRange, OnAttackRangeMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.ScanRange, OnScanRangeMessage);

                Chat.RegisterCommand("buddy", DefendBuddyCommand);

                SettingsController.RegisterSettingsWindow("DefendBuddy", pluginDir + "\\UI\\DefendBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                Game.OnUpdate += OnUpdate;

                _settings.AddVariable("ModeSelection", (int)ModeSelection.Taunt);

                _settings.AddVariable("Toggle", false);
                _settings.AddVariable("Looting", false);
                _settings.AddVariable("Sitting", false);
                _settings.AddVariable("Waiting", false);

                _settings["Toggle"] = false;

                Chat.WriteLine("DefendBuddy Loaded! Version: 0.9.9.89");
                Chat.WriteLine("/defendbuddy for settings.");

                AttackRange = Config.CharSettings[Game.ClientInst].AttackRange;
                ScanRange = Config.CharSettings[Game.ClientInst].ScanRange;
                Tick = Config.CharSettings[Game.ClientInst].Tick;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public Window[] _windows => new Window[] { _helperWindow };

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        public static void AttackRange_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].AttackRange = e;
            AttackRange = e;
            Config.Save();
        }
        public static void ScanRange_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].ScanRange = e;
            ScanRange = e;
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }
        private void OnSetPosMessage(int sender, IPCMessage msg)
        {
            SetPosMessage setposMsg = (SetPosMessage)msg;

            Chat.WriteLine("Position set.");

            Constants._posToDefend = DynelManager.LocalPlayer.Position;
        }

        private void OnSetResetPosMessage(int sender, IPCMessage msg)
        {
            SetResetPosMessage setresetposMsg = (SetResetPosMessage)msg;

            Chat.WriteLine("Position reset.");

            Constants._posToDefend = Vector3.Zero;
        }

        private void OnAttackRangeMessage(int sender, IPCMessage msg)
        {
            AttackRangeMessage rangeMsg = (AttackRangeMessage)msg;

            Config.CharSettings[Game.ClientInst].AttackRange = rangeMsg.Range;

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid 
                && SettingsController.settingsWindow.FindView("AttackRangeBox", out TextInputView attackRangeInput))
                attackRangeInput.Text = $"{rangeMsg.Range}";
            else
            {
                AttackRange = rangeMsg.Range;
                Config.Save();
            }
        }

        private void OnScanRangeMessage(int sender, IPCMessage msg)
        {
            ScanRangeMessage rangeMsg = (ScanRangeMessage)msg;

            Config.CharSettings[Game.ClientInst].ScanRange = rangeMsg.Range;

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid
                && SettingsController.settingsWindow.FindView("ScanRangeBox", out TextInputView scanRangeInput))
                scanRangeInput.Text = $"{rangeMsg.Range}";
            else
            {
                ScanRange = rangeMsg.Range;
                Config.Save();
            }
        }

        private void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;

            Chat.WriteLine("Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            Chat.WriteLine("Buddy enabled.");
            _settings["Toggle"] = true;

            Toggle = true;

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            Toggle = false;

            _settings["Toggle"] = false;
            Chat.WriteLine("Buddy disabled.");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\DefendBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void HandleRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new AttackRangeMessage()
            {
                Range = Config.CharSettings[Game.ClientInst].AttackRange
            });
            IPCChannel.Broadcast(new ScanRangeMessage()
            {
                Range = Config.CharSettings[Game.ClientInst].ScanRange
            });
        }

        private void HandleHelpersViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_helperView)) { return; }

                _helperView = View.CreateFromXml(PluginDir + "\\UI\\DefendBuddyHelpersView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Helpers", XmlViewName = "DefendBuddyHelpersView" }, _helperView);
            }
            else if (_helperWindow == null || (_helperWindow != null && !_helperWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_helperWindow, PluginDir, new WindowOptions() { Name = "Helpers", XmlViewName = "DefendBuddyHelpersView", WindowSize = new Rect(0, 0, 130, 345) }, _helperView, out var container);
                _helperWindow = container;
            }
        }

        private void HandleAddHelperViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _helpers.Add(Targeting.TargetChar?.Name);
        }

        private void HandleRemoveHelperViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _helpers.Remove(Targeting.TargetChar?.Name);
        }

        private void HandleClearHelpersViewClick(object s, ButtonBase button)
        {
            _helpers.Clear();
        }

        private void HandlePrintHelpersViewClick(object s, ButtonBase button)
        {
            foreach (string str in _helpers)
                Chat.WriteLine(str);
        }

        private void Scanning()
        {
            _bossMob = DynelManager.NPCs
                .Where(c => c.DistanceFrom(Extensions.TryGetLeader(Leader)) <= ScanRange
                    && !Constants._ignores.Contains(c.Name)
                    && c.Health > 0 && c.IsInLineOfSight
                    && Extensions.CanAttack(c)
                    && Extensions.IsBoss(c))
                .OrderBy(c => c.Position.DistanceFrom(Extensions.TryGetLeader(Leader).Position))
                .ThenByDescending(c => c.Name == "Uklesh the Beguiling")
                .ThenByDescending(c => c.Name == "Khalum the Weaver of Flesh")
                .ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => c.DistanceFrom(Extensions.TryGetLeader(Leader)) <= ScanRange
                   && !Constants._ignores.Contains(c.Name)
                   && c.Health > 0 && c.IsInLineOfSight
                   && !_charmMobs.Contains(c.Identity)
                   && !Extensions.IsBoss(c)
                   && Extensions.AttackingTeam(c)
                   && (c.Name == "Devoted Fanatic" || c.Name == "Hallowed Acolyte" || c.Name == "Confounding Bloated Carcass"
                        || c.Name == "Hand of the Colonel"
                        || c.Name == "Hacker'Uri" || c.Name == "Stim Fiend"
                        || c.Name == "Lost Thought" || c.Name == "The Sacrifice" || c.Name == "Corrupted Xan-Len"
                        || c.Name == "Blue Tower" || c.Name == "Green Tower" || c.Name == "Drone Harvester - Jaax'Sinuh"
                        || c.Name == "Strange Xan Artifact"
                        || c.Name == "Fanatic"
                        || c.Name == "Dasyatis" || (c.Name == "Ground Chief Aune" && c.MaxHealth == 1166667) || c.Name == "Dust Brigade Drone - Omega"
                        || c.Name == "Stasis Containment Field"
                        || (c.Name == "Alien Cocoon" || c.Name == "Alien Coccoon") && (Playfield.ModelIdentity.Instance == 6017 || Playfield.ModelIdentity.Instance == 382)))
                //All spelt Coccoon
                //Max hp 200000 - Level 250 coon room ship 
                //Max hp 147659 - Level 234 coon room ship 
                //Max hp 154202 - Level 236 coon room ship 
                //Max hp 154202 - Level 220 coon room ship 
                //Max hp 40000 - Level 250 s28  
                //Max hp 145 - Level 25 mitaar
                .OrderBy(c => c.HealthPercent)
                .ThenByDescending(c => c.MaxHealth)
                .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
               //.OrderBy(c => c.HealthPercent)
               //.ThenBy(c => c.Position.DistanceFrom(Extensions.TryGetLeader(Leader).Position))
               .ThenByDescending(c => (c.Name == "Alien Cocoon" || c.Name == "Alien Coccoon") && (Playfield.ModelIdentity.Instance == 6017 || Playfield.ModelIdentity.Instance == 382))
               .ThenByDescending(c => c.Name == "Devoted Fanatic" || c.Name == "Fanatic")
               .ThenByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
               .ToList();

            _mob = DynelManager.Characters
                .Where(c => !c.IsPlayer && c.Position.DistanceFrom(Constants._posToDefend) <= ScanRange
                    && !Constants._ignores.Contains(c.Name)
                    && c.Health > 0 && c.IsInLineOfSight
                    && !_charmMobs.Contains(c.Identity)
                    && !Extensions.IsBoss(c)
                    && Extensions.Checks(c)
                    && Extensions.AttackingTeam(c)
                    && !Team.Members.Select(b => b.Name).Contains(c.Name) 
                    && !c.Name.Contains("sapling")
                    && !c.IsPet)
                .OrderBy(c => c.HealthPercent)
                .ThenByDescending(c => c.MaxHealth)
                .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                //.OrderBy(c => c.HealthPercent)
                //.ThenBy(c => c.Position.DistanceFrom(Extensions.TryGetLeader(Leader).Position))
                .ThenByDescending(c => c.Name == "Corrupted Hiisi Berserker")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Cur")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Kuir")
                .ThenByDescending(c => c.Name == "Hacker'Uri")
                .ThenByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
                .ThenByDescending(c => c.Name == "Alien Cocoon")
                .ThenByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
                .ThenByDescending(c => c.Name == "Green Tower")
                .ThenByDescending(c => c.Name == "Blue Tower")
                .ThenByDescending(c => c.Name == "The Sacrifice")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Len")
                .ThenByDescending(c => c.Name == "Hallowed Acolyte")
                .ThenByDescending(c => c.Name == "Devoted Fanatic")
                .ToList();
        }

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                if (_settings["Toggle"].AsBool() && Extensions.ShouldStopAttack())
                {
                    DynelManager.LocalPlayer.StopAttack();
                    Chat.WriteLine($"Stopping attack.");
                }

                if (_settings["Sitting"].AsBool())
                    ListenerSit();

                Scanning();

                _stateMachine.Tick();
                _mainUpdate = Time.NormalTime;
            }

            #region UI Update

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                if (window.FindView("DefendBuddyAddHelper", out Button addHelperView))
                {
                    addHelperView.Tag = window;
                    addHelperView.Clicked = HandleAddHelperViewClick;
                }

                if (window.FindView("DefendBuddyRemoveHelper", out Button removeHelperView))
                {
                    removeHelperView.Tag = window;
                    removeHelperView.Clicked = HandleRemoveHelperViewClick;
                }

                if (window.FindView("DefendBuddyClearHelpers", out Button clearHelpersView))
                {
                    clearHelpersView.Tag = window;
                    clearHelpersView.Clicked = HandleClearHelpersViewClick;
                }

                if (window.FindView("DefendBuddyPrintHelpers", out Button printHelpersView))
                {
                    printHelpersView.Tag = window;
                    printHelpersView.Clicked = HandlePrintHelpersViewClick;
                }
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                SettingsController.settingsWindow.FindView("AttackRangeBox", out TextInputView attackRangeInput);
                SettingsController.settingsWindow.FindView("ScanRangeBox", out TextInputView scanRangeInput);
                SettingsController.settingsWindow.FindView("TickBox", out TextInputView tickInput);

                if (SettingsController.settingsWindow.FindView("DefendBuddyRelay", out Button relay))
                {
                    relay.Tag = window;
                    relay.Clicked = HandleRelayClick;
                }

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text)
                    && int.TryParse(channelInput.Text, out int channelValue)
                    && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text)
                    && float.TryParse(tickInput.Text, out float tickValue)
                    && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                    Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (attackRangeInput != null && !string.IsNullOrEmpty(attackRangeInput.Text)
                    && int.TryParse(attackRangeInput.Text, out int attackRangeInputValue)
                    && Config.CharSettings[Game.ClientInst].AttackRange != attackRangeInputValue)
                    Config.CharSettings[Game.ClientInst].AttackRange = attackRangeInputValue;
                if (scanRangeInput != null && !string.IsNullOrEmpty(scanRangeInput.Text)
                    && int.TryParse(scanRangeInput.Text, out int scanRangeInputValue)
                    && Config.CharSettings[Game.ClientInst].ScanRange != scanRangeInputValue)
                    Config.CharSettings[Game.ClientInst].ScanRange = scanRangeInputValue;

                if (SettingsController.settingsWindow.FindView("DefendBuddyHelpersView", out Button helperView))
                {
                    helperView.Tag = SettingsController.settingsWindow;
                    helperView.Clicked = HandleHelpersViewClick;
                }

                if (SettingsController.settingsWindow.FindView("DefendBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool()
                    && DynelManager.LocalPlayer.Identity == Leader
                    && Constants._posToDefend == Vector3.Zero)
                {
                    Constants._posToDefend = DynelManager.LocalPlayer.Position;
                    IPCChannel.Broadcast(new SetPosMessage());

                    Chat.WriteLine("Position set.");
                    return;
                }

                if (!_settings["Toggle"].AsBool()
                    && DynelManager.LocalPlayer.Identity == Leader
                    && Constants._posToDefend != Vector3.Zero)
                {
                    Constants._posToDefend = Vector3.Zero;
                    IPCChannel.Broadcast(new SetResetPosMessage());

                    Chat.WriteLine("Position reset.");
                    return;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (Leader == Identity.None)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;
                    }

                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        IPCChannel.Broadcast(new StartMessage());
                        Constants._posToDefend = DynelManager.LocalPlayer.Position;
                        IPCChannel.Broadcast(new SetPosMessage());
                        Chat.WriteLine("Position set.");
                    }

                    Start();
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Stop();

                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        IPCChannel.Broadcast(new StopMessage());
                        Constants._posToDefend = Vector3.Zero;
                        IPCChannel.Broadcast(new SetResetPosMessage());
                        Chat.WriteLine("Position reset.");
                    }
                }
            }

            #endregion
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].AttackRangeChangedEvent -= AttackRange_Changed;
                Config.CharSettings[Game.ClientInst].ScanRangeChangedEvent -= ScanRange_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].AttackRangeChangedEvent += AttackRange_Changed;
                Config.CharSettings[Game.ClientInst].ScanRangeChangedEvent += ScanRange_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        public static class RelevantNanos
        {
            public const int ReflectionsOfAngerLong = 290404;
            public static readonly int[] ShovelBuffs = Spell.GetSpellsForNanoline(NanoLine.ShovelBuffs).Where(c => c.Id != ReflectionsOfAngerLong).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
        }

        private void DefendBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        if (Leader == Identity.None)
                        {
                            IsLeader = true;
                            Leader = DynelManager.LocalPlayer.Identity;
                        }

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StartMessage());
                            Constants._posToDefend = DynelManager.LocalPlayer.Position;
                            IPCChannel.Broadcast(new SetPosMessage());
                            Chat.WriteLine("Position set.");
                        }

                        _settings["Toggle"] = true;
                        Start();
                    }
                    else
                    {
                        Stop();

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StopMessage());
                            Constants._posToDefend = Vector3.Zero;
                            IPCChannel.Broadcast(new SetResetPosMessage());
                            Chat.WriteLine("Position reset.");
                        }

                        _settings["Toggle"] = false;
                    }
                }
                else if (param.Length >= 1)
                {
                    switch (param[0].ToLower())
                    {
                        case "helpers":
                            if (param[1].ToLower() == "add")
                            {
                                if (param.Length > 2)
                                {
                                    string name = string.Join(" ", param.Skip(1));

                                    if (!_helpers.Contains(name))
                                    {
                                        _helpers.Add(name);
                                        chatWindow.WriteLine($"Added {name} to Helpers list");
                                    }
                                    else if (_helpers.Contains(name))
                                    {
                                        _helpers.Remove(name);
                                        chatWindow.WriteLine($"Removed {name} from Helpers list");
                                    }
                                }
                                else
                                {
                                    if (Targeting.TargetChar != null)
                                    {
                                        if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                        {
                                            _helpers.Add(Targeting.TargetChar?.Name);
                                            chatWindow.WriteLine($"Added {Targeting.TargetChar?.Name} to Helpers list");
                                        }
                                    }
                                    else
                                        chatWindow.WriteLine($"Must have a target.");
                                }
                            }
                            else if (param[1].ToLower() == "remove")
                            {
                                if (param.Length > 2)
                                {
                                    string name = string.Join(" ", param.Skip(1));

                                    if (!_helpers.Contains(name))
                                    {
                                        _helpers.Add(name);
                                        chatWindow.WriteLine($"Added {name} to Helpers list");
                                    }
                                    else if (_helpers.Contains(name))
                                    {
                                        _helpers.Remove(name);
                                        chatWindow.WriteLine($"Removed {name} from Helpers list");
                                    }
                                }
                                else
                                {
                                    if (Targeting.TargetChar != null)
                                    {
                                        if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                        {
                                            _helpers.Add(Targeting.TargetChar?.Name);
                                            chatWindow.WriteLine($"Added {Targeting.TargetChar?.Name} to Helpers list");
                                        }
                                    }
                                    else
                                        chatWindow.WriteLine($"Must have a target.");
                                }
                            }
                            break;
                        case "ignore":
                            if (param.Length > 1)
                            {
                                string name = string.Join(" ", param.Skip(1));

                                if (!Constants._ignores.Contains(name))
                                {
                                    Constants._ignores.Add(name);
                                    Chat.WriteLine($"Added \"{name}\" to ignored mob list");
                                }
                                else if (Constants._ignores.Contains(name))
                                {
                                    Constants._ignores.Remove(name);
                                    Chat.WriteLine($"Removed \"{name}\" from ignored mob list");
                                }
                            }
                            else
                            {
                                if (Targeting.TargetChar != null)
                                {
                                    if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                    {
                                        Constants._ignores.Add(Targeting.TargetChar?.Name);
                                        Chat.WriteLine($"Added {Targeting.TargetChar?.Name} to ignored mob list");
                                    }
                                }
                                else
                                    Chat.WriteLine($"Must have a target.");
                            }
                            break;
                        default:
                            return;
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void ListenerSit()
        {
            if (_initSit == false && Extensions.CanUseSitKit(out _kit)
                && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            {
                Task.Factory.StartNew(
                    async () =>
                    {
                        _initSit = true;
                        await Task.Delay(400);
                        MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                        await Task.Delay(1800);
                        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                        await Task.Delay(400);
                        _initSit = false;
                    });
            }
        }

        public enum ModeSelection
        {
            Taunt, Path
        }
    }
}
