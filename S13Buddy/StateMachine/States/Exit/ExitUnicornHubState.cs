﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using S13Buddy.IPCMessages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace S13Buddy
{
    public class ExitUnicornHubState : IState
    {
        private static double _timer;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return null; }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId) { return new EnterSectorState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ExitUnicornHub::OnStateEnter");

            //Edge case correction: Stuck sitting.
            //S13Buddy._initSit = false;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ExitUnicornHub::OnStateExit");

            //Edge case correction: Stuck sitting.
            //S13Buddy._initSit = false;
        }

        public void Tick()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return; }

            #region Lever Pathing

            if (Extensions.TryGetLever(out S13Buddy._lever) != null && DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)S13Buddy._lever?.Position) < 30f)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)S13Buddy._lever?.Position) < 3f 
                    && Time.NormalTime > _timer + 2f && !Extensions.IsCasting()) 
                {
                    S13Buddy._lever?.Use();

                    _timer = Time.NormalTime;
                }
            }
            else if (!SMovementController.IsNavigating()) { SMovementController.SetPath(S13Buddy._unicornHubSPath, true); }

            #endregion
        }
    }
}