﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Configuration;
using System.Linq;

namespace S13Buddy
{
    public class ExitSectorState : IState
    {
        private static double _timer;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId) { return new ReformState(); }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine($"ExitSectorState::OnStateEnter");

            ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

            SMovementController.SetNavDestination(Constants.S13GoalPosition);

            _timer = Time.NormalTime;
            S13Buddy._stateTimeOut = Time.NormalTime;
        }

        public void OnStateExit()
        {
            Chat.WriteLine("ExitSectorState::OnStateExit");

            SMovementController.Halt();
        }

        public void Tick()
        {
            if (Game.IsZoning || Playfield.ModelIdentity.Instance != Constants.S13Id) { return; }

            #region Disbanding

            #region Time-Out

            if (Time.NormalTime > S13Buddy._stateTimeOut + 600f)
            {
                if (Team.IsInTeam)
                {
                    if (DynelManager.LocalPlayer.Identity == S13Buddy.Leader) { Team.Disband(); }
                    else { Team.Leave(); }
                }
            }

            #endregion

            if (Team.IsInTeam && DynelManager.LocalPlayer.Identity == S13Buddy.Leader && Time.NormalTime > _timer + 16f && Extensions.CanProceed())
            { Team.Disband(); }

            #endregion

            #region Scan

            if (Extensions.InCombat() && Extensions.TryGetAlien(out S13Buddy._alien) != null)
            {
                Extensions.HandleAttacking(Extensions.TryGetAlien(out S13Buddy._alien));
            }

            #endregion
        }
    }
}