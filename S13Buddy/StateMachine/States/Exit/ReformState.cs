﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S13Buddy
{
    public class ReformState : IState
    {
        private const float ReformTimeout = 45;
        private const float DisbandDelay = 12;

        private static double _reformStartedTime;

        private ReformPhase _phase;

        private static double _timer;

        public static bool Reforming = false;
        private static bool _initRaid = false;

        public static List<Identity> _teamCache = new List<Identity>();
        private static List<Identity> _invitedList = new List<Identity>();

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return null; }

            if (Extensions.ShouldIdle(_reformStartedTime, ReformTimeout)) { return new IdleState(); }

            if (Extensions.TimedOut(_reformStartedTime, ReformTimeout)) { return new EnterSectorState(); }

            if (Team.IsRaid && _phase == ReformPhase.Completed
                && Team.Members.Count() >= _teamCache.Count()) { return new EnterSectorState(); }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("ReformState::OnStateEnter");

            if (S13Buddy._previousRound != DateTime.MinValue)
            {
                S13Buddy._currentRound = DateTime.UtcNow;

                S13Buddy._missionTimeGuesstimation = S13Buddy._currentRound - S13Buddy._previousRound;

                S13Buddy._previousRound = DateTime.UtcNow;
            }
            else
            {
                S13Buddy._previousRound = DateTime.UtcNow;
            }

            //Edge case correction: Stuck sitting.
            //S13Buddy._initSit = false;

            Reforming = true;

            Extensions.ResetInternals();

            _reformStartedTime = Time.NormalTime;

            SMovementController.SetNavDestination(Constants.S13ZoneOutPosition);

            if (DynelManager.LocalPlayer.Identity != S13Buddy.Leader)
            {
                Team.TeamRequest += OnTeamRequest;
                _phase = ReformPhase.Waiting;
                Chat.WriteLine("ReformPhase.Waiting");
            }
            else
            {
                _phase = ReformPhase.Disbanding;
                Chat.WriteLine("ReformPhase.Disbanding");
            }
        }

        public void OnStateExit()
        {
            Chat.WriteLine("ReformState::OnStateExit");

            _invitedList.Clear();

            Reforming = false;

            _initRaid = false;

            if (DynelManager.LocalPlayer.Identity != S13Buddy.Leader)
                Team.TeamRequest -= OnTeamRequest;
        }

        public void Tick()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return; }

            #region Disbanding

            if (_phase == ReformPhase.Disbanding && Time.NormalTime > _reformStartedTime + DisbandDelay)
            {
                _phase = ReformPhase.Inviting;
                Chat.WriteLine("ReformPhase.Inviting");
            }

            #endregion

            #region Inviting

            if (_phase == ReformPhase.Inviting)
            {
                if (Team.IsInTeam && !Team.IsRaid && !_initRaid)
                {
                    _timer = Time.NormalTime;
                    _initRaid = true;

                    Team.ConvertToRaid();
                }

                if (_invitedList.Count() < _teamCache.Count())
                {
                    if (Time.NormalTime > _timer + 1.3f)
                    {
                        _timer = Time.NormalTime;

                        foreach (SimpleChar player in DynelManager.Players.Where(c => !_invitedList.Contains(c.Identity) && _teamCache.Contains(c.Identity)).Take(3))
                        {
                            if (_invitedList.Contains(player.Identity)) { continue; }

                            _invitedList.Add(player.Identity);

                            if (player.Identity == S13Buddy.Leader) { continue; }

                            Team.Invite(player.Identity);
                            Chat.WriteLine($"Inviting {player.Name}");
                        }
                    }
                }
                else if (Team.IsRaid)
                {
                    _phase = ReformPhase.Completed;
                    Chat.WriteLine("ReformPhase.Completed");
                }
            }

            #endregion
        }

        #region Misc

        private void OnTeamRequest(object s, TeamRequestEventArgs e)
        {
            SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester && (c.Name == S13Buddy.LeaderName || e.Requester == S13Buddy.Leader));

            if (_player != null)
            {
                _phase = ReformPhase.Completed;
                e.Accept();
            }
        }

        private enum ReformPhase
        {
            Disbanding,
            Inviting,
            Waiting,
            Completed
        }

        #endregion
    }
}
