﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using S13Buddy.IPCMessages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace S13Buddy
{
    public class DiedState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return null; }

            if (Extensions.ShouldIdle()) { return new IdleState(); }
            else if (Extensions.ShouldReform())
            {
                Extensions.ResetInternals();

                return new ReformState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId) { return new ExitUnicornHubState(); }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId) { return new EnterSectorState(); }

            return null;
        }

        public void OnStateEnter()
        {
            S13Buddy._died = true;

            //Edge case correction: Stuck sitting.
            //S13Buddy._initSit = false;

            //Chat.WriteLine($"DiedState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("DiedState::OnStateExit");

            //Edge case correction: Stuck sitting.
            //S13Buddy._initSit = false;
        }

        public void Tick()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f || Playfield.ModelIdentity.Instance != Constants.XanHubId) { return; }

            #region Xan Hub Pathing

            if (!S13Buddy._initSit && Extensions.CanProceed() && Extensions.SelfHealthy()) { SMovementController.SetDestination(Constants.XanHubPosition); }

            #endregion
        }
    }
}