﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace S13Buddy
{
    public class EnterSectorState : IState
    {
        private static double _timer;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId
                || Playfield.ModelIdentity.Instance == Constants.UnicornHubId) { return new DiedState(); }

            if (Extensions.ShouldReform())
            {
                Extensions.ResetInternals();

                return new ReformState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.S13Id)
            {
                if (S13Buddy.ModeSelection.Leech == (S13Buddy.ModeSelection)S13Buddy._settings["ModeSelection"].AsInt32())
                    if (S13Buddy._died 
                        || S13Buddy._merging 
                        || (!S13Buddy._died && !Team.Members.Any(c => c.Character == null))
                        || Extensions.Waited(_timer))
                    {
                        ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

                        return new LeechState();
                    }

                if (S13Buddy._died || S13Buddy._merging 
                    || (!S13Buddy._died && !Team.Members.Any(c => c.Character == null))
                    || (!S13Buddy._died && !S13Buddy._settings["Waiting"].AsBool())
                    || Extensions.Waited(_timer))
                {
                    ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

                    return new WorkState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("EnterSectorState::OnStateEnter");

            if (S13Buddy._died)
                Chat.WriteLine($"Idling until rebuffed..");
            else { Chat.WriteLine($"Idling for {6} seconds.."); }

            _timer = Time.NormalTime;
        }

        public void OnStateExit()
        {
            Chat.WriteLine("EnterSectorState::OnStateExit");
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            #region Entrance Pathing

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
            {
                //if (DynelManager.LocalPlayer.MovementState == MovementState.Sit || (S13Buddy._initSit && DynelManager.LocalPlayer.MovementState == MovementState.Run))
                //{
                //    SMovementController.SetMovement(MovementAction.LeaveSit);
                //    S13Buddy._initSit = false;
                //}

                //if (S13Buddy._initSit && Time.NormalTime > _timer + 3f)
                //{
                //    if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                //    {
                //        SMovementController.SetMovement(MovementAction.LeaveSit);
                //        S13Buddy._initSit = false;
                //    }
                //    else { S13Buddy._initSit = false; }
                //}

                if (Extensions.Waited(_timer) && !Extensions.IsCasting() && Extensions.SelfHealthy())
                {
                    SMovementController.SetNavDestination(Constants.S13EntrancePosition);
                }
                else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
            }

            #endregion
        }
    }
}