﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S13Buddy
{
    public class IdleState : IState
    {
        private static bool _init = false;
        private static bool _initTeam = false;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (S13Buddy._settings["Toggle"].AsBool() && Team.IsRaid)
            {
                if (Playfield.ModelIdentity.Instance == Constants.APFHubId) 
                {
                    ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

                    if (!S13Buddy._merging && S13Buddy.LookingSelection.Team == (S13Buddy.LookingSelection)S13Buddy._settings["LookingSelection"].AsInt32())
                        S13Buddy._merging = true;

                    return new EnterSectorState();
                }

                if (Playfield.ModelIdentity.Instance == Constants.S13Id)
                {
                    ReformState._teamCache = Team.Members.Select(c => c.Identity).ToList();

                    if (!S13Buddy._merging && S13Buddy.LookingSelection.Team == (S13Buddy.LookingSelection)S13Buddy._settings["LookingSelection"].AsInt32())
                        S13Buddy._merging = true;

                    if (S13Buddy.ModeSelection.Leech == (S13Buddy.ModeSelection)S13Buddy._settings["ModeSelection"].AsInt32())
                        return new LeechState();

                    return new WorkState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            Chat.WriteLine("IdleState::OnStateExit");

            if (S13Buddy.LookingSelection.Team == (S13Buddy.LookingSelection)S13Buddy._settings["LookingSelection"].AsInt32())
            {
                if (_init)
                {
                    _init = false;

                    if (_initTeam)
                    {
                        _initTeam = false;
                        Team.TeamRequest -= OnTeamRequest;
                    }
                }
            }

            //S13Buddy._invitedIdentities.Clear();
        }

        public void Tick()
        {
            if (!S13Buddy._settings["Toggle"].AsBool()) { return; }

            if (S13Buddy.LookingSelection.Team == (S13Buddy.LookingSelection)S13Buddy._settings["LookingSelection"].AsInt32())
            {
                if (!_init)
                {
                    _init = true;

                    S13Buddy._settings["ModeSelection"] = 1;
                    SettingsController.CleanUp();

                    if (!Team.IsRaid)
                    {
                        _initTeam = true;

                        Team.TeamRequest += OnTeamRequest;

                        if (S13Buddy._settings["OurOrg"].AsBool())
                        {
                            string _str = $"sector13 axp {DynelManager.LocalPlayer.GetStat(Stat.Clan)}";

                            LookingForTeam.Join(_str);
                        }
                        else
                        {
                            string _str = $"sector13 axp";

                            LookingForTeam.Join(_str);
                        }
                    }
                }
            }
        }

        private static void OnTeamRequest(object s, TeamRequestEventArgs e)
        {
            SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester);

            if (_player != null)
            {
                S13Buddy.Config.CharSettings[Game.ClientInst].Leader = _player?.Name;
                S13Buddy.Leader = (Identity)_player?.Identity;

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow.FindView("LeaderBox", out S13Buddy._leaderInput);

                    if (S13Buddy._leaderInput != null)
                        S13Buddy._leaderInput.Text = S13Buddy.Config.CharSettings[Game.ClientInst].Leader;
                }

                S13Buddy.Config.Save();
            }

            //if (S13Buddy._settings["OurOrg"].AsBool()) 
            //{
            //    SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester);

            //    if (_player != null) 
            //    { 
            //        if (DynelManager.LocalPlayer.GetStat(Stat.Clan) != _player.GetStat(Stat.Clan)) { return; }
            //        else 
            //        {
            //            LookingForTeam.Leave();
            //            e.Accept();
            //        }
            //    }
            //}

            LookingForTeam.Leave();
            e.Accept();

            //SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity == e.Requester);

            //if (_player != null)
            //{
            //    S13Buddy.Leader = e.Requester;

            //    if (S13Buddy._settings["OurOrg"].AsBool() && DynelManager.LocalPlayer.GetStat(Stat.Clan) != _player.GetStat(Stat.Clan)) { return; }

            //    e.Accept();
            //}
        }
    }
}
