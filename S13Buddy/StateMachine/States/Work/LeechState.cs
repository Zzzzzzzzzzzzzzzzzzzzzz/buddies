﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace S13Buddy
{
    public class LeechState : IState
    {
        public static int _counter = 0;

        //private static bool _timeOut = false;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.CanExit()) { return new ExitSectorState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Extensions.ShouldReform()) { return new ReformState(); }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("LeechState::OnStateEnter");

            DynelManager.LocalPlayer.Position = new Vector3(DynelManager.LocalPlayer.Position.X, 67.7f, DynelManager.LocalPlayer.Position.Z);
            MovementController.Instance.SetMovement(MovementAction.Update);
        }

        public void OnStateExit()
        {
            Chat.WriteLine("LeechState::OnStateExit");

            Extensions.ResetInternals();
        }

        public void Tick()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return; }

            if (S13Buddy._died)
            {
                #region Died Pathing

                if (DynelManager.LocalPlayer.Position.Distance2DFrom(S13Buddy._ourPosition) <= 40f)
                {
                    if (Extensions.GreaterThreat())
                    {
                        if (SMovementController.IsNavigating())
                        {
                            S13Buddy._died = false;
                            SMovementController.Halt();
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.Distance2DFrom(S13Buddy._ourPosition) <= 15f)
                        {
                            if (SMovementController.IsNavigating())
                            {
                                S13Buddy._died = false;
                                SMovementController.Halt();
                            }
                        }
                        else if (Extensions.CanProceed() && !Extensions.IsCasting() && (Extensions.SelfHealthy() || Extensions.InCombat()))
                        {
                            SMovementController.SetDestination(S13Buddy._ourPosition);
                        }
                        else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                    }

                    //if (DynelManager.LocalPlayer.Position.Distance2DFrom(S13Buddy._ourPosition) <= 1f)
                    //{
                    //    if (Team.Members.Any(c => c.Identity != DynelManager.LocalPlayer.Identity && c.Character != null) 
                    //        && !Team.Members.Any(c => c.Character != null && c.Identity != DynelManager.LocalPlayer.Identity && (bool)c.Character?.IsMoving))
                    //    {
                    //        if (SMovementController.IsNavigating())
                    //        {
                    //            S13Buddy._died = false;
                    //            SMovementController.Halt();
                    //        }
                    //    }
                    //    else if (Extensions.DangerousNearOurPositon())
                    //    {
                    //        if (SMovementController.IsNavigating())
                    //        {
                    //            S13Buddy._died = false;
                    //            SMovementController.Halt();
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    if (Extensions.CanProceed() && !Extensions.IsCasting() && Extensions.SelfHealthy())
                    //    {
                    //        SMovementController.SetDestination(S13Buddy._ourPosition);
                    //    }
                    //    else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                    //}
                }
                else
                {
                    if (Extensions.CanProceed() && !Extensions.IsCasting() && (Extensions.SelfHealthy() || Extensions.InCombat()))
                    {
                        Extensions.HandleCeilingPathing();
                    }
                    else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                }

                #endregion
            }
            else
            {
                #region Time-Out

                if (Team.Members.Select(c => c.Name).Where(c => c == DynelManager.LocalPlayer.Name).ToList().Count > 1) { Team.Leave(); }

                //if (Time.NormalTime > S13Buddy._stateTimeOut + 600f)
                //{
                //    if (Team.IsInTeam)
                //    {
                //        if (DynelManager.LocalPlayer.Identity == S13Buddy.Leader) { Team.Disband(); }
                //        else { Team.Leave(); }
                //    }
                //}

                #endregion

                #region Merge Pathing

                if (!S13Buddy._merged && S13Buddy._merging)
                {
                    if (Extensions.TryGetLeader(out S13Buddy._leader) == null)
                    {
                        if (!Extensions.GreaterThreat()) { Extensions.HandleCeilingPathing(); }
                        else if (SMovementController.IsNavigating()) 
                        { 
                            SMovementController.Halt();
                            S13Buddy._merged = true;
                        }
                    }
                    else if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                        S13Buddy._merged = true;
                    }
                }

                #endregion

                if (Extensions.TryGetLeader(out S13Buddy._leader) != null)
                {
                    #region Pathing

                    if (DynelManager.LocalPlayer.Position.Distance2DFrom(Constants.S13GoalPosition) > 15f)
                    {
                        Vector3 _pos = (Vector3)Extensions.TryGetLeader(out S13Buddy._leader)?.Position;

                        if (DynelManager.LocalPlayer.Position.Distance2DFrom(_pos) > 1f)
                        {
                            SMovementController.SetDestination(new Vector3(_pos.X, DynelManager.LocalPlayer.Position.Y, _pos.Z));
                        }
                    }

                    #endregion
                }
                //else if (_timeOut)
                //{
                //    _timeOut = true;
                //    S13Buddy._stateTimeOut = Time.NormalTime;
                //}
            }
        }
    }
}