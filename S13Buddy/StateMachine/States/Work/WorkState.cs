﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace S13Buddy
{
    public class WorkState : IState
    {
        private SimpleChar _target;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.CanExit()) { return new ExitSectorState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Extensions.ShouldReform()) { return new ReformState(); }

            if (_target != null) 
            {
                if (Extensions.TeamHealthy()) { return new FightState(_target); }
                else if (Extensions.InCombat() || (!S13Buddy._settings["Waiting"].AsBool() && Extensions.SelfHealthy())) { return new FightState(_target); }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("WorkState::OnStateEnter");

            if (S13Buddy._died
                || (!S13Buddy._merged && S13Buddy._merging))
            {
                DynelManager.LocalPlayer.Position = new Vector3(DynelManager.LocalPlayer.Position.X, 67.7f, DynelManager.LocalPlayer.Position.Z);
                MovementController.Instance.SetMovement(MovementAction.Update);
            }

            S13Buddy._stateTimeOut = Time.NormalTime;
        }

        public void OnStateExit()
        {
            Chat.WriteLine("WorkState::OnStateExit");

            //Edge case correction: Stuck sitting.
            //S13Buddy._initSit = false;

            //SMovementController.Halt();

            Extensions.ResetInternals();
        }

        private void HandleScan()
        {
            if (S13Buddy._settings["Waiting"].AsBool())
            {
                if (Extensions.InCombat() || (Extensions.TeamHealthy() && Extensions.TeamValid()))
                {
                    if (Extensions.TryGetAlien(out S13Buddy._alien) != null && Extensions.CanProceed())
                    {
                        _target = S13Buddy._alien;
                        Chat.WriteLine($"Found target: {_target.Name}");
                        return;
                    }
                }
            }
            else if (Extensions.InCombat() || Extensions.SelfHealthy())
            {
                if (Extensions.TryGetAlien(out S13Buddy._alien) != null && Extensions.CanProceed())
                {
                    _target = S13Buddy._alien;
                    Chat.WriteLine($"Found target: {_target.Name}");
                    return;
                }
            }

            //if (Extensions.InCombat() || Extensions.CanProceed())
            //{
            //    if (S13Buddy._settings["Waiting"].AsBool())
            //    {
            //        if (Extensions.InCombat() || (Extensions.TeamHealthy() && Extensions.TeamValid()))
            //        {
            //            if (Extensions.TryGetAlien(out S13Buddy._alien) != null/* && !Extensions.InCeiling()*/)
            //            {
            //                _target = S13Buddy._alien;
            //                Chat.WriteLine($"Found target: {_target.Name}");
            //                return;
            //            }
            //        }
            //    }
            //    else if (Extensions.InCombat() || Extensions.SelfHealthy())
            //    {
            //        if (Extensions.TryGetAlien(out S13Buddy._alien) != null/* && !Extensions.InCeiling()*/)
            //        {
            //            _target = S13Buddy._alien;
            //            Chat.WriteLine($"Found target: {_target.Name}");
            //            return;
            //        }
            //    }
            //}
        }

        public void Tick()
        {
            if (Game.IsZoning || Time.NormalTime < S13Buddy._lastZonedTime + 2f 
                || Playfield.ModelIdentity.Instance != Constants.S13Id || !Team.IsInTeam || _target != null) { return; }

            if (S13Buddy._died)
            {
                #region Died Pathing

                if (DynelManager.LocalPlayer.Position.Distance2DFrom(S13Buddy._ourPosition) <= 40f)
                {
                    if (S13Buddy._safe)
                    {
                        if (SMovementController.IsNavigating()) { SMovementController.Halt(); }

                        Vector3 _pos = new Vector3(DynelManager.LocalPlayer.Position.X, S13Buddy._ourPosition.Y, DynelManager.LocalPlayer.Position.Z);

                        DynelManager.LocalPlayer.Position = _pos;
                        SMovementController.SetMovement(MovementAction.Update);

                        if (DynelManager.LocalPlayer.Position.DistanceFrom(_pos) <= 1f)
                        {
                            Chat.WriteLine("We are at safe position.");
                            Extensions.ResetInternals();
                            SMovementController.SetNavDestination(Constants.S13GoalPosition);
                        }
                        else { Chat.WriteLine("Broked"); }
                    }
                    else
                    {
                        if (Extensions.DangerousNearOurPositon())
                        {
                            if (SMovementController.IsNavigating() && DynelManager.LocalPlayer.Position.Distance2DFrom(S13Buddy._ourPosition) <= 35f) 
                            { 
                                SMovementController.Halt();
                                Chat.WriteLine($"It's not safe!");
                            }

                            if ((Extensions.TeamHealthy()
                                && !Team.Members.Any(c => c.Character != null && (bool)c.Character?.IsMoving && Extensions.InCeiling(c.Character)) 
                                && !Team.Members.Any(c => c.Character == null))
                                    || Extensions.InCombat()
                                    || !S13Buddy._settings["Waiting"].AsBool())
                            {
                                S13Buddy._safe = true;
                            }
                        }
                        else
                        {
                            if (SMovementController.IsNavigating() && DynelManager.LocalPlayer.Position.Distance2DFrom(S13Buddy._ourPosition) <= 35f) { SMovementController.Halt(); }

                            S13Buddy._safe = true;
                        }
                    }
                }
                else
                {
                    if (Extensions.CanProceed() && !Extensions.IsCasting() && Extensions.SelfHealthy())
                    {
                        Extensions.HandleCeilingPathing();
                    }
                    else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                }

                #endregion
            }
            else
            {
                #region Time-Out

                if (Team.Members.Select(c => c.Name).Where(c => c == DynelManager.LocalPlayer.Name).ToList().Count > 1) { Team.Leave(); }

                if (Time.NormalTime > S13Buddy._stateTimeOut + 600f)
                {
                    if (Team.IsInTeam) 
                    {
                        if (DynelManager.LocalPlayer.Identity == S13Buddy.Leader) { Team.Disband(); }
                        else { Team.Leave(); }
                    }
                }

                #endregion

                #region Merge Pathing

                if (!S13Buddy._merged && S13Buddy._merging)
                {
                    if (Extensions.TryGetLeader(out S13Buddy._leader) != null)
                    {
                        if (DynelManager.LocalPlayer.Position.Distance2DFrom((Vector3)Extensions.TryGetLeader(out S13Buddy._leader)?.Position) <= 40f)
                        {
                            if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                            else
                            {
                                Vector3 _pos = new Vector3(DynelManager.LocalPlayer.Position.X, 65.6f, DynelManager.LocalPlayer.Position.Z);

                                DynelManager.LocalPlayer.Position = _pos;
                                SMovementController.SetMovement(MovementAction.Update);

                                SMovementController.SetNavDestination(Constants.S13GoalPosition);

                                S13Buddy._merged = true;
                            }
                        }
                        else
                        {
                            if (Extensions.CanProceed() && !Extensions.IsCasting() && !Extensions.GreaterThreat())
                            {
                                Extensions.HandleCeilingPathing();
                            }
                            else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                        }
                    }
                    else
                    {
                        if (Extensions.CanProceed() && !Extensions.IsCasting())
                        {
                            if (!Extensions.GreaterThreat()) { Extensions.HandleCeilingPathing(); }
                            else if (SMovementController.IsNavigating()) 
                            {
                                S13Buddy._merged = true;
                                SMovementController.Halt();
                            }
                        }
                        else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                    }

                    return;
                }

                #endregion

                if (DynelManager.LocalPlayer.Identity != S13Buddy.Leader)
                {
                    if (!S13Buddy._merged && S13Buddy._merging) { return; }

                    if (S13Buddy._settings["Waiting"].AsBool())
                    {
                        if (Extensions.TryGetLeader(out S13Buddy._leader) != null)
                        {
                            #region Scan

                            if (Extensions.TryGetLeader(out S13Buddy._leader)?.IsAttacking == true)
                            {
                                SimpleChar targetMob = DynelManager.NPCs
                                    .FirstOrDefault(c => c.Health > 0
                                        && !Constants._ignores.Contains(c.Name)
                                        && c.Identity == (Identity)Extensions.TryGetLeader(out S13Buddy._leader)?.FightingTarget?.Identity);

                                if (targetMob != null)
                                {
                                    _target = targetMob;
                                    Chat.WriteLine($"Found target: {_target.Name}");
                                    return;
                                }
                            }

                            #endregion

                            #region Pathing

                            if (Extensions.CanProceed() && Extensions.SelfHealthy() && !Extensions.IsCasting()
                                && DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)Extensions.TryGetLeader(out S13Buddy._leader)?.Position) > 3f)
                            {
                                SMovementController.SetNavDestination((Vector3)Extensions.TryGetLeader(out S13Buddy._leader)?.Position);
                            }
                            else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }

                            #endregion
                        }
                        else
                        {
                            #region Scan

                            if (Extensions.InCombat())
                            {
                                HandleScan();
                            }
                            //else if (Extensions.Dangerous2D() && SMovementController.IsNavigating()) { SMovementController.Halt(); }

                            #endregion

                            #region Pathing

                            if (Extensions.CanProceed() && !Extensions.LesserThreat() && (Extensions.SelfHealthy() || Extensions.InCombat()))
                            {
                                SMovementController.SetNavDestination(Constants.S13GoalPosition);
                            }
                            else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }

                            #endregion
                        }
                    }
                    else
                    {
                        #region Scan

                        HandleScan();

                        #endregion

                        #region Pathing

                        if (Extensions.CanProceed() && (Extensions.SelfHealthy() || Extensions.InCombat()))
                        {
                            SMovementController.SetNavDestination(Constants.S13GoalPosition);
                        }
                        else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }

                        #endregion
                    }
                }
                else
                {
                    #region Scan

                    HandleScan();

                    #endregion

                    #region Pathing

                    if (S13Buddy._settings["Waiting"].AsBool())
                    {
                        if (Extensions.CanProceed() && !Extensions.IsCasting() && Extensions.TeamHealthy() && Extensions.TeamValid())
                        {
                            SMovementController.SetNavDestination(Constants.S13GoalPosition);
                        }
                        else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                    }
                    else
                    {
                        if (Extensions.CanProceed() && (Extensions.SelfHealthy() || Extensions.InCombat()))
                        {
                            SMovementController.SetNavDestination(Constants.S13GoalPosition);
                        }
                        else if (SMovementController.IsNavigating()) { SMovementController.Halt(); }
                    }

                    #endregion
                }
            }
        }
    }
}