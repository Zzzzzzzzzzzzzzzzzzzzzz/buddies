﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Configuration;
using System.Linq;

namespace S13Buddy
{
    public class FightState : IState
    {
        public static int _aggToolCounter = 0;
        public static int _attackTimeout = 0;

        public static bool _timeOut = false;

        private SimpleChar _target;

        public static double _fightStartTime;
        public const double FightTimeout = 45f;

        public FightState(SimpleChar target)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied()) { return new DiedState(); }

            if (Extensions.CanExit()) { return new ExitSectorState(); }

            if (Extensions.ShouldIdle()) { return new IdleState(); }

            if (Extensions.ShouldReform()) { return new ReformState(); }

            if (Extensions.IsNull(_target) || Time.NormalTime > _fightStartTime + FightTimeout 
                || (Extensions.InCombat() && Extensions.AlienStuck(_target))) { return new WorkState(); }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine($"FightState::OnStateEnter");

            //Edge case correction: Stuck sitting.
            //S13Buddy._initSit = false;

            if (S13Buddy.ModeSelection.Patrol == (S13Buddy.ModeSelection)S13Buddy._settings["ModeSelection"].AsInt32()) { SMovementController.Halt(); }

            _fightStartTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            Chat.WriteLine("FightState::OnStateExit");

            _target = null;

            Extensions.ResetInternals();

            _aggToolCounter = 0;
            _attackTimeout = 0;
        }

        public void Tick()
        {
            if (Game.IsZoning || _target == null || S13Buddy._initSit) { return; }

            if (Extensions.InCeiling())
            {
                DynelManager.LocalPlayer.Position = new Vector3(DynelManager.LocalPlayer.Position.X, _target.Position.Y, DynelManager.LocalPlayer.Position.Z);
                SMovementController.SetMovement(MovementAction.Update);
            }

            //if (!SMovementController.IsNavigating() && !(bool)_target?.IsInLineOfSight)
            //    Extensions.LineOfSightCorrection(_target);

            if (S13Buddy.ModeSelection.Patrol == (S13Buddy.ModeSelection)S13Buddy._settings["ModeSelection"].AsInt32())
            {
                if (DynelManager.LocalPlayer.Identity != S13Buddy.Leader)
                {
                    if (!_target.IsMoving)
                    {
                        Extensions.HandlePathing(_target);
                        Extensions.HandleAttacking(_target);
                    }
                }
                else
                {
                    if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f) 
                    {
                        if (Extensions.InCombat() && !Extensions.AlienStuck(_target))
                        {
                            if (!_target.IsMoving)
                            {
                                Extensions.HandlePathing(_target);
                                Extensions.HandleAttacking(_target);
                            }
                            else { Extensions.HandleTaunting(_target); }
                        }
                        else if (!SMovementController.IsNavigating()) { Extensions.HandleTaunting(_target); }
                    }
                    else if (!_target.IsMoving)
                    {
                        Extensions.HandlePathing(_target);
                        Extensions.HandleAttacking(_target);
                    }
                }
            }
            else
            {
                Extensions.HandlePathing(_target);
                Extensions.HandleAttacking(_target);
            }
        }
    }
}