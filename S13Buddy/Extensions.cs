﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using static S13Buddy.S13Buddy;
using AOSharp.Pathfinding;

namespace S13Buddy
{
    public static class Extensions
    {
        public static bool _initCount = false;

        public static bool FindTool(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants._toolNames.Contains(c.Name))) != null;
        }

        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending) { return true; }

            if (DynelManager.Characters
                .Any(c => c.Health > 0
                    && c.FightingTarget != null
                    && !InCeiling(c.FightingTarget)
                    && ZixCheck(c))) { return true; }

            return false;
        }

        public static bool ZixCheck(SimpleChar _target)
        {
            if (_target.IsPet && _target.Name == "Zix" && !DynelManager.NPCs.Any(c => c.Health > 0 && c.Position.Distance2DFrom(DynelManager.LocalPlayer.Position) <= 10f))
            { return false; }

            return true;
        }

        public static bool ShouldHalt()
        {
            return _settings["Waiting"].AsBool() && !InCombat() && (!TeamHealthy() || Team.Members.Any(c => c.Character == null));
        }

        public static bool HasDied()
        {
            return Playfield.ModelIdentity.Instance == Constants.XanHubId
                    || Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                    || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                        && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13ZoneOutPosition) > 10f);
        }

        public static void ResetInternals()
        {
            _died = false;
            _safe = false;
            LeechState._counter = 0;
        }

        public static bool ShouldReform()
        {
            return Playfield.ModelIdentity.Instance == Constants.APFHubId && !Team.IsInTeam;
        }

        public static bool CanExit()
        {
            return Playfield.ModelIdentity.Instance == Constants.S13Id
                && DynelManager.LocalPlayer.Position.Distance2DFrom(Constants.S13GoalPosition) <= 5f
                && !InCombat();
        }
        public static bool InCeiling(SimpleChar target)
        {
            return target?.Position.Y >= 65.0f && target?.Position.Y <= 68.4f;
        }

        public static bool IsCasting()
        {
            if (_casting)
            {
                if (Time.NormalTime > _castTimer + _castRecharge)
                {
                    _casting = false;
                }
                else { return true; }
            }

            return false;
        }

        public static bool InCeiling()
        {
            return DynelManager.LocalPlayer.Position.Y >= 65.0f && DynelManager.LocalPlayer.Position.Y <= 68.4f;
        }

        public static void LineOfSightCorrection(SimpleChar target)
        {
            if (!AlienStuck(target) && target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 17f)
                SMovementController.SetNavDestination(target.Position);
        }
        public static bool TryGetHealPet(out Pet healPet)
        {
            return (healPet = DynelManager.LocalPlayer.Pets.FirstOrDefault(c => c.Type == PetType.Heal)) != null;
        }

        public static bool PetNanoLow()
        {
            Pet _healPet;

            return (TryGetHealPet(out _healPet) && (_healPet.Character?.Nano / PetMaxNanoPool(_healPet) * 100 <= 55
                    && _healPet.Character?.Nano != 10
                    && DynelManager.LocalPlayer.DistanceFrom(_healPet.Character) < 10f && _healPet.Character?.IsInLineOfSight == true));
        }

        public static float PetMaxNanoPool(Pet _healPet)
        {
            if (_healPet.Character.Level == 215)
                return 5803;
            else if (_healPet.Character.Level == 192)
                return 13310;
            else if (_healPet.Character.Level == 169)
                return 11231;
            else if (_healPet.Character.Level == 146)
                return 9153;
            else if (_healPet.Character.Level == 123)
                return 7169;
            else if (_healPet.Character.Level == 99)
                return 5327;
            else if (_healPet.Character.Level == 77)
                return 3807;
            else if (_healPet.Character.Level == 55)
                return 2404;
            else if (_healPet.Character.Level == 33)
                return 1234;
            else if (_healPet.Character.Level == 14)
                return 414;

            return 0;
        }

        public static bool ShouldIdle()
        {
            return !Team.IsInTeam && LookingSelection.Team == (LookingSelection)_settings["LookingSelection"].AsInt32() && Playfield.ModelIdentity.Instance == Constants.APFHubId;
        }

        public static bool ShouldIdle(double _time, float _timeOut)
        {
            return !Team.IsInTeam && LookingSelection.Team == (LookingSelection)_settings["LookingSelection"].AsInt32() && Time.NormalTime > _time + _timeOut && Playfield.ModelIdentity.Instance == Constants.APFHubId;
        }

        public static bool TimedOut(double _time, float _timeOut)
        {
            return Team.IsInTeam && Time.NormalTime > _time + _timeOut && Playfield.ModelIdentity.Instance == Constants.APFHubId;
        }

        public static bool IsNull(SimpleChar _target)
        {
            return _target == null
                || _target?.IsPet == true
                || _target?.IsValid == false
                || _target?.Health == 0;
        }

        public static void HandleCeilingPathing()
        {
            if (LeechState._counter <= Constants._mergeLeecherPath.Count - 1)
            {
                if (!_initCount)
                {
                    _initCount = true;

                    Vector3 _position = Constants._mergeLeecherPath.OrderBy(c => c.Distance2DFrom(DynelManager.LocalPlayer.Position)).FirstOrDefault();

                    for (int i = 0; i < Constants._mergeLeecherPath.Count; i++)
                    {
                        if (Constants._mergeLeecherPath[i] == _position) { LeechState._counter = i; }
                    }
                }

                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._mergeLeecherPath.ElementAt(LeechState._counter)) < 1f)
                {
                    LeechState._counter++;
                    return;
                }

                SMovementController.SetDestination(Constants._mergeLeecherPath.FirstOrDefault(c => LeechState._counter <= Constants._mergeLeecherPath.Count && c == Constants._mergeLeecherPath.ElementAt(LeechState._counter)));
            }
            else
            {
                LeechState._counter = 0;

                SMovementController.SetDestination(Constants._mergeLeecherPath.FirstOrDefault(c => LeechState._counter <= Constants._mergeLeecherPath.Count && c == Constants._mergeLeecherPath.ElementAt(LeechState._counter)));
            }
        }

        public static bool Rooted()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot);
        }

        public static bool SelfHealthy()
        {
            return DynelManager.LocalPlayer.HealthPercent >= 66 && DynelManager.LocalPlayer.NanoPercent >= 66;
        }

        public static bool TeamValid()
        {
            return !Team.Members.Any(c => c.Character == null)
                && !Team.Members.Any(c => c.Character != null && c.Character?.Position.Distance2DFrom(DynelManager.LocalPlayer.Position) > 11f);
        }

        public static bool TeamHealthy()
        {
            return Team.Members.Where(c => c.Character != null && c.Character.HealthPercent >= 66 && c.Character.NanoPercent >= 66).Count() 
                == Team.Members.Where(c => c.Character != null).Count();
        }

        public static bool AlienStuck(SimpleChar _target)
        {
            //Edge case correction: Alien trapped moving or not.
            return _target.Position.DistanceFrom(new Vector3(61.8f, 20.3f, 350.9f)) <= 1f//*
                    || (!_target.IsMoving //68.3, 386.8, 5.2,
                        && (_target.Position.DistanceFrom(new Vector3(67.9f, 5.2f, 387.5f)) <= 0.5f
                            || _target.Position.Distance2DFrom(new Vector3(113.0f, 35.6f, 259.3f)) <= 1.5f
                            || _target.Position.Distance2DFrom(new Vector3(192.8f, 36.2f, 246.5f)) <= 1.1f//*
                            || _target.Position.Distance2DFrom(new Vector3(190.3f, 37.1f, 244.9f)) <= 1.1f));//*
        }

        public static bool DangerousNearOurPositon()
        {
            return !_safe
                && DynelManager.NPCs
                    .Where(c => c.Health > 0
                        && !c.IsAttacking
                        && !c.IsMoving
                        && c.Name != "Zix"
                        && !Constants._ignores.Contains(c.Name)
                        && !AlienStuck(c)
                        && c.Position.Distance2DFrom(_ourPosition) <= 35f)
                    .ToList().Count() > 0;
        }

        public static bool OrgCheck(SimpleChar _target)
        {
            if (!_settings["OurOrg"].AsBool()) { return true; }

            return _settings["OurOrg"].AsBool() && DynelManager.LocalPlayer.GetStat(Stat.Clan) == _target?.GetStat(Stat.Clan);
        }

        public static bool IsFlagged(SimpleChar _target)
        {
            if (_target.Buffs.Contains(216382) || _target.Buffs.Contains(214879) || _target.Buffs.Contains(284620) || _target.Buffs.Contains(202732))
                return true;

            return false;
        }

        public static bool LesserThreat()
        {
            return DynelManager.NPCs
                    .Where(c => c.Health > 0
                        && c.Name != "Zix"
                        && !Constants._ignores.Contains(c.Name)
                        && !c.IsMoving
                        && c.IsInLineOfSight
                        && c.Position.Distance2DFrom(DynelManager.LocalPlayer.Position) <= 30f)
                    .ToList().Count() > 0;
        }

        public static bool GreaterThreat()
        {
            return DynelManager.NPCs
                    .Where(c => c.Health > 0
                        && c.Name != "Zix"
                        && !Constants._ignores.Contains(c.Name)
                        && !c.IsMoving
                        && c.Position.Distance2DFrom(DynelManager.LocalPlayer.Position) <= 70f)
                    .ToList().Count() > 5;
        }

        public static bool CanProceed()
        {
            return DynelManager.LocalPlayer.MovementState == MovementState.Run
                && !_initSit
                && !DynelManager.LocalPlayer.IsFalling
                && !Rooted();
        }

        public static bool CanSetPosition()
        {
            return !_died
                && DynelManager.LocalPlayer.Health > 0
                && Playfield.ModelIdentity.Instance == Constants.S13Id;
        }

        public static bool CanUseSitKit()
        {
            if ((!IsCasting() || !_settings["Waiting"].AsBool()) && DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving)
            {
                if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if ((bool)sitKits?.Any()) { return true; }
            }

            return false;
        }

        public static CharacterWieldedWeapon GetWieldedWeapons(SimpleChar local) => (CharacterWieldedWeapon)local.GetStat(Stat.EquippedWeapons);

        public static bool Waited(double _timer)
        {
            if (_died)
            {
                if (!_settings["Waiting"].AsBool())
                {
                    return Time.NormalTime > _timer + 26f;
                }

                return Time.NormalTime > _timer + 38f;
            }
            else
            {
                return  Time.NormalTime > _timer + 6f;
            }
        }

        public static SimpleChar TryGetAlien(out SimpleChar _alien)
        {
            if (ModeSelection.Gather == (ModeSelection)_settings["ModeSelection"].AsInt32())
            {
                return _alien = DynelManager.NPCs
                    .Where(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 30f
                        && c.Health > 0
                        && c.IsValid
                        && c.IsInLineOfSight
                        && !AlienStuck(c)
                        && !Constants._ignores.Contains(c.Name)
                        && c.Name != "Zix")
                    .OrderBy(c => c.HealthPercent)
                    .ThenByDescending(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                    .FirstOrDefault();
            }

            return _alien = DynelManager.NPCs
                .Where(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 30f
                    && c.Health > 0
                    && c.IsValid
                    && c.IsInLineOfSight
                    && !AlienStuck(c)
                    && !Constants._ignores.Contains(c.Name)
                    && c.Name != "Zix")
                .OrderBy(c => c.HealthPercent)
                .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();
        }

        public static SimpleChar TryGetLeader(out SimpleChar _leader)
        {
            return _leader = Team.Members
                    .Where(c => c.Character?.Health > 0
                        && c.Identity != DynelManager.LocalPlayer.Identity
                        && c.Character?.IsValid == true
                        && (c.Identity == Leader || c.IsLeader || c.Character?.Name == LeaderName))
                    .FirstOrDefault()?.Character;
        }

        public static Dynel TryGetLever(out Dynel _lever)
        {
            return _lever = DynelManager.AllDynels
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault(c => c.Name == "A Lever");
        }

        public static void HandleTaunting(SimpleChar _target)
        {
            if (LocalCooldown.IsOnCooldown(Stat.Psychology)) { return; }

            if (FightState._aggToolCounter >= 2)
            {
                //Edge case correction: If we get stuck on a mob path to it.
                if (FightState._attackTimeout > 1)
                {
                    SMovementController.SetNavDestination(_target.Position);
                    FightState._attackTimeout = 0;
                    FightState._aggToolCounter = 0;
                    //FightState._timeOut = true;
                }
                else
                {
                    FightState._aggToolCounter = 0;
                    FightState._attackTimeout++;
                }
            }
            else if (FindTool(out Item _tool))
            {
                if (!Item.HasPendingUse && !PerkAction.List.Any(c => c.IsExecuting || c.IsPending))
                {
                    _tool.Use(_target, true);
                    FightState._aggToolCounter++;
                }
            }
        }
        public static void HandleAttacking(SimpleChar target)
        {
            CharacterWieldedWeapon _wep = (CharacterWieldedWeapon)DynelManager.LocalPlayer.GetStat(Stat.EquippedWeapons);

            if (!SMovementController.IsNavigating() && target.IsInLineOfSight)
            {
                if (target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 8f
                    && (_wep.HasFlag(CharacterWieldedWeapon.Melee) || _wep.HasFlag(CharacterWieldedWeapon.MartialArts) || DynelManager.LocalPlayer.Profession == Profession.Engineer))
                {
                    if (DynelManager.LocalPlayer.FightingTarget == null
                        && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                    {
                        DynelManager.LocalPlayer.Attack(target, false);
                        Chat.WriteLine($"Attacking {target.Name}.");
                    }
                }

                if (ModeSelection.Patrol == (ModeSelection)_settings["ModeSelection"].AsInt32())
                {
                    if (target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                        && DynelManager.LocalPlayer.Profession != Profession.Engineer
                        && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                    {
                        if (DynelManager.LocalPlayer.FightingTarget == null
                            && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                        {
                            DynelManager.LocalPlayer.Attack(target, false);
                            Chat.WriteLine($"Attacking {target.Name}.");
                        }
                    }
                }
                else
                {
                    if (target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 17f
                        && DynelManager.LocalPlayer.Profession != Profession.Engineer
                        && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                    {
                        if (DynelManager.LocalPlayer.FightingTarget == null
                            && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                        {
                            DynelManager.LocalPlayer.Attack(target, false);
                            Chat.WriteLine($"Attacking {target.Name}.");
                        }
                    }
                }
            }
        }

        public static void HandlePathing(SimpleChar target)
        {
            CharacterWieldedWeapon _wep = (CharacterWieldedWeapon)DynelManager.LocalPlayer.GetStat(Stat.EquippedWeapons);

            if (!SMovementController.IsNavigating() && target?.IsInLineOfSight == false) { SMovementController.SetNavDestination((Vector3)target?.Position); }

            if (SMovementController.IsNavigating() && target?.IsInLineOfSight == true)
            {
                if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 8f
                    && (_wep.HasFlag(CharacterWieldedWeapon.Melee) || _wep.HasFlag(CharacterWieldedWeapon.MartialArts) || DynelManager.LocalPlayer.Profession == Profession.Engineer))
                    SMovementController.Halt();

                if (ModeSelection.Patrol == (ModeSelection)_settings["ModeSelection"].AsInt32())
                {
                    if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                        && DynelManager.LocalPlayer.Profession != Profession.Engineer
                        && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                        SMovementController.Halt();
                }
                else
                {
                    if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 17f
                        && DynelManager.LocalPlayer.Profession != Profession.Engineer
                        && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                        SMovementController.Halt();
                }
            }

            if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 8f
                && (_wep.HasFlag(CharacterWieldedWeapon.Melee) || _wep.HasFlag(CharacterWieldedWeapon.MartialArts) || DynelManager.LocalPlayer.Profession == Profession.Engineer))
                SMovementController.SetNavDestination((Vector3)target?.Position);

            if (ModeSelection.Patrol == (ModeSelection)_settings["ModeSelection"].AsInt32())
            {
                if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                    && DynelManager.LocalPlayer.Profession != Profession.Engineer
                    && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                    SMovementController.SetNavDestination((Vector3)target?.Position);
            }
            else
            {
                if (target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 17f
                    && DynelManager.LocalPlayer.Profession != Profession.Engineer
                    && (_wep.HasFlag(CharacterWieldedWeapon.Ranged) || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician))
                    SMovementController.SetNavDestination((Vector3)target?.Position);
            }
        }

        [Flags]
        public enum CharacterWieldedWeapon
        {
            Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
            MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
            Melee = 0x02,             // 0x00000000000000000010b
            Ranged = 0x04,            // 0x00000000000000000100b
            Bow = 0x08,               // 0x00000000000000001000b
            Smg = 0x10,               // 0x00000000000000010000b
            Edged1H = 0x20,           // 0x00000000000000100000b
            Blunt1H = 0x40,           // 0x00000000000001000000b
            Edged2H = 0x80,           // 0x00000000000010000000b
            Blunt2H = 0x100,          // 0x00000000000100000000b
            Piercing = 0x200,         // 0x00000000001000000000b
            Pistol = 0x400,           // 0x00000000010000000000b
            AssaultRifle = 0x800,     // 0x00000000100000000000b
            Rifle = 0x1000,           // 0x00000001000000000000b
            Shotgun = 0x2000,         // 0x00000010000000000000b
            Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
            MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
            RangedEnergy = 0x10000,   // 0x00010000000000000000b
            Grenade2 = 0x20000,        // 0x00100000000000000000b
            HeavyWeapons = 0x40000,   // 0x01000000000000000000b
        }
    }
}
