﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Core.IPC;
using AOSharp.Pathfinding;
using S13Buddy.IPCMessages;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;
using AOSharp.Common.GameData.UI;
using System.Security.Cryptography;
using System.Threading;
using AOSharp.Core.Misc;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace S13Buddy
{
    public class S13Buddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;
        public static string LeaderName = string.Empty;
        public static SimpleChar _leader;
        public static Vector3 _ourPosition = Vector3.Zero;
        public static Item _kit;
        public static Spell _isCastingSpellDummy;

        public static DateTime _previousRound = DateTime.MinValue;
        public static DateTime _currentRound = DateTime.MinValue;
        public static TimeSpan _missionTimeGuesstimation = TimeSpan.Zero;

        public static SimpleChar _alien;
        public static SimpleChar _targetAlien;

        public static TextInputView _leaderInput;
        public static TextInputView _channelInput;
        public static TextInputView _tickInput;

        public static Dynel _lever;

        public static float Tick = 0;

        public static double _castTimer;
        public static double _castRecharge;
        public static bool _casting = false;

        public static bool Toggle = false;

        public static bool _merging = false;
        public static bool _merged = false;
        public static bool _died = false;
        public static bool _safe = false;

        public static bool _initSit = false;

        public static List<Identity> _invitedIdentities = new List<Identity>();

        public static bool _subbedUIEvents = false;

        public static double _stateTimeOut;
        public static double _mainUpdate;
        public static double _lftUpdate;
        public static double _sitTimer;
        public static double _lastZonedTime = Time.NormalTime;

        public static int _sitTimeOut = 0;

        public static SPath _unicornHubSPath;
        //public static SPath _sectorHubSPath;

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        private bool IsActiveWindow => GetForegroundWindow() == Process.GetCurrentProcess().MainWindowHandle;

        public static Window _infoWindow;

        public static Settings _settings;

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            try
            {
                #region Settings

                _settings = new Settings("S13Buddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\S13Buddy\\{Game.ClientInst}\\Config.json");

                SettingsController.RegisterSettingsWindow("S13Buddy", pluginDir + "\\UI\\S13BuddySettingWindow.xml", _settings);

                _settings.AddVariable("LookingSelection", (int)LookingSelection.None);
                _settings.AddVariable("ModeSelection", (int)ModeSelection.Patrol);

                _settings.AddVariable("Toggle", false);

                _settings.AddVariable("Merging", false);
                _settings.AddVariable("OurOrg", false);
                _settings.AddVariable("Waiting", true);

                _settings["Toggle"] = false;

                #endregion

                #region NavMesh Controller

                SMovementControllerSettings mSettings = new SMovementControllerSettings
                {
                    NavMeshSettings = SNavMeshSettings.Default,
                    PathSettings = SPathSettings.Default,
                };

                mSettings.PathSettings.Extents = new Vector3(5, 100, 5);
                SMovementController.Set(mSettings);
                SMovementController.AutoLoadNavmeshes($"{pluginDir}\\NavMeshes");
                SMovementController.ToggleNavMeshDraw();
                SMovementController.TogglePathDraw();
                SMovementController.Stuck += OnStuck;
                //SMovementController.SetStuckLogic(() =>
                //{
                //    //*Edge case correction: Stuck Ceiling pathing.
                //    if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(158.3f, 67.6f, 249.9f)) <= 3f)
                //        DynelManager.LocalPlayer.Position = new Vector3(155.3f, 68.2f, 249.9f);
                //    else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(152.2f, 65.0f, 117.0f)) <= 4f)
                //        DynelManager.LocalPlayer.Position = new Vector3(154.1f, 67.6f, 121.8f);
                //    else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(110.8f, 67.6f, 117.6f)) <= 4f)
                //        DynelManager.LocalPlayer.Position = new Vector3(115.3f, 67.6f, 121.0f);
                //    else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(158.3f, 67.6f, 251.7f)) <= 2f)
                //        DynelManager.LocalPlayer.Position = new Vector3(154.6f, 68.5f, 251.7f);
                //});

                _unicornHubSPath = SPath.Create(false);

                _unicornHubSPath.AddPoints(new List<Vector3>
                {
                    new Vector3(156.5f, 100.9f, 175.0f),
                    new Vector3(143.6f, 100.9f, 172.3f),
                    new Vector3(140.2f, 100.9f, 177.0f),
                    new Vector3(139.4f, 101.0f, 195.7f),
                    new Vector3(139.0f, 101.0f, 269.7f),
                    new Vector3(135.7f, 100.9f, 277.4f),
                    new Vector3(108.7f, 100.9f, 274.5),
                    new Vector3(111.3f, 100.8f, 234.3f),
                    new Vector3(80.6f, 100.8f, 232.3f),
                    new Vector3(69.4f, 101.0f, 232.3f)
                });

                //_sectorHubSPath = SPath.Create(false);

                //_sectorHubSPath.AddPoints(new List<Vector3>
                //{
                //    new Vector3(192.4f, 0.3f, 245.8f),
                //    new Vector3(173.1f, 0.3f, 242.0f),
                //    new Vector3(159.1f, 3.0f, 233.1f),
                //    new Vector3(147.0f, 2.4f, 223.2f),
                //    new Vector3(141.7f, 1.9f, 214.8f),
                //    new Vector3(146.1f, 0.5f, 207.1f)
                //});


                //NavMeshMovementController = new NavMeshMovementController($"{pluginDir}\\NavMeshes", true);
                //MovementController.Set(NavMeshMovementController);

                #endregion

                #region IPC

                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);

                #endregion

                //IsCasting Bandaid
                _isCastingSpellDummy = Spell.List.FirstOrDefault(c => c.Id == 223372);
                LookingForTeam.Leave();

                Chat.WriteLine("S13Buddy Loaded! Version: 0.9.9.98");
                Chat.WriteLine("/s13buddy for settings.");

                #region Events

                Game.OnUpdate += OnUpdate;
                Game.TeleportEnded += OnEndZoned;
                DynelManager.DynelSpawned += OnDynelSpawned;
                LookingForTeam.SearchResultReceived += LookingForTeam_SearchResultReceived;
                Network.N3MessageSent += Network_N3MessageSent;

                #endregion

                #region Commands

                Chat.RegisterCommand("buddy", S13BuddyCommand);

                #endregion

                #region Init

                _stateMachine = new StateMachine(new IdleState());

                LeaderName = Config.CharSettings[Game.ClientInst].Leader;
                Tick = Config.CharSettings[Game.ClientInst].Tick;

                #endregion
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        #region Callbacks

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None) { Leader = new Identity(IdentityType.SimpleChar, sender); }

            if (Extensions.TryGetLeader(out _leader) != null)
            {
                Config.CharSettings[Game.ClientInst].Leader = $"{Extensions.TryGetLeader(out _leader)?.Name}";

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow?.FindView("LeaderBox", out _leaderInput);

                    if (_leaderInput != null)
                        _leaderInput.Text = Config.CharSettings[Game.ClientInst].Leader;
                }

                Config.Save();
            }

            Chat.WriteLine("Buddy enabled.");

            _settings["Toggle"] = true;
            Toggle = true;

            _stateMachine.SetState(new IdleState());
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            if (DynelManager.LocalPlayer.Identity == Leader || IsActiveWindow) { return; }

            _settings["Toggle"] = false;
            Toggle = false;

            Chat.WriteLine("Buddy disabled.");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();

            _stateMachine.SetState(new IdleState());
        }

        #endregion

        #region Events

        private void OnStuck(Vector3 stuckPos, Vector3 destPos)
        {
            Chat.WriteLine($"Stuck - {stuckPos} Destination - {destPos}");
        }

        private void OnEndZoned(object s, EventArgs e)
        {
            _lastZonedTime = Time.NormalTime;
        }

        private void LookingForTeam_SearchResultReceived(object s, LookingForTeamSearchResultEventArgs e)
        {
            if (LookingSelection.Member != (LookingSelection)_settings["LookingSelection"].AsInt32() || !_settings["Toggle"].AsBool()) { return; }

            foreach (LookingForTeamApplicant _app in e.Applicants)
            {
                if (!Team.Members.Select(c => c.Name).Contains(_app.Name)
                    && _app.Message.ToLower() != ""
                    && (_app.Message.ToLower().Contains("sector13")
                        || _app.Message.ToLower().Contains("s13")
                        || _app.Message.ToLower().Contains("aliumz")))
                {
                    string[] _split = _app.Message.ToLower().Split();

                    if (_settings["OurOrg"].AsBool())
                    {
                        if (_split.Contains($"{DynelManager.LocalPlayer.GetStat(Stat.Clan)}")) { Team.Invite(_app.Identity); }
                    }
                    else if (_split.Contains("axp") || _split.Contains("farm")) { Team.Invite(_app.Identity); }
                }
            }
        }

        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

            if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action == CharacterActionType.CastNano)
                {
                    if (Spell.Find(charActionMsg.Parameter2, out Spell _currentNano))
                    {
                        _casting = true;
                        _castTimer = Time.NormalTime;
                        _castRecharge = _currentNano.AttackDelay;
                    }
                }
            }
        }

        private void OnDynelSpawned(object s, Dynel dynel)
        {
            if (dynel.Identity.Type == IdentityType.SimpleChar)
            {
                if (Team.Members.Select(c => c?.Identity).Contains(dynel.Identity) && DynelManager.LocalPlayer.IsAttacking) 
                {
                    Chat.WriteLine($"Character spawned, stopping attack.");
                    DynelManager.LocalPlayer.StopAttack();
                }
            }
        }

        #endregion

        #region Handles

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\S13BuddyInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        #endregion

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning) { return; }

            #region Edge cases

            if (Toggle && Team.IsRaid && Leader == Identity.None && Extensions.TryGetLeader(out _leader) != null)
            {
                Config.CharSettings[Game.ClientInst].Leader = $"{Extensions.TryGetLeader(out _leader)?.Name}";
                Leader = (Identity)Extensions.TryGetLeader(out _leader)?.Identity;

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow?.FindView("LeaderBox", out _leaderInput);

                    if (_leaderInput != null)
                        _leaderInput.Text = Config.CharSettings[Game.ClientInst].Leader;
                }

                Config.Save();
            }

            #endregion

            #region LFT

            if (Toggle && Time.NormalTime > _lftUpdate + 15f)
            {
                _lftUpdate = Time.NormalTime;

                if (Team.IsRaid && LookingSelection.Member == (LookingSelection)_settings["LookingSelection"].AsInt32()
                    && Leader == DynelManager.LocalPlayer.Identity && !ReformState.Reforming) { LookingForTeam.Search(); }
            }

            //Edge case correction: Griefers
            if (Team.IsInTeam && Team.Members.Any(c => c.Character != null && Extensions.IsFlagged(c.Character)))
            {
                if (DynelManager.LocalPlayer.Identity == Leader)
                    Team.Kick((Identity)Team.Members.FirstOrDefault(c => c.Character != null && Extensions.IsFlagged(c.Character)).Character?.Identity);
            }

            #endregion

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                _mainUpdate = Time.NormalTime;

                #region Stuck

                if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(158.3f, 67.6f, 249.9f)) <= 0.5f)
                    DynelManager.LocalPlayer.Position = new Vector3(155.3f, 68.2f, 249.9f);
                else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(152.2f, 65.0f, 117.0f)) <= 0.5f)
                    DynelManager.LocalPlayer.Position = new Vector3(154.1f, 67.6f, 121.8f);
                else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(110.8f, 67.6f, 117.6f)) <= 0.5f)
                    DynelManager.LocalPlayer.Position = new Vector3(115.3f, 67.6f, 121.0f);
                else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(158.3f, 67.6f, 251.7f)) <= 0.5f)
                    DynelManager.LocalPlayer.Position = new Vector3(154.6f, 68.5f, 251.7f);
                else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(168.0f, 67.6f, 170.0f)) <= 0.5f)
                    DynelManager.LocalPlayer.Position = new Vector3(172.3f, 68.2f, 170.0f);
                else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(168.0f, 67.6f, 170.0f)) <= 0.5f)
                    DynelManager.LocalPlayer.Position = new Vector3(172.3f, 68.2f, 170.0f);
                else if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(158.3f, 67.6f, 254.1f)) <= 0.2f)
                    DynelManager.LocalPlayer.Position = new Vector3(157.8f, 68.5f, 254.1f);

                #endregion

                if (Extensions.CanSetPosition()) { _ourPosition = DynelManager.LocalPlayer.Position; }

                ListenerSit();

                _stateMachine.Tick();
            }

            #region UI Update

            if (_subbedUIEvents && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out _channelInput);
                SettingsController.settingsWindow.FindView("LeaderBox", out _leaderInput);
                SettingsController.settingsWindow.FindView("TickBox", out _tickInput);

                if (_channelInput != null && !string.IsNullOrEmpty(_channelInput.Text)
                    && int.TryParse(_channelInput.Text, out int channelValue)
                    && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (_tickInput != null && !string.IsNullOrEmpty(_tickInput.Text)
                    && float.TryParse(_tickInput.Text, out float tickValue)
                    && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                    Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (_leaderInput != null && !string.IsNullOrEmpty(_leaderInput.Text)
                    && Config.CharSettings[Game.ClientInst].Leader != _leaderInput.Text)
                    Config.CharSettings[Game.ClientInst].Leader = _leaderInput.Text;

                if (SettingsController.settingsWindow.FindView("S13BuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (Leader == Identity.None && IsActiveWindow)
                    {
                        if (LookingSelection.Team != (LookingSelection)_settings["LookingSelection"].AsInt32())
                            Leader = DynelManager.LocalPlayer.Identity;

                        if (DynelManager.LocalPlayer.Identity == Leader) { IPCChannel.Broadcast(new StartMessage()); }

                        Start();
                    }
                    else
                    {
                        IPCChannel.Broadcast(new StartMessage());

                        Start();
                    }
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Stop();

                    IPCChannel.Broadcast(new StopMessage());
                }
            }

            #endregion
        }

        #region Misc

        private void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;
            LookingForTeam.Leave();

            Chat.WriteLine("Buddy disabled.");

            _stateMachine.SetState(new IdleState());

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].LeaderChangedEvent -= Leader_Changed;
                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].LeaderChangedEvent += Leader_Changed;
                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        private void ListenerSit()
        {
            if (DynelManager.LocalPlayer.Profession == Profession.Metaphysicist
                && Extensions.PetNanoLow()) { return; }

            if (_initSit)
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                {
                    if (Extensions.InCombat() || (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66))
                    {
                        _initSit = false;
                        SMovementController.SetMovement(MovementAction.LeaveSit);
                    }
                }
                else if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66)
                { _initSit = false; }
            }
            else if (!Extensions.InCombat() && Extensions.CanUseSitKit())
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Run)
                {
                    if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment) /*!LocalCooldown.IsOnCooldown(Stat.Treatment)*/
                        && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
                    {
                        _initSit = true;
                        SMovementController.SetMovement(MovementAction.SwitchToSit);
                    }
                }
                else if (DynelManager.LocalPlayer.MovementState == MovementState.Sit) { SMovementController.SetMovement(MovementAction.LeaveSit); }
            }


            //if (Extensions.CanUseSitKit())
            //{
            //    if (_initSit)
            //    {
            //        if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //        {
            //            if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66)
            //            {
            //                _initSit = false;
            //                SMovementController.SetMovement(MovementAction.LeaveSit);
            //            }
            //        }
            //        else { _initSit = false; }
            //    }
            //    else if (!Extensions.InCombat() && !LocalCooldown.IsOnCooldown(Stat.Treatment))
            //    {
            //        if (Extensions.PetNanoLow()) { return; }

            //        if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
            //        {
            //            _initSit = true;
            //            SMovementController.SetMovement(MovementAction.SwitchToSit);
            //        }
            //        else if (DynelManager.LocalPlayer.MovementState == MovementState.Sit) { SMovementController.SetMovement(MovementAction.LeaveSit); }
            //    }
            //}

            //if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
            //{
            //    if (!_initSit)
            //    {
            //        if (DynelManager.LocalPlayer.MovementState == MovementState.Run)
            //        {
            //            Chat.WriteLine($"Entering sit.");
            //            Chat.WriteLine($"_initSit = true");
            //            _initSit = true;
            //            SMovementController.SetMovement(MovementAction.SwitchToSit);
            //        }
            //    }
            //}
            //else
            //{
            //    if (_initSit) 
            //    {
            //        Chat.WriteLine($"_initSit = false");
            //        _initSit = false;
            //    }

            //    if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //    {
            //        Chat.WriteLine($"Leaving sit.");
            //        SMovementController.SetMovement(MovementAction.LeaveSit);
            //    }
            //}


            //if (!_initSit
            //    && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            //{
            //    if (!_initSit && DynelManager.LocalPlayer.MovementState == MovementState.Run)
            //    {
            //        _initSit = true;
            //        SMovementController.SetMovement(MovementAction.SwitchToSit);
            //    }
            //}
            //else if (_initSit && DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //{
            //    if (_initSit) { _initSit = false; }

            //    SMovementController.SetMovement(MovementAction.LeaveSit);
            //}

            //if (_initSit == false && !Extensions.InCombat() && Extensions.CanUseSitKit() && !S13Buddy.IsCasting && !Spell.HasPendingCast
            //    && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            //{
            //    if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66
            //        && (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment) || _initSit))
            //    {
            //        if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //            MovementController.Instance.SetMovement(MovementAction.LeaveSit);

            //        if (_initSit)
            //            _initSit = false;
            //    }

            //    if (Extensions.CanUseSitKit() && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            //    {
            //        if (!_initSit && DynelManager.LocalPlayer.MovementState != MovementState.Sit)
            //        {
            //            _initSit = true;
            //            MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //        }
            //    }


            //    Task.Factory.StartNew(
            //        async () =>
            //        {
            //            _initSit = true;
            //            await Task.Delay(400);
            //            MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //            await Task.Delay(1800);
            //            //Edge case correction: Handler decides to fail??
            //            //if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
            //            //{
            //            //    if (_sitTimeOut >= 4)
            //            //    {
            //            //        if (Inventory.Find(297274, out Item _premKit, false)) 
            //            //        {
            //            //            _premKit.Use(DynelManager.LocalPlayer, true);
            //            //        }

            //            //        List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

            //            //        if (sitKits.Any()) 
            //            //        {
            //            //            Item _kit = sitKits.OrderByDescending(c => c.QualityLevel).FirstOrDefault();

            //            //            _kit.Use(DynelManager.LocalPlayer, true);
            //            //        }

            //            //        _sitTimeOut = 0;
            //            //    }
            //            //    else { _sitTimeOut++; }
            //            //}
            //            //await Task.Delay(900);
            //            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //            await Task.Delay(400);
            //            _initSit = false;
            //        });
            //}
        }

        private void S13BuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        if (Leader == Identity.None)
                        {
                            if (LookingSelection.Team != (LookingSelection)_settings["LookingSelection"].AsInt32())
                                Leader = DynelManager.LocalPlayer.Identity;
                        }

                        IPCChannel.Broadcast(new StartMessage());

                        _settings["Toggle"] = true;
                        Start();
                    }
                    else
                    {
                        Stop();
                        _settings["Toggle"] = false;
                        IPCChannel.Broadcast(new StopMessage());
                    }
                }
                else
                {
                    switch (param[0].ToLower())
                    {
                        case "draw":
                            SMovementController.ToggleNavMeshDraw();
                            SMovementController.TogglePathDraw();
                            break;
                        case "time":
                            if (_missionTimeGuesstimation != TimeSpan.Zero)
                            {
                                Chat.WriteLine($"{_missionTimeGuesstimation} per reform.");
                            }
                            else
                            {
                                Chat.WriteLine($"Cannot calculate yet, wait for reform.");
                            }
                            break;
                        default:
                            break;
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public enum LookingSelection
        {
            None, Member, Team
        }

        public enum ModeSelection
        {
            Patrol, Roam, Gather, Leech
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        public static void Leader_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].Leader = e;
            LeaderName = e;
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }

        #endregion
    }
}