﻿using AOSharp.Common.GameData;
using System.Collections.Generic;

namespace S13Buddy
{
    public static class Constants
    {
        public static List<string> _toolNames = new List<string>
        {
                    "Aggression Enhancer",
                    "Aggression Multiplier",
                    "Codex of the Insulting Emerto",
                    "Scorpio's Aim of Anger",
                    "Aggression Enhancer (Jealousy Augmented)",
                    "Modified Aggression Enhancer",
                    "Da Taunter!"
        };

        public static List<string> _ignores = new List<string>
        {
                    "Buckethead Technodealer",
                    "Unicorn Recon Agent Chittick"
        };

        public static List<Vector3> _mergeLeecherPath = new List<Vector3>()
        {
            new Vector3(67.9f, 67.7f, 67.3f),
            new Vector3(99.8f, 67.6f, 156.1f),
            new Vector3(220.2f, 67.6f, 183.4f),
            new Vector3(222.9f, 67.6f, 251.9f),
            new Vector3(111.7f, 67.6f, 247.2f),
            new Vector3(60.2f, 67.6f, 308.6f),
            new Vector3(69.0f, 67.6f, 405.2f),
            new Vector3(81.0f, 67.6f, 472.1f),
            new Vector3(119.3f, 67.6f, 497.0f),
            new Vector3(154.1f, 67.6f, 491.0f)
        };

        public static readonly int[] Kits =
        {
            297274, 293296, 291084, 291083, 291082
        };

        public const int S13Id = 4365;
        public const int APFHubId = 4368;
        public const int UnicornHubId = 4364;
        public const int XanHubId = 6013;

        public static Vector3 LookingForTeamPosition = new Vector3(164.7f, 1.8f, 204.9f);

        public static Vector3 XanHubPosition = new Vector3(585.4f, 0.0f, 740.2);

        public static Vector3 S13StartZoneOutPosition = new Vector3(170.1f, 36.4f, 50.4f);
        public static Vector3 S13EntrancePosition = new Vector3(145.7f, 0.5f, 206.5f);
        public static Vector3 S13ZoneOutPosition = new Vector3(149.5f, 0.5f, 206.4f);
        public static Vector3 S13StartPosition = new Vector3(150.7f, 36.4f, 42.0f);
        public static Vector3 S13GoalPosition = new Vector3(135.1f, 6.2f, 495.6f); // 135.1f, 6.2f, 495.6f  || 170.3f, 6.2f, 486.1f
    }
}
