﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using static AttackBuddy.AttackBuddy;

namespace AttackBuddy
{
    public static class Extensions
    {
        public static bool ChecksMob(SimpleChar mob)
        {
            if ((mob.Name == "Alien Coccoon" && (mob.Level == 250 || mob.Level == 234)) || mob.Side == DynelManager.LocalPlayer.Side) { return false; }

            if (mob.Name == "Alien Coccoon" && mob.MaxHealth >= 154000) { return false; }

            if (Playfield.ModelIdentity.Instance == 6055 && mob.Name == "Masked Commando") { return false; }

            return true;
        }
        public static bool ChecksSwitch(SimpleChar mob)
        {
            if ((mob.Name == "Alien Coccoon" && (mob.Level == 250 || mob.Level == 234)) || mob.Side == DynelManager.LocalPlayer.Side) { return false; }

            if (Playfield.ModelIdentity.Instance == 6055 && mob.Name == "Masked Commando") { return false; }

            if (Playfield.ModelIdentity.Instance == 6055 && mob.Name == "Strange Xan Artifact") { return false; }

            return true;
        }
        public static void HandleAttacking(SimpleChar target)
        {
            if (target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= AttackBuddy.Config.CharSettings[Game.ClientInst].AttackRange)
            {
                if (Cocooned() 
                    || (DynelManager.LocalPlayer.FightingTarget == null
                        && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending))
                {
                    if (target.Name == "Alien Coccoon" && (Playfield.ModelIdentity.Instance == 6017 || Playfield.ModelIdentity.Instance == 382))
                    {
                        DynelManager.LocalPlayer.Attack(target, true);
                    }
                    else { DynelManager.LocalPlayer.Attack(target, false); }
                }
            }
        }

        public static bool AttackingTeam(SimpleChar mob)
        {
            if (mob?.FightingTarget == null || _helpers.Contains(mob.FightingTarget?.Name) || mob.Name.Contains("Fanatic")) { return true; }

            //if (mob?.FightingTarget?.Name == "Guardian Spirit of Purification"
            //    || mob?.FightingTarget?.Name == "Rookie Alien Hunter"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Alpha"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Delta"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Gamma"
            //    || RoamBuddy._helpers.Contains(mob.FightingTarget?.Name)) { return true; }

            if (Team.IsInTeam) { return Team.Members.Select(m => m.Name).Contains(mob.FightingTarget?.Name); }

            return mob.FightingTarget?.Name == DynelManager.LocalPlayer.Name;
        }

        public static SimpleChar TryGetLeader()
        {
            return DynelManager.Players
                .FirstOrDefault(c => c.Identity == Leader
                    && c.Identity != DynelManager.LocalPlayer.Identity
                    && c.IsValid
                    && c.Health > 0
                    && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301));
        }

        public static SimpleChar TryGetLeader(Identity leader)
        {
            if (!DynelManager.Players
                .Any(c => c.Identity == leader
                    && c.IsValid
                    && c.Health > 0
                    && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301)))
                return DynelManager.LocalPlayer;

            return DynelManager.Players
                .FirstOrDefault(c => c.Identity == leader
                    && c.IsValid
                    && c.Health > 0
                    && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301));
        }

        public static bool SelfHealthy()
        {
            return DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66;
        }

        public static List<SimpleChar> TryGetSwitchMob()
        {
            return DynelManager.NPCs
               .Where(c => c.DistanceFrom(TryGetLeader(Leader)) <= ScanRange
                   && !Constants._ignores.Contains(c.Name)
                   && c.IsValid
                   && c.Health > 0 && c.IsInLineOfSight
                   && !_charmMobs.Contains(c.Identity)
                   && !IsBoss(c)
                   && ChecksSwitch(c)
                   && AttackingTeam(c)
                   && (c.Name == "Devoted Fanatic" || c.Name == "Hallowed Acolyte" || c.Name == "Confounding Bloated Carcass"
                        || c.Name == "Hand of the Colonel" || c.Name == "Turbulent Windcaller"
                        || c.Name == "Hacker'Uri" || c.Name == "Stim Fiend" || c.Name == "Ruinous Reverend"
                        || c.Name == "Lost Thought" || c.Name == "The Sacrifice" || c.Name == "Corrupted Xan-Len"
                        || c.Name == "Blue Tower" || c.Name == "Green Tower" || c.Name == "Drone Harvester - Jaax'Sinuh"
                        || c.Name == "Engorged Sacrificial Husk"
                        || c.Name == "Fanatic" || c.Name == "Garbage Critter"
                        || c.Name == "Dasyatis" || (c.Name == "Ground Chief Aune" && c.MaxHealth == 1166667) || c.Name == "Dust Brigade Drone - Omega"
                        || c.Name == "Stasis Containment Field"
                        || c.Name == "Alien Cocoon" || (c.Name == "Alien Coccoon" && (Playfield.ModelIdentity.Instance == 6017 || Playfield.ModelIdentity.Instance == 382))))
               //All spelt Coccoon
               //Max hp 200000 - Level 250 coon room ship 
               //Max hp 147659 - Level 234 coon room ship 
               //Max hp 40000 - Level 250 s28  
               //Max hp 145 - Level 25 mitaar
               .OrderBy(c => c.HealthPercent)
               //.ThenByDescending(c => c.MaxHealth)
               .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
               //.OrderBy(c => c.HealthPercent)
               //.ThenByDescending(c => c.MaxHealth)
               //.ThenBy(c => c.Position.DistanceFrom(TryGetLeader(Leader).Position))
               .ThenByDescending(c => c.Name == "Alien Cocoon" || (c.Name == "Alien Coccoon" && (Playfield.ModelIdentity.Instance == 6017 || Playfield.ModelIdentity.Instance == 382)))
               .ThenByDescending(c => c.Name == "Devoted Fanatic" || c.Name == "Fanatic")
               .ThenByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
               .ToList();
        }

        public static List<SimpleChar> TryGetMob()
        {
            return DynelManager.Characters
                .Where(c => !c.IsPlayer && c.DistanceFrom(TryGetLeader(Leader)) <= ScanRange
                    && !Constants._ignores.Contains(c.Name)
                    && c.IsValid
                    && c.Health > 0 && c.IsInLineOfSight
                    && !_charmMobs.Contains(c.Identity)
                    && !IsBoss(c)
                    && ChecksMob(c)
                    && AttackingTeam(c)
                    && !Team.Members.Select(b => b.Name).Contains(c.Name)
                    && !c.Name.Contains("sapling")
                    && (!c.IsPet || c.Name == "Drop Trooper - Ilari'Ra"))
                .OrderBy(c => c.HealthPercent)
                //.ThenByDescending(c => c.MaxHealth)
                .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                //.OrderByDescending(c => c.MaxHealth)
                //.ThenBy(c => c.HealthPercent)
                //.ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ThenByDescending(c => c.Name == "Turbulent Windcaller")
                .ThenByDescending(c => c.Name == "Garbage Critter")
                .ThenByDescending(c => c.Name == "Strange Xan Artifact")
                .ThenByDescending(c => c.Name == "Dust Brigade Drone - Omega")
                .ThenByDescending(c => c.Name == "Dasyatis")
                .ThenByDescending(c => c.Name == "Corrupted Hiisi Berserker")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Cur")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Kuir")
                .ThenByDescending(c => c.Name == "Cultist Silencer")
                .ThenByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
                .ThenByDescending(c => c.Name == "Alien Cocoon" || (c.Name == "Alien Coccoon" && (Playfield.ModelIdentity.Instance == 6017 || Playfield.ModelIdentity.Instance == 382)))
                .ThenByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
                .ThenByDescending(c => c.Name == "Stim Fiend")
                .ThenByDescending(c => c.Name == "Lost Thought")
                .ThenByDescending(c => c.Name == "Masked Operator")
                .ThenByDescending(c => c.Name == "Masked Technician")
                .ThenByDescending(c => c.Name == "Masked Engineer")
                .ThenByDescending(c => c.Name == "Masked Superior Commando")
                .ThenByDescending(c => c.Name == "Green Tower")
                .ThenByDescending(c => c.Name == "Blue Tower")
                .ThenByDescending(c => c.Name == "The Sacrifice")
                .ThenByDescending(c => c.Name == "Hacker'Uri")
                .ThenByDescending(c => c.Name == "Hand of the Colonel")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Len")
                .ThenByDescending(c => c.Name == "Confounding Bloated Carcass")
                .ThenByDescending(c => c.Name == "Hallowed Acolyte")
                .ThenByDescending(c => c.Name == "Devoted Fanatic")
                .ToList();
        }

        public static List<SimpleChar> TryGetBoss()
        {
            return DynelManager.NPCs
                .Where(c => c.DistanceFrom(TryGetLeader(Leader)) <= ScanRange
                    && !Constants._ignores.Contains(c.Name)
                    && c.IsValid
                    && c.Health > 0 && c.IsInLineOfSight
                    && CanAttack(c)
                    && IsBoss(c))
                .OrderBy(c => c.Position.DistanceFrom(TryGetLeader(Leader).Position))
                .ThenByDescending(c => c.Name == "Khalum the Weaver of Flesh")
                .ThenByDescending(c => c.Name == "Uklesh the Beguiling")
                .ToList();
        }

        public static bool PetInvalid()
        {
            return DynelManager.Characters.Any(c => c.IsPet
                        && ((c.Nano / PetMaxNanoPool(c) * 100 <= 55 && c.Nano != 10)
                            || c.Buffs.Contains(NanoLine.Root) == true
                            || c.Buffs.Contains(NanoLine.AOERoot) == true
                            || c.Buffs.Contains(NanoLine.Snare) == true
                            || c.Buffs.Contains(NanoLine.AOESnare) == true
                            || c.Buffs.Contains(NanoLine.Mezz) == true
                            || c.Buffs.Contains(NanoLine.AOEMezz) == true)
                        && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) < 10f && c.IsInLineOfSight == true);
        }

        public static bool TeamInvalid()
        {
            return Team.Members.Any(c => c.Character != null
                        && (c.Character.NanoPercent < 66 || c.Character.HealthPercent < 66
                            || c.Character.Buffs.Contains(NanoLine.Root) == true || c.Character.Buffs.Contains(NanoLine.AOERoot)));
        }

        public static bool IsNull(SimpleChar _target)
        {
            return _target == null
                || _target?.Health == 0
                || _target?.IsValid == false
                || _target?.IsInLineOfSight == false
                || _target.Buffs.Contains(273220) || _target.Buffs.Contains(253953) || _target.Buffs.Contains(302745)
                || (_target.Buffs.Contains(205607) && _target.Level != 73)
                || (DynelManager.LocalPlayer.FightingTarget != null && DynelManager.LocalPlayer.FightingTarget.Name != _target.Name)
                || ((DynelManager.LocalPlayer.Buffs.Contains(274117) || DynelManager.LocalPlayer.Buffs.Contains(274101)) && _target.Name.Contains("Aune"))
                //Informant
                || (_target.Name.Contains("Aune") && Playfield.ModelIdentity.Instance == 615 && _target.HealthPercent <= 35);
        }

        public static bool IsBoss(SimpleChar _boss)
        {
            return _boss?.MaxHealth >= 999999 || _boss?.Name == "Kyr'Ozch Maid" || _boss?.Name == "Kyr'Ozch Technician"
                        || _boss?.Name == "Masked Commando Superior"
                        || _boss?.Name == "Defense Drone Tower"
                        || _boss?.Name == "Rashnu" || _boss?.Name == "Sraosha" || _boss?.Name == "Vahman" || _boss?.Name == "Ormazd"
                        || _boss?.Name == "Desert Commander" || _boss?.Name == "Desert Warden" 
                        || _boss?.Name == "Prototype Infiltration Unit";
        }

        public static float PetMaxNanoPool(SimpleChar _healPet)
        {
            if (_healPet.Level == 215)
                return 5803;
            else if (_healPet.Level == 192)
                return 13310;
            else if (_healPet.Level == 169)
                return 11231;
            else if (_healPet.Level == 146)
                return 9153;
            else if (_healPet.Level == 123)
                return 7169;
            else if (_healPet.Level == 99)
                return 5327;
            else if (_healPet.Level == 77)
                return 3807;
            else if (_healPet.Level == 55)
                return 2404;
            else if (_healPet.Level == 33)
                return 1234;
            else if (_healPet.Level == 14)
                return 414;

            return 0;
        }

        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending
                || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && (Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name)
                            || AttackBuddy._helpers.Contains(c.FightingTarget?.Name)));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }

        public static bool ShouldStopAttack()
        {
            if (DynelManager.LocalPlayer.FightingTarget == null
                || (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(RelevantNanos.ShovelBuffs) == true
                    && DynelManager.LocalPlayer.FightingTarget?.Name.Contains("Aune") == true)) { return false; }

            if (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(253953) == true
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(273220) == true
                || (DynelManager.LocalPlayer.Buffs.Contains(274101) && DynelManager.LocalPlayer.FightingTarget?.Name.Contains("Aune") == true)
                || (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(RelevantNanos.ShovelBuffs) == true
                    && DynelManager.LocalPlayer.FightingTarget?.Name != "Sraosha")
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(302745) == true
                || (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(205607) == true && DynelManager.LocalPlayer.FightingTarget?.Level != 73)
                || DynelManager.LocalPlayer.FightingTarget?.IsPlayer == true) { return true; }

            return false;
        }
        public static bool BrokenCharmMobAttacking()
        {
            return _brokenCharmMob?.FightingTarget != null && (bool)_brokenCharmMob?.IsAttacking
                    && (Team.Members.Select(c => c.Identity).Any(c => (Identity)_brokenCharmMob?.FightingTarget.Identity == c)
                        || (bool)_brokenCharmMob.FightingTarget?.IsPet);
        }

        public static bool FindTool(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants._toolNames.Contains(c.Name))) != null;
        }

        public static bool ShouldTaunt(SimpleChar _target)
        {
            return _target?.IsInLineOfSight == true
                && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending;
        }

        public static bool CanAttack()
        {
            return DynelManager.LocalPlayer.FightingTarget == null
                && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending;
        }
        public static bool Cocooned()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(257127);
        }

        public static bool ShouldAttack()
        {
            return !DynelManager.LocalPlayer.Buffs.Contains(280470) && !DynelManager.LocalPlayer.Buffs.Contains(274101);
        }  

        public static bool CanAttack(SimpleChar target)
        {
            if (target.Name.Contains("Aune"))
            {
                if (Playfield.ModelIdentity.Instance == 615 && target.HealthPercent <= 35) { return false; }

                if (Playfield.ModelIdentity.Instance == 560 || DynelManager.LocalPlayer.Buffs.Contains(274117) || DynelManager.LocalPlayer.Buffs.Contains(274101)) { return false; }
            }

            if (target.Name.Contains("Phobettor") || target.Name.Contains("Azi Dahaka")) { return true; }

            if (!target.Buffs.Contains(RelevantNanos.ShovelBuffs) && !target.Buffs.Contains(302745) 
                && !target.Buffs.Contains(253953) && !target.Buffs.Contains(205607) && !target.Buffs.Contains(273220)) { return true; }

            return false;
        }
    }
}
