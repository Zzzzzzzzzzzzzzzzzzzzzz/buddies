﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using AttackBuddy.IPCMessages;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;
using AOSharp.Common.GameData.UI;
using System.Globalization;

namespace AttackBuddy
{
    public class AttackBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static NavMeshMovementController NavMeshMovementController { get; private set; }

        public static int AttackRange;
        public static int ScanRange;

        public static bool Toggle = false;
        public static bool _init = false;

        public static bool _subbedUIEvents = false;

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public static SimpleChar _charmMob;
        public static SimpleChar _brokenCharmMob;

        public static float Tick = 0;

        public static double _stateTimeOut = Time.NormalTime;

        public static SimpleChar _mob;
        public static SimpleChar _bossMob;
        public static SimpleChar _switchMob;

        public static List<SimpleChar> _switchMobPrecision = new List<SimpleChar>();
        public static List<SimpleChar> _switchMobCharging = new List<SimpleChar>();
        public static List<SimpleChar> _switchMobShield = new List<SimpleChar>();

        public static List<Identity> _charmMobs = new List<Identity>();

        public static Pet _pet;
        public static Pet _hinderedPet;

        private static double _mainUpdate;
        private static double _stopAttackTimer;

        private static Window _infoWindow;
        private static Window _helperWindow;

        private static View _helperView;

        public static string PluginDir;

        public static Settings _settings;

        public static List<string> _helpers = new List<string>();

        public override void Run(string pluginDir)
        {
            try
            {
                #region Settings

                _settings = new Settings("AttackBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy\\{Game.ClientInst}\\Config.json");

                Chat.RegisterCommand("buddy", AttackBuddyCommand);

                SettingsController.RegisterSettingsWindow("AttackBuddy", pluginDir + "\\UI\\AttackBuddySettingWindow.xml", _settings);

                _settings.AddVariable("Toggle", false);
                _settings.AddVariable("Taunting", false);
                _settings.AddVariable("Waiting", false);

                _settings["Toggle"] = false;

                #endregion

                #region IPC

                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.AttackRange, OnAttackRangeMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.ScanRange, OnScanRangeMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.GauntIgnores, OnGauntIgnoresMessage);

                #endregion

                Chat.WriteLine("AttackBuddy Loaded! Version: 0.9.9.94");
                Chat.WriteLine("/attackbuddy for settings.");

                #region Events

                Game.OnUpdate += OnUpdate;

                #endregion

                #region Init

                _stateMachine = new StateMachine(new IdleState());

                AttackRange = Config.CharSettings[Game.ClientInst].AttackRange;
                ScanRange = Config.CharSettings[Game.ClientInst].ScanRange;
                Tick = Config.CharSettings[Game.ClientInst].Tick;

                #endregion
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        #region Events

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        public static void AttackRange_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].AttackRange = e;
            AttackRange = e;
            Config.Save();
        }
        public static void ScanRange_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].ScanRange = e;
            ScanRange = e;
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }

        #endregion

        #region Callbacks

        private void OnAttackRangeMessage(int sender, IPCMessage msg)
        {
            AttackRangeMessage rangeMsg = (AttackRangeMessage)msg;

            Config.CharSettings[Game.ClientInst].AttackRange = rangeMsg.Range;

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid
                && SettingsController.settingsWindow.FindView("AttackRangeBox", out TextInputView attackRangeInput))
                attackRangeInput.Text = $"{rangeMsg.Range}";
            else
            {
                AttackRange = rangeMsg.Range;
                Config.Save();
            }
        }

        private void OnScanRangeMessage(int sender, IPCMessage msg)
        {
            ScanRangeMessage rangeMsg = (ScanRangeMessage)msg;

            Config.CharSettings[Game.ClientInst].ScanRange = rangeMsg.Range;

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid
                && SettingsController.settingsWindow.FindView("ScanRangeBox", out TextInputView scanRangeInput))
                scanRangeInput.Text = $"{rangeMsg.Range}";
            else
            {
                ScanRange = rangeMsg.Range;
                Config.Save();
            }
        }

        private void OnGauntIgnoresMessage(int sender, IPCMessage msg)
        {
            Constants._ignores.Add("Sraosha");
            Constants._ignores.Add("Rashnu");
            Constants._ignores.Add("Vahman");
            Constants._ignores.Add("Ormazd");

            Chat.WriteLine($"Added Gaunt bosses to ignore list.");
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            Chat.WriteLine("Buddy enabled.");
            _settings["Toggle"] = true;

            Toggle = true;

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            Toggle = false;

            _settings["Toggle"] = false;
            Chat.WriteLine("Buddy disabled.");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        #endregion

        #region Handles

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\AttackBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void HandleHelpersViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_helperView)) { return; }

                _helperView = View.CreateFromXml(PluginDir + "\\UI\\AttackBuddyHelpersView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Helpers", XmlViewName = "AttackBuddyHelpersView" }, _helperView);
            }
            else if (_helperWindow == null || (_helperWindow != null && !_helperWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_helperWindow, PluginDir, new WindowOptions() { Name = "Helpers", XmlViewName = "AttackBuddyHelpersView", WindowSize = new Rect(0, 0, 130, 345) }, _helperView, out var container);
                _helperWindow = container;
            }
        }

        private void HandleRelayClick(object s, ButtonBase button)
        {
            IPCChannel.Broadcast(new AttackRangeMessage()
            {
                Range = Config.CharSettings[Game.ClientInst].AttackRange
            });
            IPCChannel.Broadcast(new ScanRangeMessage()
            {
                Range = Config.CharSettings[Game.ClientInst].ScanRange
            });
        }

        private void HandleAddHelperViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _helpers.Add(Targeting.TargetChar?.Name);
        }

        private void HandleRemoveHelperViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _helpers.Remove(Targeting.TargetChar?.Name);
        }

        private void HandleClearHelpersViewClick(object s, ButtonBase button)
        {
            _helpers.Clear();
        }

        private void HandlePrintHelpersViewClick(object s, ButtonBase button)
        {
            foreach (string str in _helpers)
                Chat.WriteLine(str);
        }

        #endregion

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning) { return; }

            #region Stop Attack

            if (Time.NormalTime > _stopAttackTimer + 2f)
            {
                if (_settings["Toggle"].AsBool() && Extensions.ShouldStopAttack())
                {
                    DynelManager.LocalPlayer.StopAttack();
                    Chat.WriteLine($"Stopping attack.");
                }

                _stopAttackTimer = Time.NormalTime;
            }

            #endregion

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                _stateMachine.Tick();
                _mainUpdate = Time.NormalTime;
            }

            #region UI Update

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                if (window.FindView("AttackBuddyAddHelper", out Button addHelperView))
                {
                    addHelperView.Tag = window;
                    addHelperView.Clicked = HandleAddHelperViewClick;
                }

                if (window.FindView("AttackBuddyRemoveHelper", out Button removeHelperView))
                {
                    removeHelperView.Tag = window;
                    removeHelperView.Clicked = HandleRemoveHelperViewClick;
                }

                if (window.FindView("AttackBuddyClearHelpers", out Button clearHelpersView))
                {
                    clearHelpersView.Tag = window;
                    clearHelpersView.Clicked = HandleClearHelpersViewClick;
                }

                if (window.FindView("AttackBuddyPrintHelpers", out Button printHelpersView))
                {
                    printHelpersView.Tag = window;
                    printHelpersView.Clicked = HandlePrintHelpersViewClick;
                }
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                SettingsController.settingsWindow.FindView("AttackRangeBox", out TextInputView attackRangeInput);
                SettingsController.settingsWindow.FindView("ScanRangeBox", out TextInputView scanRangeInput);
                SettingsController.settingsWindow.FindView("TickBox", out TextInputView tickInput);

                if (SettingsController.settingsWindow.FindView("AttackBuddyRelay", out Button relay))
                {
                    relay.Tag = window;
                    relay.Clicked = HandleRelayClick;
                }

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text)
                    && int.TryParse(channelInput.Text, out int channelValue)
                    && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text)
                    && float.TryParse(tickInput.Text, out float tickValue)
                    && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                    Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (attackRangeInput != null && !string.IsNullOrEmpty(attackRangeInput.Text)
                    && int.TryParse(attackRangeInput.Text, out int attackRangeInputValue)
                    && Config.CharSettings[Game.ClientInst].AttackRange != attackRangeInputValue)
                    Config.CharSettings[Game.ClientInst].AttackRange = attackRangeInputValue;
                if (scanRangeInput != null && !string.IsNullOrEmpty(scanRangeInput.Text)
                    && int.TryParse(scanRangeInput.Text, out int scanRangeInputValue)
                    && Config.CharSettings[Game.ClientInst].ScanRange != scanRangeInputValue)
                    Config.CharSettings[Game.ClientInst].ScanRange = scanRangeInputValue;

                if (SettingsController.settingsWindow.FindView("AttackBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (SettingsController.settingsWindow.FindView("AttackBuddyHelpersView", out Button helperView))
                {
                    helperView.Tag = SettingsController.settingsWindow;
                    helperView.Clicked = HandleHelpersViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (Leader == Identity.None)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;
                    }

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StartMessage());

                    Start();
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Stop();

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StopMessage());
                }
            }

            #endregion
        }

        #region Misc

        public Window[] _windows => new Window[] { _helperWindow };

        private void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;

            Chat.WriteLine("Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }


        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].AttackRangeChangedEvent -= AttackRange_Changed;
                Config.CharSettings[Game.ClientInst].ScanRangeChangedEvent -= ScanRange_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].AttackRangeChangedEvent += AttackRange_Changed;
                Config.CharSettings[Game.ClientInst].ScanRangeChangedEvent += ScanRange_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        public static class RelevantNanos
        {
            public const int ReflectionsOfAngerLong = 290404;
            public static readonly int[] ShovelBuffs = Spell.GetSpellsForNanoline(NanoLine.ShovelBuffs).Where(c => c.Id != ReflectionsOfAngerLong).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
        }

        private void AttackBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        if (Leader == Identity.None)
                        {
                            IsLeader = true;
                            Leader = DynelManager.LocalPlayer.Identity;
                        }

                        if (DynelManager.LocalPlayer.Identity == Leader)
                            IPCChannel.Broadcast(new StartMessage());

                        _settings["Toggle"] = true;
                        Start();
                    }
                    else
                    {
                        Stop();
                        _settings["Toggle"] = false;
                        IPCChannel.Broadcast(new StopMessage());
                    }
                }
                else if (param.Length >= 1)
                {
                    switch (param[0].ToLower())
                    {
                        case "helpers":
                            if (param[1].ToLower() == "add")
                            {
                                if (param.Length > 2)
                                {
                                    string name = string.Join(" ", param.Skip(1));

                                    if (!_helpers.Contains(name))
                                    {
                                        _helpers.Add(name);
                                        chatWindow.WriteLine($"Added {name} to Helpers list");
                                    }
                                    else if (_helpers.Contains(name))
                                    {
                                        _helpers.Remove(name);
                                        chatWindow.WriteLine($"Removed {name} from Helpers list");
                                    }
                                }
                                else
                                {
                                    if (Targeting.TargetChar != null)
                                    {
                                        if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                        {
                                            _helpers.Add(Targeting.TargetChar?.Name);
                                            chatWindow.WriteLine($"Added {Targeting.TargetChar?.Name} to Helpers list");
                                        }
                                    }
                                    else
                                        chatWindow.WriteLine($"Must have a target.");
                                }
                            }
                            else if (param[1].ToLower() == "remove")
                            {
                                if (param.Length > 2)
                                {
                                    string name = string.Join(" ", param.Skip(1));

                                    if (!_helpers.Contains(name))
                                    {
                                        _helpers.Add(name);
                                        chatWindow.WriteLine($"Added {name} to Helpers list");
                                    }
                                    else if (_helpers.Contains(name))
                                    {
                                        _helpers.Remove(name);
                                        chatWindow.WriteLine($"Removed {name} from Helpers list");
                                    }
                                }
                                else
                                {
                                    if (Targeting.TargetChar != null)
                                    {
                                        if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                        {
                                            _helpers.Add(Targeting.TargetChar?.Name);
                                            chatWindow.WriteLine($"Added {Targeting.TargetChar?.Name} to Helpers list");
                                        }
                                    }
                                    else
                                        chatWindow.WriteLine($"Must have a target.");
                                }
                            }
                            break;
                        case "ignore":
                            if (param.Length > 1)
                            {
                                string name = string.Join(" ", param.Skip(1));

                                if (!Constants._ignores.Contains(name))
                                {
                                    Constants._ignores.Add(name);
                                    Chat.WriteLine($"Added \"{name}\" to ignored mob list");
                                }
                                else if (Constants._ignores.Contains(name))
                                {
                                    Constants._ignores.Remove(name);
                                    Chat.WriteLine($"Removed \"{name}\" from ignored mob list");
                                }
                            }
                            else
                            {
                                if (Targeting.TargetChar != null)
                                {
                                    if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                    {
                                        Constants._ignores.Add(Targeting.TargetChar?.Name);
                                        Chat.WriteLine($"Added {Targeting.TargetChar?.Name} to ignored mob list");
                                    }
                                }
                                else
                                    Chat.WriteLine($"Must have a target.");
                            }
                            break;
                        case "gauntignores":
                            Constants._ignores.Add("Sraosha");
                            Constants._ignores.Add("Rashnu");
                            Constants._ignores.Add("Vahman");
                            Constants._ignores.Add("Ormazd");

                            Chat.WriteLine($"Added Gaunt bosses to ignore list.");

                            IPCChannel.Broadcast(new GauntUnIgnoresMessage());
                            break;
                        default:
                            return;
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        #endregion
    }
}
