﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace AttackBuddy
{
    public class ScanState : IState
    {
        private SimpleChar _target;

        private static bool _charmMobAttacked = false;

        private static double _charmMobAttacking;

        public IState GetNextState()
        {
            if (_target != null || !AttackBuddy._settings["Toggle"].AsBool())
                return new FightState(_target);

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ScanState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ScanState::OnStateExit");
        }

        private void HandleCharmScan()
        {
            AttackBuddy._charmMob = DynelManager.Characters
                .FirstOrDefault(c => !AttackBuddy._charmMobs.Contains(c.Identity) && (c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)));

            AttackBuddy._brokenCharmMob = DynelManager.Characters
                .FirstOrDefault(c => AttackBuddy._charmMobs.Contains(c.Identity) && !c.Buffs.Contains(NanoLine.CharmOther) && !c.Buffs.Contains(NanoLine.Charm_Short));

            if (AttackBuddy._charmMob != null && !AttackBuddy._charmMobs.Contains(AttackBuddy._charmMob.Identity))
                AttackBuddy._charmMobs.Add((Identity)AttackBuddy._charmMob?.Identity);

            if (_charmMobAttacked == true)
            {
                if (Time.NormalTime > _charmMobAttacking + 8f)
                {
                    if (Extensions.BrokenCharmMobAttacking())
                    {
                        _charmMobAttacked = false;

                        if (AttackBuddy._charmMobs.Contains((Identity)AttackBuddy._brokenCharmMob?.Identity))
                            AttackBuddy._charmMobs.Remove((Identity)AttackBuddy._brokenCharmMob?.Identity);
                    }
                    else { _charmMobAttacked = false; }
                }
            }
            else if (AttackBuddy._brokenCharmMob != null)
            {
                _charmMobAttacking = Time.NormalTime;
                _charmMobAttacked = true;
            }
        }

        public void Tick()
        {
            if (Game.IsZoning || _target != null) { return; }

            if (!Extensions.InCombat())
            {
                if (AttackBuddy._settings["Waiting"].AsBool() && (!Extensions.SelfHealthy() || Extensions.TeamInvalid() || Extensions.PetInvalid())) { return; }

                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit) { return; }
            }

            HandleCharmScan();

            if (!Extensions.ShouldAttack()) { return; }

            if (Extensions.TryGetSwitchMob().Any())
            {
                _target = Extensions.TryGetSwitchMob().FirstOrDefault();
                Chat.WriteLine($"Found target: {_target.Name}.");
                return;
            }
            else if (Extensions.TryGetMob().Any())
            {
                _target = Extensions.TryGetMob().FirstOrDefault();
                Chat.WriteLine($"Found target: {_target.Name}.");
                return;
            }
            else if (Extensions.TryGetBoss().Any() && Extensions.ShouldAttack())
            {
                _target = Extensions.TryGetBoss().FirstOrDefault();
                Chat.WriteLine($"Found target: {_target.Name}.");
                return;
            }
        }
    }
}
