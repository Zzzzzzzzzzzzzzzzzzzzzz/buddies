﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AttackBuddy
{
    public class FightState : IState
    {
        public const double _fightTimeout = 45f;

        private double _fightStartTime;
        public static float _tetherDistance;

        public static int _aggToolCounter = 0;

        private SimpleChar _target;

        public FightState(SimpleChar target)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Extensions.IsNull(_target)
                || (Time.NormalTime > _fightStartTime + _fightTimeout && _target?.MaxHealth <= 999999))
            {
                _target = null;

                return new ScanState();
            }

            if (!AttackBuddy._settings["Toggle"].AsBool())
            {
                _target = null;

                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");

            _fightStartTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }

        public void Tick()
        {
            if (Game.IsZoning || _target == null || !Extensions.ShouldAttack()) { return; }

            if (Extensions.CanAttack())
            {
                Extensions.HandleAttacking(_target);
            }
            else if (Extensions.Cocooned() || DynelManager.LocalPlayer.IsAttacking)
            {
                if (Extensions.IsBoss(_target))
                {
                    if (Extensions.TryGetSwitchMob().Any())
                    {
                        _target = Extensions.TryGetSwitchMob().FirstOrDefault();
                        Chat.WriteLine($"Switching to target: {_target.Name}.");
                        return;
                    }
                    else if (Extensions.TryGetMob().Any())
                    {
                        _target = Extensions.TryGetMob().FirstOrDefault();
                        Chat.WriteLine($"Switching to target: {_target.Name}.");
                        return;
                    }

                    #region Sector 7

                    ////if (AttackBuddy._switchMobPrecision.Count >= 1)
                    ////{
                    ////    if (_target != AttackBuddy._switchMobPrecision.FirstOrDefault()
                    ////             && _target != AttackBuddy._switchMobCharging.FirstOrDefault()
                    ////             && _target != AttackBuddy._switchMobShield.FirstOrDefault())
                    ////    {
                    ////        _target = AttackBuddy._switchMobPrecision.FirstOrDefault();
                    ////        DynelManager.LocalPlayer.Attack(_target);
                    ////        Chat.WriteLine($"Switching to target {_target.Name}.");
                    ////        _fightStartTime = Time.NormalTime;
                    ////    }
                    ////}
                    ////else if (AttackBuddy._switchMobCharging.Count >= 1)
                    ////{
                    ////    if (_target != AttackBuddy._switchMobPrecision.FirstOrDefault()
                    ////            && _target != AttackBuddy._switchMobCharging.FirstOrDefault()
                    ////            && _target != AttackBuddy._switchMobShield.FirstOrDefault())
                    ////    {
                    ////        _target = AttackBuddy._switchMobCharging.FirstOrDefault();
                    ////        DynelManager.LocalPlayer.Attack(_target);
                    ////        Chat.WriteLine($"Switching to target {_target.Name}.");
                    ////        _fightStartTime = Time.NormalTime;
                    ////    }
                    ////}
                    ////else if (AttackBuddy._switchMobShield.Count >= 1)
                    ////{
                    ////    if (_target != AttackBuddy._switchMobPrecision.FirstOrDefault()
                    ////               && _target != AttackBuddy._switchMobCharging.FirstOrDefault()
                    ////               && _target != AttackBuddy._switchMobShield.FirstOrDefault())
                    ////    {
                    ////        _target = AttackBuddy._switchMobShield.FirstOrDefault();
                    ////        DynelManager.LocalPlayer.Attack(_target);
                    ////        Chat.WriteLine($"Switching to target {_target.Name}.");
                    ////        _fightStartTime = Time.NormalTime;
                    ////    }
                    ////}

                    #endregion
                }
                else if (Extensions.TryGetSwitchMob().Any() && !Extensions.TryGetSwitchMob().Select(c => c.Name).Contains(_target.Name))
                {
                    _target = Extensions.TryGetSwitchMob().FirstOrDefault();
                    Chat.WriteLine($"Switching to target: {_target.Name}.");
                    return;
                }
            }

            if (Extensions.ShouldTaunt(_target) && AttackBuddy._settings["Taunting"].AsBool() && DynelManager.LocalPlayer.Identity == AttackBuddy.Leader
                && _target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > AttackBuddy.Config.CharSettings[Game.ClientInst].AttackRange
                && Extensions.FindTool(out Item _tool) && !Item.HasPendingUse && !PerkAction.List.Any(c => c.IsExecuting || c.IsPending)
                && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
            {
                _tool?.Use(_target, true);
            }
        }
    }
}
