﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using MitaarBuddy.IPCMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MitaarBuddy
{
    public class MitaarBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;

        public static List<SimpleChar> _mob = new List<SimpleChar>();
        public static List<SimpleChar> _bossMob = new List<SimpleChar>();
        public static List<SimpleChar> _switchMob = new List<SimpleChar>();

        public static bool Toggle = false;
        public static bool Sitting = false;
        public static bool _initSit = false;

        private static double _refreshList;
        public static double _sitUpdateTimer;

        public static Window _infoWindow;

        public static Settings _settings;

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("MitaarBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\MitaarBuddy\\{Game.ClientInst}\\Config.json");
                NavMeshMovementController = new NavMeshMovementController($"{pluginDir}\\NavMeshes", true);
                MovementController.Set(NavMeshMovementController);
                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Proceed, OnProceedMessage);

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;

                _settings.AddVariable("ModeSelection", (int)ModeSelection.Single);

                Chat.RegisterCommand("buddy", MitaarBuddyCommand);

                SettingsController.RegisterSettingsWindow("MitaarBuddy", pluginDir + "\\UI\\MitaarBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                Game.OnUpdate += OnUpdate;

                _settings.AddVariable("Toggle", false);

                _settings["Toggle"] = false;

                Chat.WriteLine("MitaarBuddy Loaded! Version: 0.9.9.81");
                Chat.WriteLine("/mitaarbuddy for settings.");
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }
        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        public static void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;

            Chat.WriteLine("Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());

            NavMeshMovementController.Halt();
        }

        private void OnProceedMessage(int sender, IPCMessage msg)
        {
            ReformState._proceed = true;
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None
                && DynelManager.LocalPlayer.Identity.Instance != sender)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            _settings["Toggle"] = true;
            Start();
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            _settings["Toggle"] = false;
            Stop();
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\MitaarBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void Scanning()
        {
            _bossMob = DynelManager.NPCs
                .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) <= 30f
                    && !Constants._ignores.Contains(c.Name)
                    && c.Health > 0 && c.IsInLineOfSight
                    && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
                    && !c.Buffs.Contains(302745)
                    && c.MaxHealth >= 1000000)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) <= 30f
                   && !Constants._ignores.Contains(c.Name)
                   && c.Health > 0 && c.IsInLineOfSight && c.MaxHealth < 1000000
                   && (c.Name == "Alien Coccoon"
                        || c.Name == "Alien Cocoon"))
               .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
               .OrderBy(c => c.HealthPercent)
               .ToList();

            _mob = DynelManager.Characters
                .Where(c => !c.IsPlayer && c.DistanceFrom(DynelManager.LocalPlayer) <= 30f
                    && !Constants._ignores.Contains(c.Name)
                    && c.Health > 0 && c.IsInLineOfSight && c.MaxHealth < 1000000
                    && (c.Name == "Alien Coccoon"
                        || c.Name == "Alien Cocoon"))
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .OrderBy(c => c.HealthPercent)
                .ToList();

            _refreshList = Time.NormalTime;
        }


        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            _stateMachine.Tick();

            if (Time.NormalTime > _refreshList + 0.5f
                && Toggle == true)
                Scanning();

            if (Time.NormalTime > _sitUpdateTimer + 1 && Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                ListenerSit();

                _sitUpdateTimer = Time.NormalTime;
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

                if (channelInput != null)
                {
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                    }
                }

                if (SettingsController.settingsWindow.FindView("MitaarBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    Leader = DynelManager.LocalPlayer.Identity;

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StartMessage());

                    Start();
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Stop();

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StopMessage());
                }
            }
        }

        private void ListenerSit()
        {
            if (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66
                && (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment) || _initSit))
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                    MovementController.Instance.SetMovement(MovementAction.LeaveSit);

                if (_initSit)
                    _initSit = false;
            }

            if (Extensions.CanUseSitKit() && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            {
                if (!_initSit && DynelManager.LocalPlayer.MovementState != MovementState.Sit)
                {
                    _initSit = true;
                    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                }
            }
        }

        private void MitaarBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        Leader = DynelManager.LocalPlayer.Identity;

                        IPCChannel.Broadcast(new StartMessage());
                        _settings["Toggle"] = true;
                        Start();

                    }
                    else
                    {
                        Stop();
                        _settings["Toggle"] = false;
                        IPCChannel.Broadcast(new StopMessage());
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public static class RelevantItems
        {
            public static readonly int[] Kits = {
                297274, 293296, 291084, 291083, 291082
            };
        }
        public enum ModeSelection
        {
            Single, Loop
        }
    }
}