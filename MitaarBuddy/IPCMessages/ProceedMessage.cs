﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace MitaarBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.Proceed)]
    public class ProceedMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Proceed;
    }
}
