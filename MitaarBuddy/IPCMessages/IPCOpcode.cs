﻿namespace MitaarBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 1011,
        Stop = 1012,
        Proceed = 1013
    }
}
