﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MitaarBuddy
{
    public class FightState : IState
    {
        private SimpleChar _target;

        public FightState(SimpleChar target)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Extensions.IsNull(_target))
            {
                _target = null;
                return new ScanState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }

        public void Tick()
        {
            if (_target == null)
                return;

            if (Extensions.CanAttack())
            {
                if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 30f)
                {
                    DynelManager.LocalPlayer.Attack(_target, true);
                    Chat.WriteLine($"Attacking {_target.Name}.");
                }
            }
            if (MitaarBuddy._mob.Count >= 1
                    && _target.Name != MitaarBuddy._mob.FirstOrDefault().Name
                    && DynelManager.LocalPlayer.FightingTarget != null)
            {
                _target = MitaarBuddy._mob.FirstOrDefault();
                DynelManager.LocalPlayer.Attack(_target, true);
                Chat.WriteLine($"Switching to target {_target.Name}.");
            }
        }
    }
}
