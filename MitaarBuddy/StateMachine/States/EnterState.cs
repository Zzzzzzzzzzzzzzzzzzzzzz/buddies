﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;

namespace MitaarBuddy
{
    public class EnterState : IState
    {
        private CancellationTokenSource _cancellationToken1 = new CancellationTokenSource();
        private CancellationTokenSource _cancellationToken2 = new CancellationTokenSource();

        private static bool _init = false;
        private static bool _proceed = false;
        private static bool _exit = false;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.MitaarId
                && _init && _proceed && _exit
                && !Team.Members.Any(c => c.Character == null))
                return new ScanState();

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("EnterState::OnStateEnter");
            Task.Factory.StartNew(
                async () =>
                {
                    await Task.Delay(5000);
                    MitaarBuddy.NavMeshMovementController.SetDestination(new Vector3(347.5f, 310.9f, 407.8f));
                }, _cancellationToken1.Token);
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("EnterState::OnStateExit");

            _cancellationToken1.Cancel();
            _cancellationToken2.Cancel();

            _init = false;
            _proceed = false;
            _exit = false;
        }

        public void Tick()
        {
            if (Playfield.ModelIdentity.Instance != Constants.MitaarId)
                return;

            if (!_init)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(97.5f, 11.7f, 103.3f)) > 2f)
                {
                    MitaarBuddy.NavMeshMovementController.SetDestination(new Vector3(97.5f, 11.7f, 103.3f));
                }

                if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(97.5f, 11.7f, 103.3f)) <= 2f)
                {
                    _init = true;
                    MitaarBuddy.NavMeshMovementController.Halt();
                }
            }

            if (_init
                && DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(97.5f, 11.7f, 103.3f)) <= 2f
                && !_proceed)
            {
                _proceed = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(10000);
                        List<Identity> _openBags = Inventory.Backpacks
                            .Where(c => !c.IsOpen)
                            .Select(c => c.Identity)
                            .ToList();
                        await Task.Delay(1500);
                        List<Item> bags = Inventory.Items
                            .Where(c => c.UniqueIdentity.Type == IdentityType.Container
                                    && _openBags.Contains(c.UniqueIdentity))
                            .ToList();
                        await Task.Delay(1500);
                        foreach (Item bag in bags)
                        {
                            bag.Use();
                            bag.Use();
                        }
                        await Task.Delay(7000);
                        if (DynelManager.LocalPlayer.Profession == Profession.Doctor
                            || DynelManager.LocalPlayer.Profession == Profession.Bureaucrat
                            || DynelManager.LocalPlayer.Profession == Profession.NanoTechnician
                            || DynelManager.LocalPlayer.Profession == Profession.Adventurer
                            || DynelManager.LocalPlayer.Profession == Profession.MartialArtist)
                        {
                            DynelManager.LocalPlayer.Position = Constants._bluePodium;
                            MitaarBuddy.NavMeshMovementController.SetMovement(MovementAction.Update);
                        }
                        else
                        {
                            DynelManager.LocalPlayer.Position = Constants._redPodium;
                            MitaarBuddy.NavMeshMovementController.SetMovement(MovementAction.Update);
                        }
                        await Task.Delay(2000);
                        //if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
                        //{
                        //    if (Spell.Find("Improved Element of Malice", out Spell malice))
                        //    {
                        //        malice.Cast(DynelManager.NPCs.FirstOrDefault(c => (bool)c?.Name.Contains("Sinuh")), true);
                        //        await Task.Delay(5500);
                        //        malice.Cast(DynelManager.NPCs.FirstOrDefault(c => (bool)c?.Name.Contains("Sinuh")), true);
                        //    }
                        //}
                        //else
                        //    await Task.Delay(5600);

                        _exit = true;
                    }, _cancellationToken2.Token);
            }
        }
    }
}