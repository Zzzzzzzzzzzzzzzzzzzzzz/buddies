﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace MitaarBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.XanHubId
                && Team.IsInTeam
                && MitaarBuddy._settings["Toggle"].AsBool())
            {
                if (Team.IsInTeam)
                {
                    foreach (TeamMember member in Team.Members)
                        if (!ReformState._teamCache.Contains(member.Identity))
                            ReformState._teamCache.Add(member.Identity);
                }

                return new EnterState();
            }      

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");
        }

        public void Tick()
        {
        }
    }
}
