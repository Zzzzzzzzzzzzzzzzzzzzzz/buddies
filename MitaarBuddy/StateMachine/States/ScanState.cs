﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MitaarBuddy
{
    public class ScanState : IState
    {
        private SimpleChar _target;

        private static bool _init = false;
        private static bool _proceed = false;

        private static double _beaconTimer;

        private CancellationTokenSource _cancellationToken1 = new CancellationTokenSource();
        private CancellationTokenSource _cancellationToken2 = new CancellationTokenSource();

        public IState GetNextState()
        {
            if (_target != null)
                return new FightState(_target);

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId
                && _proceed && !Team.Members.Any(c => c.Character == null))
            {
                return new ReformState();
            }

            if (!MitaarBuddy._settings["Toggle"].AsBool())
                return new IdleState();

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ScanState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ScanState::OnStateExit");

            _cancellationToken1.Cancel();
            _cancellationToken2.Cancel();

            _init = false;
            _proceed = false;
        }

        public void Tick()
        {
            if (!_proceed && !_init && DynelManager.Corpses.Any(c => c.Name.Contains("Sinuh")) 
                && !DynelManager.NPCs.Any(c => c.Name.Contains("Cocoon") || c.Name.Contains("Coccoon") || c.Name.Contains("Xan Spirit"))
                && MitaarBuddy.ModeSelection.Single == (MitaarBuddy.ModeSelection)MitaarBuddy._settings["ModeSelection"].AsInt32())
            {
                _init = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(15000);
                        MitaarBuddy._settings["Toggle"] = false;
                        MitaarBuddy.Toggle = false;
                        _init = false;
                        Chat.WriteLine("Finished.");
                    }, _cancellationToken1.Token);
            }

            if (_proceed && Time.NormalTime > _beaconTimer + 1.5f)
            {
                Dynel beacon = DynelManager.AllDynels
                    .Where(c => c.Name.Contains("Strange")
                        && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) < 3f)
                    .FirstOrDefault();

                if (beacon != null)
                    beacon.Use();

                _beaconTimer = Time.NormalTime;
            }

            if (!_proceed && !_init && DynelManager.Corpses.Any(c => c.Name.Contains("Sinuh"))
                && !DynelManager.NPCs.Any(c => c.Name.Contains("Cocoon") || c.Name.Contains("Coccoon") || c.Name.Contains("Xan Spirit"))
                && MitaarBuddy.ModeSelection.Loop == (MitaarBuddy.ModeSelection)MitaarBuddy._settings["ModeSelection"].AsInt32())
            {
                _init = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(15000);
                        Corpse _corpse = DynelManager.Corpses.FirstOrDefault(c => c.Name.Contains("Sinuh"));
                        await Task.Delay(1000);
                        MovementController.Instance.SetDestination((Vector3)_corpse?.Position);
                        await Task.Delay(22000);
                        MovementController.Instance.SetDestination(new Vector3(100.0f, 11.7f, 110.0f).Randomize(1f));
                        await Task.Delay(5000);
                        _proceed = true;
                        _init = false;
                    }, _cancellationToken2.Token);
            }

            if (MitaarBuddy._mob.Count >= 1)
            {
                if (MitaarBuddy._mob.FirstOrDefault().Health == 0) { return; }

                _target = MitaarBuddy._mob.FirstOrDefault();
                Chat.WriteLine($"Found target: {_target.Name}.");
            }
            else if (MitaarBuddy._bossMob.Count >= 1)
            {
                if (MitaarBuddy._bossMob.FirstOrDefault().Health == 0) { return; }

                _target = MitaarBuddy._bossMob.FirstOrDefault();
                Chat.WriteLine($"Found target: {_target.Name}.");
            }
        }
    }
}
