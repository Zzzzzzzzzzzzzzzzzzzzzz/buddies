﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using static RoamBuddy.RoamBuddy;

namespace RoamBuddy
{
    public static class Extensions
    {
        public static void AddRandomness(this ref Vector3 pos, int entropy)
        {
            pos.X += Next(-entropy, entropy);
            pos.Z += Next(-entropy, entropy);
        }

        public static int Next(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException("Min value is greater or equals than Max value.");
            }

            byte[] intBytes = new byte[4];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(intBytes);
            }

            return min + Math.Abs(BitConverter.ToInt32(intBytes, 0)) % (max - min + 1);
        }

        public static bool CanUseSitKit()
        {
            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)
                && DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving && !Game.IsZoning
                && !DynelManager.LocalPlayer.Buffs.Contains(280488) && !InCombat())
            {
                if (!Spell.List.Any(c => (c.Id == 223372 || c.Id == 287046) && c.IsReady)) { return false; }

                if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if (sitKits.Any())
                {
                    foreach (Item sitKit in sitKits.OrderBy(x => x.QualityLevel))
                    {
                        int skillReq = (sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f));

                        if (DynelManager.LocalPlayer.GetStat(Stat.FirstAid) >= skillReq || DynelManager.LocalPlayer.GetStat(Stat.Treatment) >= skillReq)
                            return true;
                    }
                }
            }

            return false;
        }
        public static bool CanLoot(Corpse corpse)
        {
            if (corpse.Position.DistanceFrom(RoamState._counterPosition) <= ScanRange
                && (corpse?.Container.IsOpen == false
                    || (corpse?.Container.IsOpen == true
                        && (!_corpsesLooted.Contains(corpse?.Name.Remove(0, 10))
                            || !_corpsesPositions.Contains((Vector3)corpse?.Position))))) { return true; }

            return false;
        }

        public static bool IsLootable(Corpse corpse)
        {
            if (corpse?.IsOpen == false
                || (corpse?.IsOpen == true
                    && !_corpsesLooted.Contains(corpse?.Name.Remove(0, 10)))
                || (_corpsesLooted.Contains(corpse?.Name.Remove(0, 10))
                    && !_corpsesPositions.Contains((Vector3)corpse?.Position))) { return true; }

            return false;
        }

        public static Corpse TryGetCorpse(out Corpse _corpse)
        {
            return _corpse = DynelManager.Corpses
                .Where(c => CanLoot(c) && !Team.Members.Select(m => m.Name).Contains(c.Name))
                .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                .FirstOrDefault();
        }

        public static void HandleTaunting(SimpleChar _target)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology)) { return; }

            if (FightState._aggToolCounter >= 2)
            {
                if (FightState._attackTimeout >= 1 || (InCombat() && !_target.IsMoving))
                {
                    MovementController.Instance.SetDestination((Vector3)_target?.Position);
                    FightState._attackTimeout = 0;
                    FightState._aggToolCounter = 0;
                }
                else
                {
                    FightState._aggToolCounter = 0;
                    FightState._attackTimeout++;
                }
            }

            if (FindTool(out Item _tool) && _target != null)
            {
                if (!Item.HasPendingUse && !PerkAction.List.Any(c => c.IsExecuting || c.IsPending))
                {
                    _tool.Use(_target, true);
                    FightState._aggToolCounter++;
                }
            }
        }
        public static bool IsBoss(SimpleChar _boss)
        {
            return _boss?.MaxHealth >= 1200000 || _boss?.Name == "Kyr'Ozch Maid" || _boss?.Name == "Kyr'Ozch Technician"
                        || _boss?.Name == "Defense Drone Tower" || _boss?.Name == "Kyr'Ozch Technician"
                        || _boss?.Name == "Sraosha" || _boss?.Name == "Desert Commander" || _boss?.Name == "Desert Warden"
                        || _boss?.Name == "Prototype Infiltration Unit";
        }
        public static bool AttackingTeam(SimpleChar mob)
        {
            if (mob?.FightingTarget == null || _helpers.Contains(mob.FightingTarget?.Name)) { return true; }

            //if (mob?.FightingTarget?.Name == "Guardian Spirit of Purification"
            //    || mob?.FightingTarget?.Name == "Rookie Alien Hunter"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Alpha"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Delta"
            //    || mob?.FightingTarget?.Name == "Unicorn Service Tower Gamma"
            //    || RoamBuddy._helpers.Contains(mob.FightingTarget?.Name)) { return true; }

            if (Team.IsInTeam) { return Team.Members.Select(m => m.Name).Contains(mob.FightingTarget?.Name); }

            return mob.FightingTarget?.Name == DynelManager.LocalPlayer.Name;
        }

        public static float PetMaxNanoPool(SimpleChar _healPet)
        {
            if (_healPet.Level == 215)
                return 5803;
            else if (_healPet.Level == 192)
                return 13310;
            else if (_healPet.Level == 169)
                return 11231;
            else if (_healPet.Level == 146)
                return 9153;
            else if (_healPet.Level == 123)
                return 7169;
            else if (_healPet.Level == 99)
                return 5327;
            else if (_healPet.Level == 77)
                return 3807;
            else if (_healPet.Level == 55)
                return 2404;
            else if (_healPet.Level == 33)
                return 1234;
            else if (_healPet.Level == 14)
                return 414;

            return 0;
        }

        public static bool IsNull(SimpleChar _target)
        {
            return _target == null
                || _target?.Health == 0
                || _target?.IsValid == false
                || _target?.IsInLineOfSight == false;    
        }

        public static bool ShouldStopAttack()
        {
            if (DynelManager.LocalPlayer.FightingTarget == null
                || (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(RelevantNanos.ShovelBuffs) == true
                    && DynelManager.LocalPlayer.FightingTarget?.Name.Contains("Aune") == true)) { return false; }

            if (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(253953) == true
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(273220) == true
                || DynelManager.LocalPlayer.Buffs.Contains(274101)
                || (DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(RelevantNanos.ShovelBuffs) == true
                    && DynelManager.LocalPlayer.FightingTarget?.Name != "Sraosha")
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(302745) == true
                || DynelManager.LocalPlayer.FightingTarget?.Buffs.Contains(205607) == true
                || DynelManager.LocalPlayer.FightingTarget?.IsPlayer == true) { return true; }

            return false;
        }

        public static bool BrokenCharmMobAttacking()
        {
            return _brokenCharmMob?.FightingTarget != null && (bool)_brokenCharmMob?.IsAttacking
                    && (Team.Members.Select(c => c.Identity).Any(c => (Identity)_brokenCharmMob?.FightingTarget.Identity == c)
                        || (bool)_brokenCharmMob.FightingTarget?.IsPet);
        }

        public static bool FindKit(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants.Kits.Contains(c.Id))) != null;
        }

        public static bool FindTool(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants._toolNames.Contains(c.Name))) != null;
        }

        public static bool ShouldTaunt(SimpleChar _target)
        {
            return _target?.IsInLineOfSight == true
                && _target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > RoamBuddy.Config.CharSettings[Game.ClientInst].AttackRange
                && !DynelManager.LocalPlayer.IsMoving
                && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending;
        }

        public static bool CanAttack()
        {
            return DynelManager.LocalPlayer.FightingTarget == null
                    && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending;
        }

        public static bool CanAttack(SimpleChar target)
        {
            if (target.Name.Contains("Phoe")) { return true; }

            if (target.Buffs.Contains(NanoLine.ShovelBuffs) && target.Name == "Sraosha") { return true; }

            if (!target.Buffs.Contains(NanoLine.ShovelBuffs) && !target.Buffs.Contains(302745)) { return true; }

            return false;
        }

        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending
                || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && (Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name)
                            || RoamBuddy._helpers.Contains(c.FightingTarget?.Name)));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }
    }
}
