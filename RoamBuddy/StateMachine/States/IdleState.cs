﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace RoamBuddy
{
    public class IdleState : IState
    {
        private static double _timer;
        private static bool _working = false;

        public IState GetNextState()
        {
            if (RoamBuddy._settings["Toggle"].AsBool()) { return new RoamState(); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");
        }

        public void Tick()
        {

        }
    }
}
