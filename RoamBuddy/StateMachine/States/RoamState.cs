﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoamBuddy
{
    public class RoamState : IState
    {
        private SimpleChar _target;

        public static bool _init = false;
        public static bool _reachedEnd = false;

        public static int _counter = 0;

        private static double _timeOut;
        private static double _timer;

        private static bool _charmMobAttacked = false;
        private static double _charmMobAttacking;

        public static Vector3 _nextWaypoint;
        public static Vector3 _counterPosition;
        public static List<Vector3> _waypointList = new List<Vector3>(RoamBuddy._waypoints);

        public IState GetNextState()
        {
            if (_target != null) { return new FightState(_target); }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("RoamState::OnStateEnter");

            if (!_init)
            {
                _counter = _waypointList.IndexOf(_waypointList.OrderBy(c => DynelManager.LocalPlayer.Position.DistanceFrom(c)).FirstOrDefault());
                MovementController.Instance.SetDestination(_waypointList.OrderBy(c => DynelManager.LocalPlayer.Position.DistanceFrom(c)).FirstOrDefault());

                _init = true;
            }

            //_timeOut = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("RoamState::OnStateExit");
        }

        private void HandleCharmScan()
        {
            RoamBuddy._charmMob = DynelManager.Characters
                .FirstOrDefault(c => !RoamBuddy._charmMobs.Contains(c.Identity) && (c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)));

            RoamBuddy._brokenCharmMob = DynelManager.Characters
                .FirstOrDefault(c => RoamBuddy._charmMobs.Contains(c.Identity) && !c.Buffs.Contains(NanoLine.CharmOther) && !c.Buffs.Contains(NanoLine.Charm_Short));

            if (RoamBuddy._charmMob != null && !RoamBuddy._charmMobs.Contains(RoamBuddy._charmMob.Identity))
                RoamBuddy._charmMobs.Add((Identity)RoamBuddy._charmMob?.Identity);

            if (_charmMobAttacked == true)
            {
                if (Time.NormalTime > _charmMobAttacking + 8f)
                {
                    if (Extensions.BrokenCharmMobAttacking())
                    {
                        _charmMobAttacked = false;

                        if (RoamBuddy._charmMobs.Contains((Identity)RoamBuddy._brokenCharmMob?.Identity))
                            RoamBuddy._charmMobs.Remove((Identity)RoamBuddy._brokenCharmMob?.Identity);
                    }
                    else { _charmMobAttacked = false; }
                }
            }
            else if (RoamBuddy._brokenCharmMob != null)
            {
                _charmMobAttacking = Time.NormalTime;
                _charmMobAttacked = true;
            }
        }

        public void Tick()
        {
            if (RoamBuddy._settings["Waiting"].AsBool() && !Extensions.InCombat())
            {
                if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66
                    || Team.Members.Any(c => c.Character != null 
                        && (c.Character.NanoPercent < 66 || c.Character.HealthPercent < 66 
                            || c.Character.Buffs.Contains(NanoLine.Root) == true || c.Character.Buffs.Contains(NanoLine.AOERoot)))
                    || DynelManager.Characters.Any(c => c.IsPet
                        && ((c.Nano / Extensions.PetMaxNanoPool(c) * 100 <= 55 && c.Nano != 10)
                            || c.Buffs.Contains(NanoLine.Root) == true
                            || c.Buffs.Contains(NanoLine.AOERoot) == true
                            || c.Buffs.Contains(NanoLine.Snare) == true
                            || c.Buffs.Contains(NanoLine.AOESnare) == true
                            || c.Buffs.Contains(NanoLine.Mezz) == true
                            || c.Buffs.Contains(NanoLine.AOEMezz) == true)
                        && c.IsInLineOfSight == true))
                { return; }
            }

            HandleCharmScan();

            if (RoamBuddy._switchMob.Count >= 1)
            {
                if (RoamBuddy._switchMob.FirstOrDefault().Health > 0)
                {
                    _target = RoamBuddy._switchMob.FirstOrDefault();
                    MovementController.Instance.Halt();
                    Chat.WriteLine($"Found target: {_target.Name}.");
                }
            }
            else if (RoamBuddy._mob.Count >= 1)
            {
                if (RoamBuddy._mob.FirstOrDefault().Health > 0)
                {
                    _target = RoamBuddy._mob.FirstOrDefault();
                    MovementController.Instance.Halt();
                    Chat.WriteLine($"Found target: {_target.Name}.");
                }
            }

            //if ((RoamBuddy._settings["IgnoreHinderedMovement"].AsBool() 
            //    && (Extensions.Snared()
            //        || Extensions.Rooted()
            //        || Extensions.Mezzed()
            //        || Extensions.TryGetHinderedPet(out Pet _hinderedPet)))
            //    || DynelManager.LocalPlayer.MovementState == MovementState.Sit
            //    || (!Extensions.InCombat() 
            //        && RoamBuddy._settings["Sitting"].AsBool()
            //        && ((DynelManager.LocalPlayer.HealthPercent < 66 
            //            || DynelManager.LocalPlayer.NanoPercent < 66)
            //        || (Extensions.TryGetHealPet(out Pet _healPet)
            //            && (_healPet.Character?.Nano / Extensions.PetMaxNanoPool(_healPet) * 100 <= 55)))))
            //{
            //    //_timeOut = Time.NormalTime;
            //    return;
            //}

            //TODO: Needs work after going back a few it will miss the timer check <= _timeOut + 9f
            //if (Time.NormalTime > _timeOut + 9 && MovementController.Instance.IsNavigating)
            //{
            //    _timeOut = Time.NormalTime;
            //    MovementController.Instance.Halt();

            //    if (_counter == 0)
            //    {
            //        _counter = _waypointList.Count();
            //        return;
            //    }
            //    else
            //    {
            //        _counter--;
            //        return;
            //    }
            //}

            if (!MovementController.Instance.IsNavigating && Spell.List.Any(c => (c.Id == 223372 || c.Id == 287046) && c.IsReady) && !Spell.HasPendingCast
                /*&& Time.NormalTime <= _timeOut + 9*/
                && RoamBuddy._mob.Count == 0)
            {
                //_timeOut = Time.NormalTime;

                if (_counter <= _waypointList.Count - 1)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(_waypointList.ElementAt(_counter)) < 1f)
                    {
                        _counter++;
                        return;
                    }

                    _counterPosition = _waypointList.FirstOrDefault(c => c == _waypointList.ElementAt(_counter));

                    MovementController.Instance.SetDestination(_counterPosition);
                }
                else if (!_reachedEnd)
                {
                    _timer = Time.NormalTime;
                    _reachedEnd = true;
                }
                else if (_reachedEnd && Time.NormalTime > _timer + RoamBuddy.RespawnDelay)
                {
                    //FightState._ignoreTargetIdentity.Clear();
                    _counter = 0;
                    _reachedEnd = false;

                    _counterPosition = _waypointList.FirstOrDefault(c => c == _waypointList.ElementAt(_counter));

                    MovementController.Instance.SetDestination(_counterPosition);
                }
            }
        }
    }
}
