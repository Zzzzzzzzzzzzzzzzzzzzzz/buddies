﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace RoamBuddy
{
    public class FightState : IState
    {
        public const double _fightTimeout = 45f;
        
        private double _fightStartTime;
        public static float _tetherDistance;

        private static bool _initLoot = false;

        public static double _timer;

        public static int _aggToolCounter = 0;
        public static int _attackTimeout = 0;

        private SimpleChar _target;

        public FightState(SimpleChar target)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Extensions.IsNull(_target)
                || (Time.NormalTime > _fightStartTime + _fightTimeout && _target?.MaxHealth <= 999999))
            {
                _target = null;

                if (RoamBuddy._settings["Looting"].AsBool()
                    && !DynelManager.NPCs.Any(c => c.Health > 0 && !Constants._ignores.Contains(c.Name) && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 30f))
                {
                    if (!_initLoot)
                    {
                        _timer = Time.NormalTime;
                        _initLoot = true;
                    }

                    if (_initLoot && Time.NormalTime > _timer + 3f) { return new LootState(); }
                }
                else { return new RoamState(); }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");

            _fightStartTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            _aggToolCounter = 0;
            _attackTimeout = 0;

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }

        public void Tick()
        {
            if (_target == null || RoamBuddy._initSit)
                return;

            if (Extensions.CanAttack())
            {
                if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= RoamBuddy.Config.CharSettings[Game.ClientInst].AttackRange)
                {
                    MovementController.Instance.Halt();
                    DynelManager.LocalPlayer.Attack(_target, false);
                    Chat.WriteLine($"Attacking {_target.Name}.");
                    _fightStartTime = Time.NormalTime;
                }
            }
            else if (DynelManager.LocalPlayer.FightingTarget != null)
            {
                if (RoamBuddy._switchMob.Count >= 1 && _target.Name != RoamBuddy._switchMob.FirstOrDefault().Name)
                {
                    if (RoamBuddy._switchMob.FirstOrDefault().Health > 0)
                    {
                        _target = RoamBuddy._switchMob.FirstOrDefault();
                        DynelManager.LocalPlayer.Attack(_target, false);
                        Chat.WriteLine($"Switching to target {_target.Name}.");
                        _fightStartTime = Time.NormalTime;
                    }
                }
            }

            if (RoamBuddy.ModeSelection.Taunt == (RoamBuddy.ModeSelection)RoamBuddy._settings["ModeSelection"].AsInt32()
                && Extensions.ShouldTaunt(_target)) { Extensions.HandleTaunting(_target); }

            if (RoamBuddy.ModeSelection.Path == (RoamBuddy.ModeSelection)RoamBuddy._settings["ModeSelection"].AsInt32()
                && !_target.IsMoving 
                && _target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > RoamBuddy.Config.CharSettings[Game.ClientInst].AttackRange)
            { MovementController.Instance.SetDestination(_target.Position); }
        }
    }
}
