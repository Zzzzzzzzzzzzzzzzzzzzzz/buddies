﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace RoamBuddy
{
    public class LootState : IState
    {
        //private static double _timeOut;
        private static double _timer;

        private static bool _working = false;

        public IState GetNextState()
        {
            if (!RoamBuddy._settings["Toggle"].AsBool()) { return new IdleState(); }

            if (!DynelManager.Corpses.Any(c => c.Position.DistanceFrom(RoamState._counterPosition) <= RoamBuddy.ScanRange)
                || !DynelManager.Corpses.Any(c => Extensions.CanLoot(c)))
            { return new RoamState(); }

            //if (Time.NormalTime > _timeOut + 9f) { return null; }

            //if (!DynelManager.Corpses.Any(c => c.DistanceFrom(DynelManager.LocalPlayer) <= RoamBuddy.ScanRange && Extensions.IsLootable(c))) { return new RoamState(); }

            return null;
        }

        public void OnStateEnter()
        {
            Inventory.ContainerOpened += OnContainerOpened;

            //Chat.WriteLine("LootState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("LootState::OnStateExit");

            Inventory.ContainerOpened -= OnContainerOpened;
        }
        private void OnContainerOpened(object sender, Container container)
        {
            if (container.Identity.Type != IdentityType.Corpse)
                return;

            Corpse _corpse = DynelManager.Corpses.FirstOrDefault(c => c.Container.Identity == container.Identity);

            if (_corpse != null)
            {
                if (!RoamBuddy._corpsesPositions.Contains(_corpse.Position))
                    RoamBuddy._corpsesPositions.Add(_corpse.Position);

                if (!RoamBuddy._corpsesLooted.Contains(_corpse.Name.Remove(0, 10)))
                    RoamBuddy._corpsesLooted.Add(_corpse.Name.Remove(0, 10));
            }
        }

        public void Tick()
        {
            if (Extensions.TryGetCorpse(out RoamBuddy._corpse) == null) { return; }

            if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)Extensions.TryGetCorpse(out RoamBuddy._corpse)?.Position) > 2f)
                MovementController.Instance.SetDestination((Vector3)Extensions.TryGetCorpse(out RoamBuddy._corpse)?.Position);
            else if (MovementController.Instance.IsNavigating) { MovementController.Instance.Halt(); }


            //if (!DynelManager.Corpses.Any(c => c.DistanceFrom(DynelManager.LocalPlayer) <= RoamBuddy.ScanRange && Extensions.IsLootable(c))) { return; }

            //if (Time.NormalTime > _timer + 2f && _working) { _working = false; }



            //Corpse corpse = DynelManager.Corpses
            //    .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) <= RoamBuddy.ScanRange && Extensions.IsLootable(c))
            //    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
            //    .FirstOrDefault();

            //if (corpse != null && !_working)
            //{
            //    _working = true;

            //    if (!MovementController.Instance.IsNavigating && DynelManager.LocalPlayer.Position.DistanceFrom(corpse.Position) >= 5f)
            //        MovementController.Instance.SetDestination(corpse.Position);

            //    if (DynelManager.LocalPlayer.Position.DistanceFrom(corpse.Position) < 5f)
            //    {
            //        if (MovementController.Instance.IsNavigating) { MovementController.Instance.Halt(); }

            //        if (!RoamBuddy._corpsesPositions.Contains(corpse.Position))
            //            RoamBuddy._corpsesPositions.Add(corpse.Position);

            //        if (!RoamBuddy._corpsesLooted.Contains(corpse.Name.Remove(0, 10)))
            //            RoamBuddy._corpsesLooted.Add(corpse.Name.Remove(0, 10));

            //        _timer = Time.NormalTime;
            //    }
            //}
        }
    }
}
