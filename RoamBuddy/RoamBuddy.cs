﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using RoamBuddy.IPCMessages;
using AOSharp.Common.GameData.UI;
using System.IO;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;

namespace RoamBuddy
{
    public class RoamBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static int AttackRange;
        public static int ScanRange;
        public static int RespawnDelay;

        public static bool Toggle = false;
        public static bool _init = false;
        public static bool _initSit = false;

        public static bool _subbedUIEvents = false;

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;
        public static Item _kit;

        public static SimpleChar _charmMob;
        public static SimpleChar _brokenCharmMob;

        public static float Tick = 0;

        public static double _stateTimeOut = Time.NormalTime;
        public static double _sitUpdateTimer;

        public static List<Vector3> _waypoints = new List<Vector3>();
        public static List<string> _helpers = new List<string>();

        public static List<SimpleChar> _mob = new List<SimpleChar>();
        public static List<SimpleChar> _switchMob = new List<SimpleChar>();

        public static List<Identity> _charmMobs = new List<Identity>();

        public static List<string> _corpsesLooted = new List<string>();
        public static List<Vector3> _corpsesPositions = new List<Vector3>();

        public static Corpse _corpse;

        private static double _mainUpdate;

        private static Window _infoWindow;
        private static Window _waypointWindow;
        private static Window _helperWindow;

        private static View _helperView;
        private static View _waypointView;

        public static string PluginDir;

        public static Settings _settings;
        public Window[] _windows => new Window[] { _waypointWindow, _helperWindow };

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("RoamBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\{Game.ClientInst}\\Config.json");
                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.AttackRange, OnAttackRangeMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.ScanRange, OnScanRangeMessage);

                Chat.RegisterCommand("buddy", RoamBuddyCommand);

                SettingsController.RegisterSettingsWindow("RoamBuddy", pluginDir + "\\UI\\RoamBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                Game.OnUpdate += OnUpdate;

                _settings.AddVariable("ModeSelection", (int)ModeSelection.Taunt);

                _settings.AddVariable("Toggle", false);
                _settings.AddVariable("Looting", false);
                _settings.AddVariable("Sitting", false);
                _settings.AddVariable("Waiting", false);      

                _settings["Toggle"] = false;

                Chat.WriteLine("RoamBuddy Loaded! Version: 0.9.9.95");
                Chat.WriteLine("/roambuddy for settings.");

                AttackRange = Config.CharSettings[Game.ClientInst].AttackRange;
                ScanRange = Config.CharSettings[Game.ClientInst].ScanRange;
                Tick = Config.CharSettings[Game.ClientInst].Tick;
                RespawnDelay = Config.CharSettings[Game.ClientInst].RespawnDelay;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        #region Callbacks

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            Chat.WriteLine("Buddy enabled.");
            _settings["Toggle"] = true;

            Toggle = true;

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None)
                Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            Toggle = false;

            _settings["Toggle"] = false;
            Chat.WriteLine("Buddy disabled.");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void OnAttackRangeMessage(int sender, IPCMessage msg)
        {
            AttackRangeMessage rangeMsg = (AttackRangeMessage)msg;

            Config.CharSettings[Game.ClientInst].AttackRange = rangeMsg.Range;
            AttackRange = rangeMsg.Range;
            Config.Save();
        }

        private void OnScanRangeMessage(int sender, IPCMessage msg)
        {
            ScanRangeMessage rangeMsg = (ScanRangeMessage)msg;

            Config.CharSettings[Game.ClientInst].ScanRange = rangeMsg.Range;
            ScanRange = rangeMsg.Range;
            Config.Save();
        }

        #endregion

        #region Handles

        private void HandleHelpersViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_helperView)) { return; }

                _helperView = View.CreateFromXml(PluginDir + "\\UI\\RoamBuddyHelpersView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Helpers", XmlViewName = "RoamBuddyHelpersView" }, _helperView);
            }
            else if (_helperWindow == null || (_helperWindow != null && !_helperWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_helperWindow, PluginDir, new WindowOptions() { Name = "Helpers", XmlViewName = "RoamBuddyHelpersView", WindowSize = new Rect(0, 0, 130, 345) }, _helperView, out var container);
                _helperWindow = container;
            }
        }

        private void HandleAddHelperViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _helpers.Add(Targeting.TargetChar?.Name);
        }

        private void HandleRemoveHelperViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _helpers.Remove(Targeting.TargetChar?.Name);
        }

        private void HandleClearHelpersViewClick(object s, ButtonBase button)
        {
            _helpers.Clear();
        }

        private void HandlePrintHelpersViewClick(object s, ButtonBase button)
        {
            foreach (string str in _helpers)
                Chat.WriteLine(str);
        }

        private void HandleWaypointsViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_waypointView)) { return; }

                _waypointView = View.CreateFromXml(PluginDir + "\\UI\\RoamBuddyWaypointsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Waypoints", XmlViewName = "RoamBuddyWaypointsView" }, _waypointView);
            }
            else if (_waypointWindow == null || (_waypointWindow != null && !_waypointWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_waypointWindow, PluginDir, new WindowOptions() { Name = "Waypoints", XmlViewName = "RoamBuddyWaypointsView", WindowSize = new Rect(0, 0, 130, 345) }, _waypointView, out var container);
                _waypointWindow = container;
            }
        }

        private void HandleAddWaypointViewClick(object s, ButtonBase button)
        {
            _waypoints.Add(DynelManager.LocalPlayer.Position);
        }
        private void HandleRemoveWaypointViewClick(object s, ButtonBase button)
        {
            Vector3 waypoint = _waypoints.OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer.Position)).FirstOrDefault();

            if (waypoint != Vector3.Zero)
                _waypoints.Remove(waypoint);
        }

        private void HandleClearWaypointsViewClick(object s, ButtonBase button)
        {
            _waypoints.Clear();
        }

        private void HandleSaveListViewClick(object s, ButtonBase button)
        {
            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                window.FindView("ListNameBox", out TextInputView nameInput);

                if (nameInput != null && !string.IsNullOrEmpty(nameInput.Text))
                {
                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints");

                    string _exportPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\RoamBuddy\Waypoints\{nameInput.Text}.txt";
                    Chat.WriteLine($"Waypoints list saved.");

                    if (!File.Exists(_exportPath))
                    {
                        // Create the file.
                        using (FileStream fs = File.Create(_exportPath))
                        {
                            foreach (Vector3 vector in _waypoints)
                            {
                                string vectorstring = vector.ToString();
                                string vectorstring2 = vectorstring.Substring(1, vectorstring.Length - 2);

                                Byte[] info =
                                        new UTF8Encoding(true).GetBytes($"{vectorstring2}-");

                                fs.Write(info, 0, info.Length);
                            }
                        }
                    }
                }
            }
        }
        private void HandleLoadListViewClick(object s, ButtonBase button)
        {
            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                window.FindView("ListNameBox", out TextInputView nameInput);

                if (nameInput != null && !string.IsNullOrEmpty(nameInput.Text))
                {
                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints");

                    string _loadPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\RoamBuddy\Waypoints\{nameInput.Text}.txt";
                    Chat.WriteLine($"Waypoints list loaded.");

                    if (!File.Exists(_loadPath))
                    {
                        Chat.WriteLine($"No such file.");
                        return;
                    }

                    using (StreamReader sr = File.OpenText(_loadPath))
                    {
                        string[] _stringAsArray = sr.ReadLine().Split('-');

                        foreach (string _string in _stringAsArray)
                        {
                            if (_string.Length > 1)
                            {
                                float x, y, z;

                                string[] _stringAsArraySplit = _string.Split(',');
                                if (_string.Contains('.'))
                                {
                                    x = float.Parse(_stringAsArraySplit[0], CultureInfo.InvariantCulture.NumberFormat);
                                    y = float.Parse(_stringAsArraySplit[1], CultureInfo.InvariantCulture.NumberFormat);
                                    z = float.Parse(_stringAsArraySplit[2], CultureInfo.InvariantCulture.NumberFormat);
                                }
                                else
                                {
                                    x = float.Parse($"{_stringAsArraySplit[0]}.{_stringAsArraySplit[1]}", CultureInfo.InvariantCulture.NumberFormat);
                                    y = float.Parse($"{_stringAsArraySplit[2]}.{_stringAsArraySplit[3]}", CultureInfo.InvariantCulture.NumberFormat);
                                    z = float.Parse($"{_stringAsArraySplit[4]}.{_stringAsArraySplit[5]}", CultureInfo.InvariantCulture.NumberFormat);
                                }

                                _waypoints.Add(new Vector3(x, y, z));
                            }
                        }
                    }
                }
            }
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\RoamBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        #endregion

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            if (Time.NormalTime > _mainUpdate + Tick)
            {
                if (_settings["Toggle"].AsBool() && Extensions.ShouldStopAttack())
                {
                    DynelManager.LocalPlayer.StopAttack();
                    Chat.WriteLine($"Stopping attack.");
                }

                if (_settings["Sitting"].AsBool())
                    ListenerSit();

                Scanning();

                _stateMachine.Tick();
                _mainUpdate = Time.NormalTime;
            }

            #region Drawing Waypoints

            if (_waypoints.Count >= 1)
            {
                foreach (Vector3 pos in _waypoints)
                {
                    Debug.DrawSphere(pos, 0.2f, DebuggingColor.White);
                }
            }

            #endregion

            #region UI Update

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                if (window.FindView("RoamBuddyAddHelper", out Button addHelperView))
                {
                    addHelperView.Tag = window;
                    addHelperView.Clicked = HandleAddHelperViewClick;
                }

                if (window.FindView("RoamBuddyRemoveHelper", out Button removeHelperView))
                {
                    removeHelperView.Tag = window;
                    removeHelperView.Clicked = HandleRemoveHelperViewClick;
                }

                if (window.FindView("RoamBuddyClearHelpers", out Button clearHelpersView))
                {
                    clearHelpersView.Tag = window;
                    clearHelpersView.Clicked = HandleClearHelpersViewClick;
                }

                if (window.FindView("RoamBuddyPrintHelpers", out Button printHelpersView))
                {
                    printHelpersView.Tag = window;
                    printHelpersView.Clicked = HandlePrintHelpersViewClick;
                }
                if (window.FindView("RoamBuddyAddWaypoint", out Button addWaypointView))
                {
                    addWaypointView.Tag = window;
                    addWaypointView.Clicked = HandleAddWaypointViewClick;
                }

                if (window.FindView("RoamBuddyRemoveWaypoint", out Button removeWaypointView))
                {
                    removeWaypointView.Tag = window;
                    removeWaypointView.Clicked = HandleRemoveWaypointViewClick;
                }

                if (window.FindView("RoamBuddyClearWaypoints", out Button clearWaypointsView))
                {
                    clearWaypointsView.Tag = window;
                    clearWaypointsView.Clicked = HandleClearWaypointsViewClick;
                }

                if (window.FindView("RoamBuddySaveList", out Button saveListView))
                {
                    saveListView.Tag = window;
                    saveListView.Clicked = HandleSaveListViewClick;
                }

                if (window.FindView("RoamBuddyLoadList", out Button loadListView))
                {
                    loadListView.Tag = window;
                    loadListView.Clicked = HandleLoadListViewClick;
                }
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                SettingsController.settingsWindow.FindView("TickBox", out TextInputView tickInput);
                SettingsController.settingsWindow.FindView("AttackRangeBox", out TextInputView attackRangeInput);
                SettingsController.settingsWindow.FindView("ScanRangeBox", out TextInputView scanRangeInput);
                SettingsController.settingsWindow.FindView("RespawnDelayBox", out TextInputView _respawnDelayInput);

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (attackRangeInput != null && !string.IsNullOrEmpty(attackRangeInput.Text)
                    && int.TryParse(attackRangeInput.Text, out int attackRangeInputValue)
                    && Config.CharSettings[Game.ClientInst].AttackRange != attackRangeInputValue)
                {
                    Config.CharSettings[Game.ClientInst].AttackRange = attackRangeInputValue;
                    IPCChannel.Broadcast(new AttackRangeMessage()
                    {
                        Range = Config.CharSettings[Game.ClientInst].AttackRange
                    });
                }
                if (scanRangeInput != null && !string.IsNullOrEmpty(scanRangeInput.Text)
                    && int.TryParse(scanRangeInput.Text, out int scanRangeInputValue)
                    && Config.CharSettings[Game.ClientInst].ScanRange != scanRangeInputValue)
                {
                    Config.CharSettings[Game.ClientInst].ScanRange = scanRangeInputValue;
                    IPCChannel.Broadcast(new ScanRangeMessage()
                    {
                        Range = Config.CharSettings[Game.ClientInst].ScanRange
                    });
                }
                if (int.TryParse(_respawnDelayInput.Text, out int _respawnDelayValue)
                    && Config.CharSettings[Game.ClientInst].RespawnDelay != _respawnDelayValue)
                {
                    Config.CharSettings[Game.ClientInst].RespawnDelay = _respawnDelayValue;
                    RespawnDelay = _respawnDelayValue;
                }

                if (SettingsController.settingsWindow.FindView("RoamBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (SettingsController.settingsWindow.FindView("RoamBuddyWaypointsView", out Button waypointView))
                {
                    waypointView.Tag = SettingsController.settingsWindow;
                    waypointView.Clicked = HandleWaypointsViewClick;
                }

                if (SettingsController.settingsWindow.FindView("RoamBuddyHelpersView", out Button helperView))
                {
                    helperView.Tag = SettingsController.settingsWindow;
                    helperView.Clicked = HandleHelpersViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (Leader == Identity.None)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;
                    }

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StartMessage());

                    Start();
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Stop();

                    if (DynelManager.LocalPlayer.Identity == Leader)
                        IPCChannel.Broadcast(new StopMessage());
                }
            }

            #endregion
        }

        #region Misc

        private void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;

            RoamState._init = false;
            RoamState._counter = 0;
            RoamState._reachedEnd = false;

            Chat.WriteLine("Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();
        }
        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void Scanning()
        {
            _switchMob = DynelManager.NPCs
               .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) <= ScanRange
                   && !Constants._ignores.Contains(c.Name)
                   && c.Health > 0 && c.IsInLineOfSight
                   && !_charmMobs.Contains(c.Identity)
                   && !Extensions.IsBoss(c)
                   && Extensions.AttackingTeam(c)
                   && (c.Name == "Hacker'Uri" || c.Name == "Corrupted Xan-Len"))
                .OrderBy(c => c.HealthPercent)
                .ThenByDescending(c => c.MaxHealth)
                .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
               //.OrderBy(c => c.HealthPercent)
               //.ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
               .ToList();

            _mob = DynelManager.Characters
                .Where(c => !c.IsPlayer && c.DistanceFrom(DynelManager.LocalPlayer) <= ScanRange
                    && !Constants._ignores.Contains(c.Name)
                    && c.Health > 0 && c.IsInLineOfSight
                    && !_charmMobs.Contains(c.Identity)
                    && !Extensions.IsBoss(c)
                    && Extensions.AttackingTeam(c)
                    && !Team.Members.Select(b => b.Name).Contains(c.Name)
                    && !c.Name.Contains("sapling")
                    && !c.IsPet)
                .OrderBy(c => c.HealthPercent)
                .ThenByDescending(c => c.MaxHealth)
                .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                //.OrderBy(c => c.HealthPercent)
                //.ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ThenByDescending(c => c.Name == "Corrupted Hiisi Berserker")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Cur")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Kuir")
                .ThenByDescending(c => c.Name == "Hacker'Uri")
                .ThenByDescending(c => c.Name == "Corrupted Xan-Len")
                .ToList();
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
                Config.CharSettings[Game.ClientInst].AttackRangeChangedEvent -= AttackRange_Changed;
                Config.CharSettings[Game.ClientInst].ScanRangeChangedEvent -= ScanRange_Changed;
                Config.CharSettings[Game.ClientInst].RespawnDelayChangedEvent -= RespawnDelay_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
                Config.CharSettings[Game.ClientInst].AttackRangeChangedEvent += AttackRange_Changed;
                Config.CharSettings[Game.ClientInst].ScanRangeChangedEvent += ScanRange_Changed;
                Config.CharSettings[Game.ClientInst].RespawnDelayChangedEvent += RespawnDelay_Changed;
            }
        }

        private void ListenerSit()
        {
            if (_initSit == false && Extensions.CanUseSitKit()
                && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            {
                Task.Factory.StartNew(
                    async () =>
                    {
                        _initSit = true;
                        await Task.Delay(400);
                        MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                        await Task.Delay(1800);
                        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                        await Task.Delay(400);
                        _initSit = false;
                    });
            }
        }

        private void RoamBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        if (Leader == Identity.None)
                        {
                            IsLeader = true;
                            Leader = DynelManager.LocalPlayer.Identity;
                        }

                        if (DynelManager.LocalPlayer.Identity == Leader)
                            IPCChannel.Broadcast(new StartMessage());

                        _settings["Toggle"] = true;
                        Start();

                    }
                    else
                    {
                        Stop();
                        _settings["Toggle"] = false;
                        IPCChannel.Broadcast(new StopMessage());
                    }
                }
                else if (param.Length >= 1)
                {
                    switch (param[0].ToLower())
                    {
                        case "helpers":
                            if (param[1].ToLower() == "add")
                            {
                                if (param.Length > 2)
                                {
                                    string name = param[2];

                                    if (!_helpers.Contains(name))
                                    {
                                        _helpers.Add(name);
                                        Chat.WriteLine($"Added {name} to Helpers list");
                                    }
                                    else if (_helpers.Contains(name))
                                    {
                                        _helpers.Remove(name);
                                        Chat.WriteLine($"Removed {name} from Helpers list");
                                    }
                                }
                                else
                                {
                                    if (Targeting.TargetChar != null)
                                    {
                                        if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                        {
                                            _helpers.Add(Targeting.TargetChar?.Name);
                                            Chat.WriteLine($"Added {Targeting.TargetChar?.Name} to Helpers list");
                                        }
                                    }
                                    else
                                        Chat.WriteLine($"Must have a target.");
                                }
                            }
                            else if (param[1].ToLower() == "remove")
                            {
                                if (param.Length > 2)
                                {
                                    string name = param[2];

                                    if (!_helpers.Contains(name))
                                    {
                                        _helpers.Add(name);
                                        Chat.WriteLine($"Added {name} to Helpers list");
                                    }
                                    else if (_helpers.Contains(name))
                                    {
                                        _helpers.Remove(name);
                                        Chat.WriteLine($"Removed {name} from Helpers list");
                                    }
                                }
                                else
                                {
                                    if (Targeting.TargetChar != null)
                                    {
                                        if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                        {
                                            _helpers.Add(Targeting.TargetChar?.Name);
                                            Chat.WriteLine($"Added {Targeting.TargetChar?.Name} to Helpers list");
                                        }
                                    }
                                    else
                                        Chat.WriteLine($"Must have a target.");
                                }
                            }
                            break;
                        case "ignore":
                            if (param.Length > 1)
                            {
                                string name = string.Join(" ", param.Skip(1));

                                if (!Constants._ignores.Contains(name))
                                {
                                    Constants._ignores.Add(name);
                                    Chat.WriteLine($"Added {name} to ignored mob list");
                                }
                                else if (Constants._ignores.Contains(name))
                                {
                                    Constants._ignores.Remove(name);
                                    Chat.WriteLine($"Removed {name} from ignored mob list");
                                }
                            }
                            else
                            {
                                if (Targeting.TargetChar != null)
                                {
                                    if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name)
                                    {
                                        Constants._ignores.Add(Targeting.TargetChar?.Name);
                                        Chat.WriteLine($"Added {Targeting.TargetChar?.Name} to ignored mob list");
                                    }
                                }
                                else
                                    Chat.WriteLine($"Must have a target.");
                            }
                            break;
                        case "load":
                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints");

                            string _loadPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\RoamBuddy\Waypoints\{param[1]}.txt";
                            Chat.WriteLine($"Waypoints list loaded.");

                            if (!File.Exists(_loadPath))
                            {
                                Chat.WriteLine($"No such file.");
                                return;
                            }

                            using (StreamReader sr = File.OpenText(_loadPath))
                            {
                                string[] _stringAsArray = sr.ReadLine().Split('-');

                                foreach (string _string in _stringAsArray)
                                {
                                    if (_string.Length > 1)
                                    {
                                        float x, y, z;

                                        string[] _stringAsArraySplit = _string.Split(',');
                                        if (_string.Contains('.'))
                                        {
                                            x = float.Parse(_stringAsArraySplit[0], CultureInfo.InvariantCulture.NumberFormat);
                                            y = float.Parse(_stringAsArraySplit[1], CultureInfo.InvariantCulture.NumberFormat);
                                            z = float.Parse(_stringAsArraySplit[2], CultureInfo.InvariantCulture.NumberFormat);
                                        }
                                        else
                                        {
                                            x = float.Parse($"{_stringAsArraySplit[0]}.{_stringAsArraySplit[1]}", CultureInfo.InvariantCulture.NumberFormat);
                                            y = float.Parse($"{_stringAsArraySplit[2]}.{_stringAsArraySplit[3]}", CultureInfo.InvariantCulture.NumberFormat);
                                            z = float.Parse($"{_stringAsArraySplit[4]}.{_stringAsArraySplit[5]}", CultureInfo.InvariantCulture.NumberFormat);
                                        }

                                        _waypoints.Add(new Vector3(x, y, z));
                                    }
                                }
                            }
                            break;
                        case "save":
                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoamBuddy\\Waypoints");

                            string _exportPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\RoamBuddy\Waypoints\{param[1]}.txt";
                            Chat.WriteLine($"Waypoints list exported.");

                            if (!File.Exists(_exportPath))
                            {
                                // Create the file.
                                using (FileStream fs = File.Create(_exportPath))
                                {
                                    foreach (Vector3 vector in _waypoints)
                                    {
                                        string vectorstring = vector.ToString();
                                        string vectorstring2 = vectorstring.Substring(1, vectorstring.Length - 2);

                                        Byte[] info =
                                                new UTF8Encoding(true).GetBytes($"{vectorstring2}-");

                                        fs.Write(info, 0, info.Length);
                                    }
                                }
                            }
                            break;
                        case "addpos":
                            _waypoints.Add(DynelManager.LocalPlayer.Position);
                            break;
                        default:
                            return;
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }

        public static void AttackRange_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].AttackRange = e;
            AttackRange = e;
            Config.Save();
        }

        public static void ScanRange_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].ScanRange = e;
            ScanRange = e;
            Config.Save();
        }

        public static void RespawnDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].RespawnDelay = e;
            Config.Save();
        }

        public enum ModeSelection
        {
            Taunt, Path
        }

        public static class RelevantNanos
        {
            public const int ReflectionsOfAngerLong = 290404;
            public static readonly int[] ShovelBuffs = Spell.GetSpellsForNanoline(NanoLine.ShovelBuffs).Where(c => c.Id != ReflectionsOfAngerLong).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
        }

        #endregion
    }
}
