﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSTBuddy
{
    public class PullState : IState
    {
        public static bool SwitchingState = false;

        public static bool MoveToStartNorth = false;
        public static bool MoveToStartSouth = false;

        public static bool StartedSwitch = false;

        public static bool StartedSouthSwitch = false;
        public static bool StartedNorthSwitch = false;

        public static bool GettingAllSamePosition = false;



        public static bool ReadyToCast = false;

        public static bool _init = false;

        private static double _timer;
        private static double _castTimer;

        public static bool _reachedEnd = false;

        public static int _counter = 0;
        private static double _waitAtPos;

        public static int _mongoId = 0;

        public static Vector3 _nextWaypoint;
        public static Vector3 _counterPosition;

        public static List<Vector3> _waypointList = new List<Vector3>(OSTBuddy._waypoints);

        public IState GetNextState()
        {
            if (!OSTBuddy.Toggle)
            {
                MovementController.Instance.Halt();
                return new IdleState();
            }

            List<SimpleChar> mobs = DynelManager.NPCs
                .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) <= 30f
                        && c.Health > 0 && c.IsInLineOfSight)
                .ToList();

            Spell.Find(_mongoId, out Spell mongobuff);

            if (SwitchingState && mobs.Count >= 1
                && !DynelManager.LocalPlayer.IsMoving)
            {
                mongobuff?.Cast();
                SwitchingState = false;
                return new CastState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            if (!_init)
            {
                _counter = _waypointList.IndexOf(_waypointList.OrderBy(c => DynelManager.LocalPlayer.Position.DistanceFrom(c)).FirstOrDefault());
                MovementController.Instance.SetDestination(_waypointList.OrderBy(c => DynelManager.LocalPlayer.Position.DistanceFrom(c)).FirstOrDefault());

                _init = true;
            }

            //Chat.WriteLine("PullState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("PullState::OnStateExit");
        }

        public void Tick()
        {
            if (OSTBuddy._waypoints.Count >= 1)
            {
                foreach (Vector3 pos in OSTBuddy._waypoints)
                {
                    Debug.DrawSphere(pos, 0.2f, DebuggingColor.White);
                }
            }

            if (OSTBuddy.MongoSelection.Demolish == (OSTBuddy.MongoSelection)OSTBuddy._settings["MongoSelection"].AsInt32())
                _mongoId = 270786;
            else if (OSTBuddy.MongoSelection.Slam == (OSTBuddy.MongoSelection)OSTBuddy._settings["MongoSelection"].AsInt32())
                _mongoId = 100198;

            Spell.Find(_mongoId, out Spell mongobuff);

            if (!MovementController.Instance.IsNavigating)
            {
                if (!ReadyToCast)
                {
                    if (_counter <= _waypointList.Count - 1)
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(_waypointList.ElementAt(_counter)) < 1f)
                        {
                            _counter++;
                            MovementController.Instance.SetMovement(MovementAction.Update);
                            ReadyToCast = true;
                            _waitAtPos = Time.NormalTime;
                            return;
                        }

                        _counterPosition = _waypointList.FirstOrDefault(c => c == _waypointList.ElementAt(_counter));

                        MovementController.Instance.SetDestination(_counterPosition);
                    }
                    else if (!_reachedEnd)
                    {
                        _timer = Time.NormalTime;
                        _reachedEnd = true;
                    }
                    else if (_reachedEnd)
                    {
                        _counter = 0;
                        _reachedEnd = false;

                        _counterPosition = _waypointList.FirstOrDefault(c => c == _waypointList.ElementAt(_counter));
                        MovementController.Instance.SetDestination(_counterPosition);

                        SwitchingState = true;
                    }
                }
                else if (!Extensions.IsCasting()) 
                {
                    if (Time.NormalTime > _castTimer + 2f)
                    {
                        _castTimer = Time.NormalTime;
                        mongobuff?.Cast();
                    }

                    if (Time.NormalTime > _waitAtPos + 5.8f) { ReadyToCast = false; }
                }
            }
        }
    }
}
