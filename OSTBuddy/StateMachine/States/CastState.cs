﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OSTBuddy
{
    public class CastState : IState
    {
        public const double RefreshMongoTime = 8f;
        public const double RefreshAbsorbTime = 11f;

        public double _timer;
        public double _refreshMongoTimer;
        public double _refreshAbsorbTimer;

        public static int _mongoId = 0;

        public SimpleChar LeaderChar;
        Spell absorb = null;


        public IState GetNextState()
        {
            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                if (!OSTBuddy.Toggle)
                {
                    MovementController.Instance.Halt();
                    return new IdleState();
                }

                if (Time.NormalTime > _timer + OSTBuddy.Config.CharSettings[Game.ClientInst].RespawnDelay && OSTBuddy.MobsAllDead)
                {
                    OSTBuddy.MobsAllDead = false;
                    return new PullState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("CastState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("CastState::OnStateExit");
        }

        public void Tick()
        {
            if (OSTBuddy._waypoints.Count >= 1)
            {
                foreach (Vector3 pos in OSTBuddy._waypoints)
                {
                    Debug.DrawSphere(pos, 0.2f, DebuggingColor.White);
                }
            }

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                if (OSTBuddy.MongoSelection.Demolish == (OSTBuddy.MongoSelection)OSTBuddy._settings["MongoSelection"].AsInt32())
                    _mongoId = 270786;
                else if (OSTBuddy.MongoSelection.Slam == (OSTBuddy.MongoSelection)OSTBuddy._settings["MongoSelection"].AsInt32())
                    _mongoId = 100198;

                Spell.Find(_mongoId, out Spell mongobuff);

                if (absorb == null && DynelManager.LocalPlayer.Level >= 200)
                    absorb = Spell.List.Where(x => x.Nanoline == NanoLine.AbsorbACBuff).OrderBy(x => x.StackingOrder).FirstOrDefault();

                List<SimpleChar> mobsatend = DynelManager.NPCs
                    .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) <= 15f
                        && c.Health > 0 && c.FightingTarget != null
                        && c.Position.DistanceFrom(OSTBuddy._waypoints.Last()) < 8f)
                    .ToList();

                List<Corpse> mobsatenddead = DynelManager.Corpses
                    .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) <= 15f
                        && c.Position.DistanceFrom(OSTBuddy._waypoints.Last()) < 8f)
                    .ToList();

                if (mobsatend.Count == 0 && mobsatenddead.Count >= 1 && !OSTBuddy.MobsAllDead)
                {
                    _timer = Time.NormalTime;
                    OSTBuddy.MobsAllDead = true;
                }

                if (mobsatend.Count >= 1)
                {
                    if (PullState._mongoId == 0) { return; }

                    if (!Spell.HasPendingCast && !Extensions.IsCasting() && Time.NormalTime > _refreshMongoTimer + RefreshMongoTime)
                    {
                        mongobuff?.Cast();
                        _refreshMongoTimer = Time.NormalTime;
                    }

                    if (absorb == null) { return; }

                    if (!Spell.HasPendingCast && !Extensions.IsCasting() && Time.NormalTime > _refreshAbsorbTimer + RefreshAbsorbTime)
                    {
                        absorb?.Cast();
                        _refreshAbsorbTimer = Time.NormalTime;
                    }
                }
            }
        }
    }
}
