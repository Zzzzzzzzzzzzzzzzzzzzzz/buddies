﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;
using AOSharp.Common.GameData.UI;
using System.Globalization;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;

namespace OSTBuddy
{
    public class OSTBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public static string PluginDir;

        public static Window infoWindow;
        private static Window _waypointWindow;

        private static View _waypointView;

        public static bool Toggle = false;

        public static bool SwitchForLastPos = false;
        public static bool MobsAllDead = false;
        public static bool Slam = false;
        public static bool Demo = true;

        public static double _castTimer;
        public static double _castRecharge;
        public static bool _casting = false;

        private static double _mainUpdate;

        public static int RespawnDelay;

        public double _refreshAbsorbTimer;

        public static Vector3 currentPos;
        public static Vector3 lastPos;
        public static List<Vector3> _waypoints = new List<Vector3>();

        public static Settings _settings;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("OSTBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\{Game.ClientInst}\\Config.json");

                Config.CharSettings[Game.ClientInst].RespawnDelayChangedEvent += RespawnDelay_Changed;

                Chat.RegisterCommand("buddy", OSTBuddyCommand); Chat.RegisterCommand("buddy", OSTBuddyCommand);

                SettingsController.RegisterSettingsWindow("OSTBuddy", pluginDir + "\\UI\\OSTBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                Game.OnUpdate += OnUpdate;
                Network.N3MessageSent += Network_N3MessageSent;

                _settings.AddVariable("Toggle", false);

                _settings.AddVariable("MongoSelection", (int)MongoSelection.Demolish);

                _settings["Toggle"] = false;

                Chat.WriteLine("OSTBuddy Loaded! Version: 0.9.9.91");
                Chat.WriteLine("/ostbuddy for settings.");

                RespawnDelay = Config.CharSettings[Game.ClientInst].RespawnDelay;
            }
            catch(Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public Window[] _windows => new Window[] { _waypointWindow };

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void RespawnDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].RespawnDelay = e;
            Config.Save();
        }

        private void Start()
        {
            Toggle = true;

            Chat.WriteLine("Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;
            PullState._counter = 0;

            Chat.WriteLine("Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();
        }
        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

            if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action == CharacterActionType.CastNano)
                {
                    if (Spell.Find(charActionMsg.Parameter2, out Spell _currentNano))
                    {
                        _casting = true;
                        _castTimer = Time.NormalTime;
                        _castRecharge = _currentNano.AttackDelay;
                    }
                }
            }
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\OSTBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 250, 345),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);
                infoWindow.Show(true);
        }


        private void HandleWaypointsViewClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_waypointView)) { return; }

                _waypointView = View.CreateFromXml(PluginDir + "\\UI\\OSTBuddyWaypointsView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Waypoints", XmlViewName = "OSTBuddyWaypointsView" }, _waypointView);
            }
            else if (_waypointWindow == null || (_waypointWindow != null && !_waypointWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_waypointWindow, PluginDir, new WindowOptions() { Name = "Waypoints", XmlViewName = "OSTBuddyWaypointsView", WindowSize = new Rect(0, 0, 130, 345) }, _waypointView, out var container);
                _waypointWindow = container;
            }
        }

        private void HandleAddWaypointViewClick(object s, ButtonBase button)
        {
            _waypoints.Add(DynelManager.LocalPlayer.Position);
        }
        private void HandleRemoveWaypointViewClick(object s, ButtonBase button)
        {
            Vector3 waypoint = _waypoints.OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer.Position)).FirstOrDefault();

            if (waypoint != Vector3.Zero)
                _waypoints.Remove(waypoint);
        }

        private void HandleClearWaypointsViewClick(object s, ButtonBase button)
        {
            _waypoints.Clear();
        }

        private void HandleSaveListViewClick(object s, ButtonBase button)
        {
            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                window.FindView("ListNameBox", out TextInputView nameInput);

                if (nameInput != null && !string.IsNullOrEmpty(nameInput.Text))
                {
                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints");

                    string _exportPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\OSTBuddy\Waypoints\{nameInput.Text}.txt";
                    Chat.WriteLine($"Waypoints list exported.");

                    if (!File.Exists(_exportPath))
                    {
                        // Create the file.
                        using (FileStream fs = File.Create(_exportPath))
                        {
                            foreach (Vector3 vector in _waypoints)
                            {
                                string vectorstring = vector.ToString();
                                string vectorstring2 = vectorstring.Substring(1, vectorstring.Length - 2);

                                Byte[] info =
                                        new UTF8Encoding(true).GetBytes($"{vectorstring2}-");

                                fs.Write(info, 0, info.Length);
                            }
                        }
                    }
                }
            }
        }
        private void HandleLoadListViewClick(object s, ButtonBase button)
        {
            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                window.FindView("ListNameBox", out TextInputView nameInput);

                if (nameInput != null && !string.IsNullOrEmpty(nameInput.Text))
                {
                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy");

                    if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints"))
                        Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints");

                    string _loadPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\OSTBuddy\Waypoints\{nameInput.Text}.txt";
                    Chat.WriteLine($"Waypoints list loaded.");

                    if (!File.Exists(_loadPath))
                    {
                        Chat.WriteLine($"No such file.");
                        return;
                    }

                    using (StreamReader sr = File.OpenText(_loadPath))
                    {
                        string[] _stringAsArray = sr.ReadLine().Split('-');

                        foreach (string _string in _stringAsArray)
                        {
                            if (_string.Length > 1)
                            {
                                float x, y, z;

                                string[] _stringAsArraySplit = _string.Split(',');
                                if (_string.Contains('.'))
                                {
                                    x = float.Parse(_stringAsArraySplit[0], CultureInfo.InvariantCulture.NumberFormat);
                                    y = float.Parse(_stringAsArraySplit[1], CultureInfo.InvariantCulture.NumberFormat);
                                    z = float.Parse(_stringAsArraySplit[2], CultureInfo.InvariantCulture.NumberFormat);
                                }
                                else
                                {
                                    x = float.Parse($"{_stringAsArraySplit[0]}.{_stringAsArraySplit[1]}", CultureInfo.InvariantCulture.NumberFormat);
                                    y = float.Parse($"{_stringAsArraySplit[2]}.{_stringAsArraySplit[3]}", CultureInfo.InvariantCulture.NumberFormat);
                                    z = float.Parse($"{_stringAsArraySplit[4]}.{_stringAsArraySplit[5]}", CultureInfo.InvariantCulture.NumberFormat);
                                }

                                _waypoints.Add(new Vector3(x, y, z));
                            }
                        }
                    }
                }
            }
        }

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning) { return; }

            if (Time.NormalTime > _mainUpdate + 0.0f)
            {
                _stateMachine.Tick();
                _mainUpdate = Time.NormalTime;
            }

            if (_waypoints.Count >= 1)
            {
                foreach (Vector3 pos in _waypoints)
                {
                    Debug.DrawSphere(pos, 0.2f, DebuggingColor.White);
                }
            }

            #region UI Update

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                if (window.FindView("OSTBuddyAddWaypoint", out Button addWaypointView))
                {
                    addWaypointView.Tag = window;
                    addWaypointView.Clicked = HandleAddWaypointViewClick;
                }

                if (window.FindView("OSTBuddyRemoveWaypoint", out Button removeWaypointView))
                {
                    removeWaypointView.Tag = window;
                    removeWaypointView.Clicked = HandleRemoveWaypointViewClick;
                }

                if (window.FindView("OSTBuddyClearWaypoints", out Button clearWaypointsView))
                {
                    clearWaypointsView.Tag = window;
                    clearWaypointsView.Clicked = HandleClearWaypointsViewClick;
                }

                if (window.FindView("OSTBuddySaveList", out Button saveListView))
                {
                    saveListView.Tag = window;
                    saveListView.Clicked = HandleSaveListViewClick;
                }

                if (window.FindView("OSTBuddyLoadList", out Button loadListView))
                {
                    loadListView.Tag = window;
                    loadListView.Clicked = HandleLoadListViewClick;
                }
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("RespawnDelayBox", out TextInputView _respawnDelayInput);

                if (int.TryParse(_respawnDelayInput.Text, out int _respawnDelayValue)
                    && Config.CharSettings[Game.ClientInst].RespawnDelay != _respawnDelayValue)
                {
                    Config.CharSettings[Game.ClientInst].RespawnDelay = _respawnDelayValue;
                    RespawnDelay = _respawnDelayValue;
                }

                if (SettingsController.settingsWindow.FindView("OSTBuddyWaypointsView", out Button waypointView))
                {
                    waypointView.Tag = SettingsController.settingsWindow;
                    waypointView.Clicked = HandleWaypointsViewClick;
                }

                if (SettingsController.settingsWindow.FindView("OSTBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (Leader == Identity.None)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;
                    }

                    Start();
                }

                if (!_settings["Toggle"].AsBool() && Toggle) { Stop(); }
            }

            #endregion
        }

        private void OSTBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        if (Leader == Identity.None)
                        {
                            IsLeader = true;
                            Leader = DynelManager.LocalPlayer.Identity;
                        }

                        _settings["Toggle"] = true;
                        Start();
                    }
                    else
                    {
                        Stop();
                        _settings["Toggle"] = false;
                    }
                }
                else if (param.Length >= 1)
                {
                    switch (param[0].ToLower())
                    {
                        case "load":
                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints");

                            string _loadPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\OSTBuddy\Waypoints\{param[1]}.txt";
                            Chat.WriteLine($"Waypoints list loaded.");

                            if (!File.Exists(_loadPath))
                            {
                                Chat.WriteLine($"No such file.");
                                return;
                            }

                            using (StreamReader sr = File.OpenText(_loadPath))
                            {
                                string[] _stringAsArray = sr.ReadLine().Split('-');

                                foreach (string _string in _stringAsArray)
                                {
                                    if (_string.Length > 1)
                                    {
                                        float x, y, z;

                                        string[] _stringAsArraySplit = _string.Split(',');
                                        if (_string.Contains('.'))
                                        {
                                            x = float.Parse(_stringAsArraySplit[0], CultureInfo.InvariantCulture.NumberFormat);
                                            y = float.Parse(_stringAsArraySplit[1], CultureInfo.InvariantCulture.NumberFormat);
                                            z = float.Parse(_stringAsArraySplit[2], CultureInfo.InvariantCulture.NumberFormat);
                                        }
                                        else
                                        {
                                            x = float.Parse($"{_stringAsArraySplit[0]}.{_stringAsArraySplit[1]}", CultureInfo.InvariantCulture.NumberFormat);
                                            y = float.Parse($"{_stringAsArraySplit[2]}.{_stringAsArraySplit[3]}", CultureInfo.InvariantCulture.NumberFormat);
                                            z = float.Parse($"{_stringAsArraySplit[4]}.{_stringAsArraySplit[5]}", CultureInfo.InvariantCulture.NumberFormat);
                                        }

                                        _waypoints.Add(new Vector3(x, y, z));
                                    }
                                }
                            }

                            break;

                        case "save":
                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy");

                            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints"))
                                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\OSTBuddy\\Waypoints");

                            string _exportPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\OSTBuddy\Waypoints\{param[1]}.txt";
                            Chat.WriteLine($"Waypoints list exported.");

                            if (!File.Exists(_exportPath))
                            {
                                // Create the file.
                                using (FileStream fs = File.Create(_exportPath))
                                {
                                    foreach (Vector3 vector in _waypoints)
                                    {
                                        string vectorstring = vector.ToString();
                                        string vectorstring2 = vectorstring.Substring(1, vectorstring.Length - 2);

                                        Byte[] info =
                                                new UTF8Encoding(true).GetBytes($"{vectorstring2}-");

                                        fs.Write(info, 0, info.Length);
                                    }
                                }
                            }
                            break;
                        case "addpos":
                            _waypoints.Add(DynelManager.LocalPlayer.Position);
                            break;

                        default:
                            return;
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public enum MongoSelection
        {
            Slam, Demolish
        }
    }
}
