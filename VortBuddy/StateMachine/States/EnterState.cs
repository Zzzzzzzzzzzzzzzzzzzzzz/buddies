﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;

namespace VortBuddy
{
    public class EnterState : IState
    {
        private static bool _init = false;
        private static bool _proceed = false;
        public static bool _passed = false;

        private CancellationTokenSource _cancellationToken1 = new CancellationTokenSource();
        private CancellationTokenSource _cancellationToken2 = new CancellationTokenSource();

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.VortId
                && _proceed
                && !Team.Members.Any(c => c.Character == null))
            {
                return new ScanState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("EnterState::OnStateEnter");
            Task.Factory.StartNew(
                async () =>
                {
                    await Task.Delay(5000);
                    VortBuddy.NavMeshMovementController.SetDestination(new Vector3(523.5f, 310.9f, 307.4f));
                }, _cancellationToken1.Token);
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("EnterState::OnStateExit");

            _cancellationToken1.Cancel();
            _cancellationToken2.Cancel();

            _init = false;
            _proceed = false;
        }

        public void Tick()
        {
            if (Game.IsZoning || Playfield.ModelIdentity.Instance != Constants.VortId 
                || Team.Members.Any(c => c.Character == null)) { return; }

            if (!_init)
            {
                _init = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(9000);
                        List<Identity> _openBags = Inventory.Backpacks
                            .Where(c => !c.IsOpen)
                            .Select(c => c.Identity)
                            .ToList();
                        await Task.Delay(1500);
                        List<Item> bags = Inventory.Items
                            .Where(c => c.UniqueIdentity.Type == IdentityType.Container
                                    && _openBags.Contains(c.UniqueIdentity))
                            .ToList();
                        await Task.Delay(1500);
                        foreach (Item bag in bags)
                        {
                            bag.Use();
                            bag.Use();
                        }
                        await Task.Delay(2000);
                        VortBuddy.NavMeshMovementController.SetDestination(new Vector3(204.7f, 17.6f, 203.2f));
                        await Task.Delay(4000);
                        _proceed = true;
                        _passed = true;
                    }, _cancellationToken2.Token);
            }
        }
    }
}