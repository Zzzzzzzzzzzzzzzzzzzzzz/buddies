﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using VortBuddy.IPCMessages;
using System.Collections.Generic;
using System.Linq;

namespace VortBuddy
{
    public class ReformState : IState
    {
        private static double _reformStartedTime;

        public static bool _proceed = false;

        private ReformPhase _phase;

        public static List<Identity> _teamCache = new List<Identity>();
        private static List<Identity> _invitedList = new List<Identity>();

        public IState GetNextState()
        {
            if (_phase == ReformPhase.Completed)
            {
                if (!Team.Members.Any(c => c.Character == null))
                {
                    EnterState._passed = false;
                    return new EnterState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ReformState::OnStateEnter");

            _reformStartedTime = Time.NormalTime;

            if (DynelManager.LocalPlayer.Identity != VortBuddy.Leader)
            {
                _phase = ReformPhase.Waiting;
            }
            else
                _phase = ReformPhase.Disbanding;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ReformState::OnStateExit");

            _invitedList.Clear();
            _proceed = false;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (_phase == ReformPhase.Waiting
                && Team.IsInTeam
                && _proceed
                && DynelManager.LocalPlayer.Identity != VortBuddy.Leader)
            {
                _phase = ReformPhase.Completed;
            }

            if (DynelManager.LocalPlayer.Identity != VortBuddy.Leader) { return; }

            if (_phase == ReformPhase.Disbanding)
            {
                if (Team.IsInTeam)
                    Team.Disband();

                if (Time.NormalTime > _reformStartedTime + 4f)
                    _phase = ReformPhase.Inviting;
            }

            if (_phase == ReformPhase.Inviting && _invitedList.Count() < _teamCache.Count())
            {
                foreach (SimpleChar player in DynelManager.Players.Where(c => c.IsInPlay && !_invitedList.Contains(c.Identity) && _teamCache.Contains(c.Identity)))
                {
                    if (_invitedList.Contains(player.Identity)) { continue; }

                    _invitedList.Add(player.Identity);

                    if (player.Identity == VortBuddy.Leader) { continue; }

                    Team.Invite(player.Identity);
                }
            }

            if (_phase == ReformPhase.Inviting
                && Team.IsInTeam
                && Team.Members.Where(c => c.Character != null).ToList().Count == _teamCache.Count()
                && _invitedList.Count() == _teamCache.Count())
            {
                _phase = ReformPhase.Completed;
                VortBuddy.IPCChannel.Broadcast(new ProceedMessage());
            }
        }
        private enum ReformPhase
        {
            Disbanding,
            Inviting,
            Waiting,
            Completed
        }
    }
}
