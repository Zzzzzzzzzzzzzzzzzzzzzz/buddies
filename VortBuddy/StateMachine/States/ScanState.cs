﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace VortBuddy
{
    public class ScanState : IState
    {
        private SimpleChar _target;

        public static bool _init = false;
        private static bool _proceed = false;

        private CancellationTokenSource _cancellationToken1 = new CancellationTokenSource();
        private CancellationTokenSource _cancellationToken2 = new CancellationTokenSource();

        public IState GetNextState()
        {
            if (_target != null)
                return new FightState(_target);

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId
                && _proceed && !Team.Members.Any(c => c.Character == null))
            {
                return new ReformState();
            }

            if (!VortBuddy._settings["Toggle"].AsBool())
                return new IdleState();

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ScanState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ScanState::OnStateExit");

            _cancellationToken1.Cancel();
            _cancellationToken2.Cancel();

            _init = false;
            _proceed = false;
        }

        public void Tick()
        {
            if (Game.IsZoning || Playfield.ModelIdentity.Instance != Constants.VortId) { return; }

            if (!_init && DynelManager.Corpses.Any(c => c.Name.Contains("Vortexx"))
                && !DynelManager.NPCs.Any(c => c.Name.Contains("Desecrated Spirit"))
                && VortBuddy.ModeSelection.Single == (VortBuddy.ModeSelection)VortBuddy._settings["ModeSelection"].AsInt32())
            {
                _init = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(25000);
                        VortBuddy._settings["Toggle"] = false;
                        VortBuddy.Toggle = false;
                        _init = false;
                        EnterState._passed = false;
                        Chat.WriteLine("Finished.");
                    }, _cancellationToken1.Token);
            }

            if (!_proceed && !_init && DynelManager.Corpses.Any(c => c.Name.Contains("Vortexx"))
                && !DynelManager.NPCs.Any(c => c.Name.Contains("Desecrated Spirit"))
                && VortBuddy.ModeSelection.Loop == (VortBuddy.ModeSelection)VortBuddy._settings["ModeSelection"].AsInt32())
            {
                _init = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(15000);
                        Corpse _corpse = DynelManager.Corpses.FirstOrDefault(c => c.Name.Contains("Vortexx"));
                        await Task.Delay(1000);
                        MovementController.Instance.SetDestination((Vector3)_corpse?.Position);
                        await Task.Delay(22000);
                        MovementController.Instance.SetDestination(new Vector3(204.4f, 17.5f, 201.9f));
                        await Task.Delay(4000);
                        Dynel beacon = DynelManager.AllDynels
                            .Where(c => c.Name.Contains("Dust Brigade Beacon")
                                && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) < 30f)
                            .FirstOrDefault();
                        await Task.Delay(4000);
                        if (beacon != null)
                            MovementController.Instance.SetDestination((Vector3)beacon?.Position.Randomize(1f));
                        await Task.Delay(40000);
                        if (beacon != null)
                            beacon.Use();
                        await Task.Delay(11000);
                        _proceed = true;
                        _init = false;
                    }, _cancellationToken2.Token);
            }

            if (VortBuddy._mob.Count >= 1)
            {
                if (VortBuddy._mob.FirstOrDefault().Health == 0) { return; }

                _target = VortBuddy._mob.FirstOrDefault();
                Chat.WriteLine($"Found target: {_target.Name}.");
            }
            else if (VortBuddy._bossMob.Count >= 1)
            {
                if (VortBuddy._bossMob.FirstOrDefault().Health == 0) { return; }

                _target = VortBuddy._bossMob.FirstOrDefault();
                Chat.WriteLine($"Found target: {_target.Name}.");
            }
        }
    }
}
