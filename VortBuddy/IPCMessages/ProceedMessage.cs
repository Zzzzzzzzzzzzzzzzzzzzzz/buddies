﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace VortBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.Proceed)]
    public class ProceedMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Proceed;
    }
}
